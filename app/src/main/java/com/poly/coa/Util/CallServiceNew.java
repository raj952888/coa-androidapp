package com.poly.coa.Util;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.poly.coa.Util.NotificationUtil;

import androidx.annotation.Nullable;

public class CallServiceNew extends Service {
//    private static final String LOG_TAG = TokenService.class.getSimpleName();

    @Nullable
    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationUtil.createServiceChannel(this, NotificationUtil.SIP_CHANNEL_ID,"callcoauser");
            Notification notification = NotificationUtil.buildServiceNotification(this, NotificationUtil.SIP_CHANNEL_ID,"COA-Audio Call in",intent);
            startForeground(NotificationUtil.NOTIFICATION_ID, notification);
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onTaskRemoved(final Intent rootIntent) {
        Log.d("Aaaaaa", "InCallService onTaskRemoved called");
        stopSelf();
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onCreate() {
        Log.d("Aaaaaa", "InCallService onCreate called");
        super.onCreate();
    }


}
