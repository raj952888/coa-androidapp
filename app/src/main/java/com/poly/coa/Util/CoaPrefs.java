package com.poly.coa.Util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.List;

public class CoaPrefs {
    private static final String Coa = "CoaSecurity";
    private static SharedPreferences sharedPreferences;

    public static SharedPreferences getInstance(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Coa, 0);
        }
        return sharedPreferences;
    }

    public static String getString(Context context, String str) {
        getInstance(context);
        return sharedPreferences.getString(str, "");
    }

    public static void putString(Context context, String str, String str2) {
        getInstance(context);
        sharedPreferences.edit().putString(str, String.valueOf(str2)).commit();
    }

    public static int getInt(Context context, String str, int i) {
        getInstance(context);
        return sharedPreferences.getInt(str, i);
    }

    public static void putInt(Context context, String str, int i) {
        getInstance(context);
        sharedPreferences.edit().putInt(str, i).commit();
    }

    public static boolean getBoolean(Context context, String str) {
        getInstance(context);
        return sharedPreferences.getBoolean(str, false);
    }

    public static void putBoolean(Context context, String str, boolean z) {
        getInstance(context);
        sharedPreferences.edit().putBoolean(str, z).apply();
    }

    public static void clear(Context context) {
        getInstance(context);
        sharedPreferences.edit().clear().apply();
    }

    public static void putLong(Context context, String str, long j) {
        getInstance(context);
        sharedPreferences.edit().putLong(str, j).apply();
    }

    public static long getLong(Context context, String str) {
        getInstance(context);
        return sharedPreferences.getLong(str, 0);
    }

    public static void putFloat(Context context, String str, float f) {
        getInstance(context);
        sharedPreferences.edit().putFloat(str, f).apply();
    }
/*
    public static void saveCart(GetCartResponse getCartResponse) {
        Gson gson = new Gson();
        String json = gson.toJson(getCartResponse);
        sharedPreferences.edit().putString("cartObject", json).commit();
    }

    public static GetCartResponse getCart(Context context) {
        getInstance(context);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("cartObject", "");
        GetCartResponse getCartResponse = gson.fromJson(json, GetCartResponse.class);
        return getCartResponse;
    }

    public static void FilterResponse(FilterResponseNew filterResponse) {
        Gson gson = new Gson();
        String json = gson.toJson(filterResponse);
        sharedPreferences.edit().putString("bestSellerObject", json).commit();
    }

    public static FilterResponseNew getFilterResponse(Context context) {
        getInstance(context);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("bestSellerObject", "");
        FilterResponseNew filterResponse = gson.fromJson(json, FilterResponseNew.class);
        return filterResponse;
    }

    public static void CategoryAndSubcategory(CategoryAndSubCategoryResponse categoryAndSubCategoryResponse) {
        Gson gson = new Gson();
        String json = gson.toJson(categoryAndSubCategoryResponse);
        sharedPreferences.edit().putString("categoryandsubcategoryobject", json).commit();
    }

    public static CategoryAndSubCategoryResponse getCategoryAndSubCategoryResponse(Context context) {
        getInstance(context);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("categoryandsubcategoryobject", "");
        CategoryAndSubCategoryResponse categoryAndSubCategoryResponse = gson.fromJson(json, CategoryAndSubCategoryResponse.class);
        return categoryAndSubCategoryResponse;
    }


    public static void saveProduct(Context context, Datum getCartResponse, String key) {
        getInstance(context);
        Gson gson = new Gson();
        String json = gson.toJson(getCartResponse);
        sharedPreferences.edit().putString(key, json).commit();
    }

    public static Datum getProduct(Context context, String key) {
        getInstance(context);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, "");
        Datum getCartResponse = gson.fromJson(json, Datum.class);
        return getCartResponse;
    }

    public static void saveSubCategory(Context context, Subcategory subcategory, String key) {
        getInstance(context);
        Gson gson = new Gson();
        String json = gson.toJson(subcategory);
        sharedPreferences.edit().putString(key, json).commit();
    }

    public static Subcategory getSubCategory(Context context, String key) {
        getInstance(context);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, "");
        Subcategory subcategory = gson.fromJson(json, Subcategory.class);
        return subcategory;
    }*/

    public static void removeKey(Context context, String key) {
        getInstance(context);
        sharedPreferences.edit().remove(key).commit();
    }




    public static String getCartId(Context context, String str) {
        getInstance(context);
        return sharedPreferences.getString(str, "");
    }

    public static void putCartId(Context context, String str, String str2) {
        getInstance(context);
        sharedPreferences.edit().putString(str, String.valueOf(str2)).commit();
    }



}
