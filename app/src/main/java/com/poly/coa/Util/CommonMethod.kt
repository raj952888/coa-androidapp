package com.poly.coa.Util

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.provider.Settings
import android.util.Patterns
import com.poly.coa.R
import com.poly.coa.R.layout.progress_dialog
import java.util.regex.Pattern


/*
 * Created By Avdhesh
 * */
object CommonMethod {

    /**
     * This method use for change particular
     * portion of text color from  given string
     *
     * @param context    get context/activity get resources
     * @param myString   Your string which you want to span as per given color
     * @param fontType   change font type for given spannable
     * @param startIndex start index of spannable color for given string
     * @param endIndex   end index of spannable color string
     * @param color      color  resource have you want to change
     */


    @JvmStatic
    fun showLoadingDialog(context: Context): ProgressDialog {
        val progressDialog = ProgressDialog(context,R.style.TransparentProgressDialog)
        progressDialog.show()
        if (progressDialog.window != null) {
            progressDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
        progressDialog.setContentView(progress_dialog)
        progressDialog.isIndeterminate = true
        progressDialog.setCancelable(true)
        progressDialog.setCanceledOnTouchOutside(false)
        return progressDialog
    }

    fun isValidEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

//    fun isMobileNumberValid(number: String): Boolean {
//
//        val strRegex = "^(-?\\d+\\\\d+)$|^(\\d+)$"
//
//        return Pattern.compile(strRegex).matcher(number).matches()
//    }

    fun isMobileNumberValid(number: String): Boolean {

        if (!number.equals("")) {
            return true
        } else {
            return false
        }
    }

    fun isCountryCodeNumberValid(number: String): Boolean {

//        var regex:String? = "\\d+"

        var regex: String? = "^\\+(?:[0-9] ?){6,14}[0-9]$"
        return Pattern.compile(regex).matcher(number).matches()
    }

    @SuppressLint("all")
    fun getDeviceId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

/*
    fun isEmailValid(email: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(email)
        return matcher.matches()
    }
*/

    fun sendMail(context: Context, mailAddress: String) {
        val emailIntent = Intent(Intent.ACTION_SENDTO)
        emailIntent.data = Uri.parse("mailto: $mailAddress")
        context.startActivity(Intent.createChooser(emailIntent, "Send feedback"))


    }

    fun openDialPad(context: Context, telNumber: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$telNumber")
        context.startActivity(intent)

    }


}
