package com.poly.coa.Util;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.widget.Toast;

import androidx.palette.graphics.Palette;

import com.poly.coa.BuildConfig;
import com.poly.coa.model.fcmresponse.FcmTokenResponse;
import com.poly.coa.model.onetoone.VideoCall;
import com.poly.coa.network.RestApiService;
import com.poly.coa.network.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Constants  {
    public static final int GPS_REQUEST = 111;
    public static final String ACCESS_FLAG = "acountflag";
    public static final String ACCESS_TOKEN_EMAIL = "access_token_email";
    public static final String NAME = "name";
    public static final String NAME_2 = "name_2";
    public static final String USERIMAGE = "userimage";
    public static final String IS_TRACK = "istack";

    public static Palette.Swatch palette = null;
    public static final String EMAILID = "emailid";
    public static final String LoginCheck = "logincheck";
    public static final String LOGGEDIN = "loggedin";
    public static final String LOGGEDFBIN = "loggedinn";
    public static final String NOTIFICATION = "notification";
    public static final String Resultsubmit = "resultsubmit";
    public static final String UN_ATTEMPTED = "UnAttempted";
    public static final String ATTEMPTED = "Attempted";
    public static final String ISSTORESELCTED = "isstoreselected";
    public static final String FREE = "Free";
    public static int color_body = 0;
    public static Palette paletteText = null;



    public static final String BASE_URL_API_STAGE = BuildConfig.API_SERVER_IP;


    public static int isNullorZero(Integer i) {
        if (i == null) {
            return 0;
        } else {
            return i;
        }
    }

    public static String isNull(String i) {
        if (i == null) {
            return "null";
        } else {
            return i;
        }
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }


}
