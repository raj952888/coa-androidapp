package com.poly.coa.Util

import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns
import android.util.Log
import java.io.File
import java.io.FileOutputStream

class FileUtils11{

    companion object{
        fun makeFileCopyInCacheDir(contentUri : Uri, context: Context) : String? {
            try {
                val filePathColumn = arrayOf(
                    //Base File
                    "_id",
                    "title",
                    "_data",
                    "_size",
                    "date_added",
                    "_display_name",
                    "mime_type",
                    )
                //val contentUri = FileProvider.getUriForFile(context, "${BuildConfig.APPLICATION_ID}.provider", File(mediaUrl))
                val returnCursor = contentUri.let { context.contentResolver.query(it, filePathColumn, null, null, null) }
                if (returnCursor!=null) {
                    returnCursor.moveToFirst()
                    val nameIndex = returnCursor.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME)
                    val name = returnCursor.getString(nameIndex)
                    val file = File(context.getCacheDir(), name)
                    val inputStream = context.contentResolver.openInputStream(contentUri)
                    val outputStream = FileOutputStream(file)
                    var read = 0
                    val maxBufferSize = 1 * 1024 * 1024
                    val bytesAvailable = inputStream!!.available()

                    //int bufferSize = 1024;
                    val bufferSize = Math.min(bytesAvailable, maxBufferSize)
                    val buffers = ByteArray(bufferSize)
                    while (inputStream.read(buffers).also { read = it } != -1) {
                        outputStream.write(buffers, 0, read)
                    }
                    inputStream.close()
                    outputStream.close()
                    Log.e("File Path", "Path " + file.path)
                    Log.e("File Size", "Size " + file.length())
                    return file.absolutePath
                }
            } catch (ex: Exception) {
                Log.e("Exception", ex.message!!)
            }
            return contentUri.let { FileUtils.getRealPath(context, it).toString() }
        }

    }
}