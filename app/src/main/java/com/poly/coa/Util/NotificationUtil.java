package com.poly.coa.Util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.poly.coa.R;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationUtil {
    public static final String NOTIFICATION_CHANNEL_ID = "10001" ;
    private final static String default_notification_channel_id = "defaultcoa" ;
    public static final String SIP_CHANNEL_ID = "com.polycoa.sip_notification_channel";
    public static final String SCREENSHARE_CHANNEL_ID = "com.polycoa.screenshare_channel";
    public static final String CALL_CHANNEL_ID = "com.polycoa.call_notification_channel";
    public static final String SERVICE_GROUP_ID = "com.polycoa.notification_group";
    public static final String CALL_GROUP_ID = "com.polycoa.call_notification_group";
    public static final String ORGANIZATION_CHANNEL_ID = "compoly.coa.organization_channel";
    public static final String ORGANIZATION_GROUP_ID = "compoly.coa.organization_group";

    public static final int NOTIFICATION_ID = 117;
    public static final int SCREENSHARE_SERVICE_NOTIFICATION_ID = 901;
    public static  NotificationManager  notificationManager;

    public static Notification buildServiceNotification(Context context, String channelId, String notificationContent,Intent intent) {
         RemoteViews contentView = new RemoteViews(context.getPackageName() , R.layout.custom_notification_layout1 ) ;

        contentView.setTextViewText(R.id.title, notificationContent);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, channelId);
        return notificationBuilder.setContentTitle(notificationContent)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setContent(contentView)
                .setChannelId(channelId)
                .setGroup(SERVICE_GROUP_ID)
                .setGroupSummary(true)
                .setOngoing(true)
                .setSmallIcon(R.drawable.logo_no_title)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
    }

    public static Notification buildCallNotification(Context context, String notificationContent, PendingIntent pendingIntent) {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CALL_CHANNEL_ID);
        return notificationBuilder.setContentTitle(context.getString(R.string.app_name))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setChannelId(CALL_CHANNEL_ID)
                .setContentIntent(pendingIntent)
                .setContentText(notificationContent)
                .setGroup(CALL_GROUP_ID)
                .setGroupSummary(true)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.logo_no_title)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .build();
    }


//    public static Notification buildCallNotificationFromBackground(Context context, String notificationContent, PendingIntent pendingIntentAccept) {
//
//        Intent accept = new Intent(context, NotificationActionReceiver.class);
//        accept.putExtra("call", "accept");
//        PendingIntent acceptCall = PendingIntent.getBroadcast(context, 0, accept, PendingIntent.FLAG_CANCEL_CURRENT);
//
//        Intent reject = new Intent(context, NotificationActionReceiver.class);
//
//        reject.putExtra("call", "reject");
//
//        PendingIntent rejectCall = PendingIntent.getBroadcast(context, 0, reject, PendingIntent.FLAG_ONE_SHOT);
//
//
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CALL_CHANNEL_ID);
//
//        return notificationBuilder.setContentTitle(context.getString(R.string.app_name))
//                .setPriority(NotificationCompat.PRIORITY_HIGH)
//                .setChannelId(CALL_CHANNEL_ID)
//                .setContentIntent(pendingIntentAccept)
//                .setContentText(notificationContent)
//                .setGroup(CALL_GROUP_ID)
//                .setGroupSummary(true)
//                .setAutoCancel(false)
//                .setOngoing(true)
//                .setOnlyAlertOnce(true)
//                .addAction(R.drawable.recording_red_border_accept, "ACCEPT", acceptCall) // #0
//                .addAction(R.drawable.recording_red_border, "REJECT", rejectCall)
//                .setSmallIcon(R.drawable.app_notification)
//                .setColor(ContextCompat.getColor(context, R.color.greenPrimary))
//                .build();
//    }

//    public static Notification buildOrganizationNotification(Context context, String title, String body, PendingIntent pendingIntent) {
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, ORGANIZATION_CHANNEL_ID);
//
//
//        return notificationBuilder.setContentTitle(context.getString(R.string.app_name))
//                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                .setChannelId(ORGANIZATION_CHANNEL_ID)
//                .setContentIntent(pendingIntent)
//                .setContentText(body)
//                .setContentTitle(title)
//                .setGroup(ORGANIZATION_GROUP_ID)
//                .setGroupSummary(true)
//                .setAutoCancel(true)
//
//                .setSmallIcon(R.drawable.app_notification)
//                .setColor(ContextCompat.getColor(context, R.color.greenPrimary))
//                .build();
//    }
//
//    public static void createOrganizationChannel(Context context, String notificationContext) {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//
//            NotificationChannel channel = new NotificationChannel(ORGANIZATION_CHANNEL_ID, notificationContext, NotificationManager.IMPORTANCE_DEFAULT);
//            channel.setGroup(ORGANIZATION_GROUP_ID);
//            channel.enableLights(true);
//            channel.enableVibration(true);
//            channel.setShowBadge(true);
//            NotificationChannelGroup channelGroup = new NotificationChannelGroup(ORGANIZATION_GROUP_ID, context.getString(R.string.notification_call_group_title));
//            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            if (manager != null) {
//                manager.createNotificationChannelGroup(channelGroup);
//                manager.createNotificationChannel(channel);
//            }
//        }
//    }
//
//
//    public static void createCallChannel(Context context) {
//
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//
//            NotificationChannel channel = new NotificationChannel(CALL_CHANNEL_ID, context.getString(R.string.notification_call_channel_title), NotificationManager.IMPORTANCE_HIGH);
//            channel.setGroup(CALL_GROUP_ID);
//            channel.enableLights(true);
//            channel.enableVibration(true);
//            channel.setShowBadge(true);
//            NotificationChannelGroup channelGroup = new NotificationChannelGroup(CALL_GROUP_ID, context.getString(R.string.notification_call_group_title));
//            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            if (manager != null) {
//                manager.createNotificationChannelGroup(channelGroup);
//                manager.createNotificationChannel(channel);
//            }
//        }
//    }
//
    public static void createServiceChannel(Context context, String channelId, String channelTitle) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(channelId, channelTitle, NotificationManager.IMPORTANCE_NONE);
            channel.setGroup(SERVICE_GROUP_ID);
            channel.enableLights(false);
            channel.enableVibration(false);
            channel.setShowBadge(false);
            NotificationChannelGroup channelGroup = new NotificationChannelGroup(SERVICE_GROUP_ID, context.getString(R.string.notification_service_group_title));
            NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            if (manager != null) {
                manager.createNotificationChannelGroup(channelGroup);
                manager.createNotificationChannel(channel);
            }

        }
    }

    public static  void createNotification (Context context,int notificationId,Intent intent,String callName) {

        PendingIntent pendingIntent =  PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        RemoteViews contentView = new RemoteViews(context.getPackageName() , R.layout.custom_notification_layout ) ;

        contentView.setTextViewText(R.id.title,callName);

        notificationManager = (NotificationManager) context.getSystemService( NOTIFICATION_SERVICE ) ;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, default_notification_channel_id ) ;
        mBuilder.setContent(contentView);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setDefaults(NotificationCompat.DEFAULT_ALL);
        mBuilder.setSmallIcon(R.drawable.logo_no_title );
        mBuilder.setColor(context.getResources().getColor(R.color.colorPrimary));;
        mBuilder.setAutoCancel( false ) ;
        mBuilder.setOngoing(true);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        if (android.os.Build.VERSION. SDK_INT >= android.os.Build.VERSION_CODES. O ) {
            int importance = NotificationManager. IMPORTANCE_HIGH ;
            NotificationChannel notificationChannel = new NotificationChannel( NOTIFICATION_CHANNEL_ID , "NOTIFICATION_CHANNEL_NAME" , importance) ;
            mBuilder.setChannelId( NOTIFICATION_CHANNEL_ID ) ;
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel) ;
        }
        assert notificationManager != null;
        notificationManager.notify( notificationId , mBuilder.build()) ;
    }


    public static void removeNotification (Context context,int notificationId) {
        notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if ( notificationId != 0 && notificationManager!=null)
            if(notificationId==181) {
                notificationManager.cancel(notificationId);
                notificationManager.cancel(117);
            }else{
                notificationManager.cancelAll();
            }
    }
    public static void clearAll(Context context) {
        notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }
}
