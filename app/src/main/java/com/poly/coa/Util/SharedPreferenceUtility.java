package com.poly.coa.Util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceUtility {

    private Context context;
    private SharedPreferences mSharedPreferences;


    public SharedPreferenceUtility(Context context) {
        this.context = context;
        mSharedPreferences = context.getSharedPreferences("SharedPrefBringin",
                Context.MODE_PRIVATE);

    }


    public String getSharedPrefenceVersion() {
        String userOuth = mSharedPreferences.getString("VERSION_NAME", "");
        return userOuth;
    }

    public void setPreferenceVersionUpdate(String versionName) {
        mSharedPreferences.edit().putString("VERSION_NAME", versionName).commit();

    }


    public void clearAllData() {
        mSharedPreferences.edit().clear().apply();

    }


    public String getAuthToken() {
        String userAuth = mSharedPreferences.getString("userAuth", "");
        return userAuth;
    }

    public void setAuthToken(String AuthToken) {
        mSharedPreferences.edit().putString("userAuth", AuthToken).commit();

    }

    public void setString(String key, String value) {
        mSharedPreferences.edit().putString(key, value).commit();

    }

    public String getString(String key) {
        String valueSong = mSharedPreferences.getString(key, "");
        return valueSong;
    }

    public boolean getUserLoggedIn() {
        boolean userAuth = mSharedPreferences.getBoolean("login", false);
        return userAuth;
    }

    public void setUserLoggedin(boolean userLogged) {
        mSharedPreferences.edit().putBoolean("login", userLogged).commit();

    }


    public void setSongAdded(String key, boolean isAdded) {
        mSharedPreferences.edit().putBoolean(key, true).commit();
    }

    public boolean getSongAdded(String key) {
        boolean getIsAdded = mSharedPreferences.getBoolean(key, false);
        return getIsAdded;
    }

    public String getUserRecord(String key) {
        String getUserRecord = mSharedPreferences.getString(key, "");
        return getUserRecord;
    }


    public void setUserRecord(String key, String recordValue) {
        mSharedPreferences.edit().putString(key, recordValue).commit();

    }

    public void Logout() {
        setAuthToken("");
    }


}
