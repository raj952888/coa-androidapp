package com.poly.coa.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.poly.coa.R;
import com.poly.coa.model.completedBooking.Datum;
import com.poly.coa.model.getAllConsultant.Consultant;
import com.poly.coa.model.getAllConsultant.GetAllAstrologerResponse;
import com.poly.coa.model.getslot.Result;
import com.poly.coa.view.ConsultanyDetailActivity;
import com.poly.coa.view.MainActivity2;
import com.google.gson.Gson;

import java.util.List;

public class CompletedNew extends RecyclerView.Adapter<CompletedNew.ViewHolder> {

    private List<Datum> datumList;
    Context context;
    HoroscopeInterface horoscopeInterface;
    int row_index;

    public CompletedNew(List<Datum> datumList, Context context) {
        this.datumList = datumList;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.completedchild, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Datum getCategoryItem = datumList.get(position);

        holder.textView.setText(getCategoryItem.getConsultantName());
        holder.textViewTime.setText(getCategoryItem.getTime());
        holder.textViewdate.setText(getCategoryItem.getDate());

        Glide.with(context).load(getCategoryItem.getConsultantImage())

                .addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true).
                centerInside().
                into(holder.imageView);


    }


    @Override
    public int getItemCount() {

        return datumList.size();


    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView, imageViewicon, imageViewsubcategoryproductimage;
        LinearLayout linearLayout, linearLayoutll_astrologer;
        public TextView textView, textViewSessionTime,textViewdate, textViewcallstatus, textViewTime;
        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.name);
            imageView= itemView.findViewById(R.id.consultantimage);
            textViewSessionTime = itemView.findViewById(R.id.sessiontime);
            textViewcallstatus = itemView.findViewById(R.id.status);
            textViewTime = itemView.findViewById(R.id.idtime);
            textViewdate= itemView.findViewById(R.id.date);


        }
    }

    public interface HoroscopeInterface {
        public void horoscopeList(String endTime,String starttime);

    }


    public void horoscopeList(HoroscopeInterface horoscopeInterface) {
        this.horoscopeInterface = horoscopeInterface;


    }


}