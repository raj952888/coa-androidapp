package com.poly.coa.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.poly.coa.R;
import com.poly.coa.model.getAllConsultant.Consultant;
import com.poly.coa.model.getAllConsultant.GetAllAstrologerResponse;
import com.poly.coa.model.getslot.Result;
import com.poly.coa.model.horoscope.Datum;
import com.poly.coa.view.ConsultanyDetailActivity;
import com.poly.coa.view.MainActivity2;
import com.google.gson.Gson;

import java.util.List;

public class GetAllSlot extends RecyclerView.Adapter<GetAllSlot.ViewHolder> {

    private List<Result> datumList;
    Context context;
    HoroscopeInterface horoscopeInterface;
    int row_index;

    public GetAllSlot(List<Result> datumList, Context context) {
        this.datumList = datumList;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.getconsultantchild, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Result getCategoryItem = datumList.get(position);
        holder.textView.setText(getCategoryItem.getSlotStartTime() + "-" + getCategoryItem.getSlotEndTime());

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 if(horoscopeInterface != null){
                    horoscopeInterface.horoscopeList(getCategoryItem.getSlotEndTime(),getCategoryItem.getSlotStartTime());

                }
                row_index = position;
                notifyDataSetChanged();
            }
        });
        if (row_index == position) {
            holder.textView.setBackgroundResource(R.drawable.loginbuttonbackgroundcircle);
            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.white));

        }
        else {
            holder.textView.setBackgroundResource(R.drawable.loginbutongray);
            holder.textView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        }

    }


    @Override
    public int getItemCount() {

        return datumList.size();


    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView, imageViewicon, imageViewsubcategoryproductimage;
        LinearLayout linearLayout, linearLayoutll_astrologer;
        public TextView textView, textViewconsultant_type, textViewlanguage, textViewexperince;
        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.slot);


        }
    }

    public interface HoroscopeInterface {
        public void horoscopeList(String endTime,String starttime);

    }


    public void horoscopeList(HoroscopeInterface horoscopeInterface) {
        this.horoscopeInterface = horoscopeInterface;


    }


}