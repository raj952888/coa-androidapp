package com.poly.coa.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.poly.coa.R;
import com.poly.coa.model.getAllConsultant.GetAllAstrologerResponse;

public class GetAstrolozerAdapter extends RecyclerView.Adapter<GetAstrolozerAdapter.ViewHolder> {

    private GetAllAstrologerResponse datumList;
    Context context;
    HoroscopeInterface horoscopeInterface;
    SubCategoryInterfacePrdocutDescriptionNew subCategoryInterfacePrdocutDescriptionNew;
    int row_index;
    SubCategoryInterfacePrdocutDescription subCategoryInterfacePrdocutDescription;
    // RecyclerView recyclerView;
    public GetAstrolozerAdapter(GetAllAstrologerResponse datumList, Context context) {
        this.datumList = datumList;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.astrolozer_child, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final GetAllAstrologerResponse getCategoryItem = datumList;
        holder.textView.setText(getCategoryItem.getConsultantList().get(position).getFirstName());
        holder.textViewconsultant_type.setText(""+getCategoryItem.getConsultantList().get(position).getExpertise());
        holder.textViewlanguage.setText(""+getCategoryItem.getConsultantList().get(position).getLanguage());
        holder.textViewexperince.setText(""+getCategoryItem.getConsultantList().get(position).getExperience()+" "+"years of experience");

        holder.textViewVideoCallPrice.setText(""+getCategoryItem.getConsultantList().get(position).getPer_minute_charge()+"/ min");
        holder.textViewphoneCallPrice.setText(""+getCategoryItem.getConsultantList().get(position).getPer_minute_charge()+"/ min");
        holder.textViewChatPrice.setText(""+getCategoryItem.getConsultantList().get(position).getPerSessionCharge()+"/ min");

        if(getCategoryItem.getConsultantList().get(position).getStatus().equalsIgnoreCase("online")){
            holder.imageViewOnline.setVisibility(View.VISIBLE);
        }else {

            holder.imageViewOnline.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(getCategoryItem.getConsultantList().get(position).getCustomImage())) {
            Glide.with(context).load(getCategoryItem.getConsultantList().get(position).getCustomImage()).listener(new RequestListener<Drawable>() {

                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Glide.with(context).load(getCategoryItem.getConsultantList().get(position).getImage()).diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .skipMemoryCache(true)
                                    .fitCenter() .into(holder.imageView);
                        }
                    }, 1000);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            }).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .fitCenter()
                    .into(holder.imageView);
        }
        else if (!TextUtils.isEmpty(getCategoryItem.getConsultantList().get(position).getCustomImage()) && !getCategoryItem.getConsultantList().get(position).getCustomImage().equalsIgnoreCase("null"))
            Glide.with(context).load(getCategoryItem.getConsultantList().get(position).getCustomImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true)
                    .fitCenter() .into(holder.imageView);

//        Glide.with(context)
//                .load(getCategoryItem.getConsultantList().get(position).getCustomImage())
//                .addListener(new RequestListener<Drawable>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                        return false;
//                    }
//                }).diskCacheStrategy(DiskCacheStrategy.ALL)
//                .skipMemoryCache(true)
//                .fitCenter() // scale to fit entire image within ImageView
//                .into(holder.imageView);

        holder.linearLayoutll_astrologer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (subCategoryInterfacePrdocutDescriptionNew != null) {
                    subCategoryInterfacePrdocutDescriptionNew.SubcategoryPrdocutDescriptionlistnew(position, datumList);


                }


            }
        });

        holder.linearLayoutvideoCallLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


             if(horoscopeInterface != null ){
                horoscopeInterface.horoscopeList(position, datumList);
             }
            }
        });

        holder.linearLayoutphonecallid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(subCategoryInterfacePrdocutDescription != null ){
                    subCategoryInterfacePrdocutDescription.SubcategoryPrdocutDescriptionlist(position, datumList);
                }
            }
        });


    }


    @Override
    public int getItemCount() {
        if (datumList != null
                && datumList.getConsultantList() != null &&
                datumList.getConsultantType() != null
                && datumList.getConsultantList() != null && datumList.getConsultantList().size() > 0) {
            return datumList.getConsultantList().size();
        } else {
            return 0;
        }

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView, imageViewiconVideoCall, imageViewOnline;
        LinearLayout linearLayout,linearLayoutphonecallid, linearLayoutll_astrologer,linearLayoutvideoCallLl;
        public TextView textView,textViewphoneCallPrice,textViewVideoCallPrice,textViewChatPrice, textViewconsultant_type, textViewlanguage, textViewexperince;
        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.consultantname);
            imageViewOnline= itemView.findViewById(R.id.image_online);
            imageView = itemView.findViewById(R.id.imageView);
            linearLayout = itemView.findViewById(R.id.ll_horoscope);
            linearLayoutphonecallid= itemView.findViewById(R.id.phonecallid);
            textViewconsultant_type = itemView.findViewById(R.id.consultant_type);
            textViewlanguage = itemView.findViewById(R.id.language);
            textViewexperince = itemView.findViewById(R.id.experince);
            imageViewiconVideoCall = itemView.findViewById(R.id.videoCall);
            linearLayoutll_astrologer = itemView.findViewById(R.id.ll_detail);
            linearLayoutvideoCallLl= itemView.findViewById(R.id.videoCallLl);

            textViewChatPrice = itemView.findViewById(R.id.Chatprice);
            textViewphoneCallPrice = itemView.findViewById(R.id.phoneCallPrice);
            textViewVideoCallPrice = itemView.findViewById(R.id.videoCallPrice);


        }
    }

    public interface HoroscopeInterface {
        public void horoscopeList(int position, GetAllAstrologerResponse getAllAstrologerResponse);

    }


    public void horoscopeList(HoroscopeInterface horoscopeInterface) {
        this.horoscopeInterface = horoscopeInterface;



    }

    public interface SubCategoryInterfacePrdocutDescription {
        public void SubcategoryPrdocutDescriptionlist(int position, GetAllAstrologerResponse getAllAstrologerResponse);

    }

    public void SubcategoryPrdocutDescriptionlist(SubCategoryInterfacePrdocutDescription subCategoryInterfacePrdocutDescription) {
        this.subCategoryInterfacePrdocutDescription = subCategoryInterfacePrdocutDescription;

    }

    public interface SubCategoryInterfacePrdocutDescriptionNew {
        public void SubcategoryPrdocutDescriptionlistnew(int position, GetAllAstrologerResponse getAllAstrologerResponse);

    }

    public void SubcategoryPrdocutDescriptionlistnew(SubCategoryInterfacePrdocutDescriptionNew subCategoryInterfacePrdocutDescriptionNew) {
        this.subCategoryInterfacePrdocutDescriptionNew = subCategoryInterfacePrdocutDescriptionNew;

    }

}