package com.poly.coa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.poly.coa.R;
import com.poly.coa.model.homedataresponse.TodayBooking;

import java.util.List;

public class HomeBookingList extends RecyclerView.Adapter<HomeBookingList.ViewHolder> {

    private List<TodayBooking> datumList;
    Context context;
    HoroscopeInterface horoscopeInterface;
    int row_index;

    public HomeBookingList(List<TodayBooking> datumList, Context context) {
        this.datumList = datumList;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.hoelustchild, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final TodayBooking getCategoryItem = datumList.get(position);

        holder.textView.setText(""+getCategoryItem.getBookingDate());
        holder.textViewTime.setText(getCategoryItem.getStartTime()+"-"+getCategoryItem.getEndTime());





    }


    @Override
    public int getItemCount() {

        return datumList.size();


    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView, imageViewicon, imageViewsubcategoryproductimage;
        LinearLayout linearLayout, linearLayoutll_astrologer;
        public TextView textView, textViewSessionTime,textViewdate, textViewcallstatus, textViewTime;
        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.name);

            textViewSessionTime = itemView.findViewById(R.id.sessiontime);
            textViewcallstatus = itemView.findViewById(R.id.status);
            textViewTime = itemView.findViewById(R.id.idtime);
            textViewdate= itemView.findViewById(R.id.date);


        }
    }

    public interface HoroscopeInterface {
        public void horoscopeList(String endTime,String starttime);

    }


    public void horoscopeList(HoroscopeInterface horoscopeInterface) {
        this.horoscopeInterface = horoscopeInterface;


    }


}