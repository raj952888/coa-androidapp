package com.poly.coa.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.poly.coa.R;
import com.poly.coa.model.horoscope.Datum;

import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    private List<Datum> datumList;
    Context context;
    HoroscopeInterface horoscopeInterface;
    int row_index;

    // RecyclerView recyclerView;
    public MenuAdapter(List<Datum> datumList, Context context) {
        this.datumList = datumList;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.choose_horoscope_sign, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Datum getCategoryItem = datumList.get(position);
        holder.textView.setText(getCategoryItem.getTitle());
        Glide.with(context)
                .load(Uri.parse((getCategoryItem.getImage())))
                .addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true)


                .fitCenter() // scale to fit entire image within ImageView
                .into(holder.imageView);

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (horoscopeInterface != null) {
                    horoscopeInterface.horoscopeList(getCategoryItem.getId());
                }
                row_index = position;
                notifyDataSetChanged();

            }
        });
        if (row_index == position) {
            holder.linearLayout.setBackgroundResource(R.drawable.white_rectangle);

        }


    }


    @Override
    public int getItemCount() {
        return datumList.size();

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView, imageViewicon, imageViewsubcategoryproductimage;
        LinearLayout linearLayout;
        public TextView textView;
        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.name_horoscope);
            imageView = itemView.findViewById(R.id.image);
            linearLayout = itemView.findViewById(R.id.ll_horoscope);


        }
    }

    public interface HoroscopeInterface {
        public void horoscopeList(int horoscopeid);

    }


    public void horoscopeList(HoroscopeInterface horoscopeInterface) {
        this.horoscopeInterface = horoscopeInterface;

    }
}