
package com.poly.coa.model.addamounttowallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("user_transaction")
    @Expose
    private Integer userTransaction;
    @SerializedName("data_roz")
    @Expose
    private DataRoz dataRoz;

    public Integer getUserTransaction() {
        return userTransaction;
    }

    public void setUserTransaction(Integer userTransaction) {
        this.userTransaction = userTransaction;
    }

    public DataRoz getDataRoz() {
        return dataRoz;
    }

    public void setDataRoz(DataRoz dataRoz) {
        this.dataRoz = dataRoz;
    }

}
