
package com.poly.coa.model.bookingSlot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("booking_id")
    @Expose
    private Integer bookingId;
    @SerializedName("consultant_name")
    @Expose
    private String consultantName;
    @SerializedName("consultant_image")
    @Expose
    private String consultantImage;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("consultant_type_title")
    @Expose
    private String consultantTypeTitle;

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public String getConsultantName() {
        return consultantName;
    }

    public void setConsultantName(String consultantName) {
        this.consultantName = consultantName;
    }

    public String getConsultantImage() {
        return consultantImage;
    }

    public void setConsultantImage(String consultantImage) {
        this.consultantImage = consultantImage;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getConsultantTypeTitle() {
        return consultantTypeTitle;
    }

    public void setConsultantTypeTitle(String consultantTypeTitle) {
        this.consultantTypeTitle = consultantTypeTitle;
    }

}
