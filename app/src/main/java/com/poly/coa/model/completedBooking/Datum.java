
package com.poly.coa.model.completedBooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("consultant_name")
    @Expose
    private String consultantName;
    @SerializedName("consultant_image")
    @Expose
    private String consultantImage;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("consultant_type_title")
    @Expose
    private Object consultantTypeTitle;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("date")
    @Expose
    private String date;

    public String getConsultantName() {
        return consultantName;
    }

    public void setConsultantName(String consultantName) {
        this.consultantName = consultantName;
    }

    public String getConsultantImage() {
        return consultantImage;
    }

    public void setConsultantImage(String consultantImage) {
        this.consultantImage = consultantImage;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Object getConsultantTypeTitle() {
        return consultantTypeTitle;
    }

    public void setConsultantTypeTitle(Object consultantTypeTitle) {
        this.consultantTypeTitle = consultantTypeTitle;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
