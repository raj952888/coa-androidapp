
package com.poly.coa.model.getAllConsultant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Consultant implements Serializable {

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("social_id")
    @Expose
    private String socialId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("consultant_id")
    @Expose
    private Integer consultantId;
    @SerializedName("is_verified")
    @Expose
    private String isVerified;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("expertise")
    @Expose
    private Object expertise;
    @SerializedName("experience")
    @Expose
    private Object experience;
    @SerializedName("amount_per_minute")
    @Expose
    private int perSessionCharge;
    @SerializedName("language")
    @Expose
    private Object language;
    @SerializedName("address")
    @Expose
    private Object address;
    @SerializedName("about_me")
    @Expose
    private Object aboutMe;

    public int getPer_minute_charge() {
        return per_minute_charge;
    }

    public void setPer_minute_charge(int per_minute_charge) {
        this.per_minute_charge = per_minute_charge;
    }

    @SerializedName("per_minute_charge")
    @Expose
    private int per_minute_charge;

    @SerializedName("session_duration_in_min")
    @Expose
    private Object sessionDurationInMin;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("gender")
    @Expose
    private Object gender;
    @SerializedName("mobile_verified")
    @Expose
    private String mobileVerified;
    @SerializedName("consultant_type")
    @Expose
    private Object consultantType;
    @SerializedName("custom_image")
    @Expose
    private String customImage;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getConsultantId() {
        return consultantId;
    }

    public void setConsultantId(Integer consultantId) {
        this.consultantId = consultantId;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Object getExpertise() {
        return expertise;
    }

    public void setExpertise(Object expertise) {
        this.expertise = expertise;
    }

    public Object getExperience() {
        return experience;
    }

    public void setExperience(Object experience) {
        this.experience = experience;
    }

    public int getPerSessionCharge() {
        return perSessionCharge;
    }

    public void setPerSessionCharge(int perSessionCharge) {
        this.perSessionCharge = perSessionCharge;
    }

    public Object getLanguage() {
        return language;
    }

    public void setLanguage(Object language) {
        this.language = language;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public Object getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(Object aboutMe) {
        this.aboutMe = aboutMe;
    }

    public Object getSessionDurationInMin() {
        return sessionDurationInMin;
    }

    public void setSessionDurationInMin(Object sessionDurationInMin) {
        this.sessionDurationInMin = sessionDurationInMin;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public String getMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(String mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    public Object getConsultantType() {
        return consultantType;
    }

    public void setConsultantType(Object consultantType) {
        this.consultantType = consultantType;
    }

    public String getCustomImage() {
        return customImage;
    }

    public void setCustomImage(String customImage) {
        this.customImage = customImage;
    }

}
