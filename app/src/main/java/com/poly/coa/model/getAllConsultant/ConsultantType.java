
package com.poly.coa.model.getAllConsultant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConsultantType {

    @SerializedName("consultant_type")
    @Expose
    private Integer consultantType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("consultantimages")
    @Expose
    private String consultantimages;

    public Integer getConsultantType() {
        return consultantType;
    }

    public void setConsultantType(Integer consultantType) {
        this.consultantType = consultantType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getConsultantimages() {
        return consultantimages;
    }

    public void setConsultantimages(String consultantimages) {
        this.consultantimages = consultantimages;
    }

}
