
package com.poly.coa.model.getAllConsultant;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAllAstrologerResponse implements Serializable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("consultant_type")
    @Expose
    private List<ConsultantType> consultantType = null;
    @SerializedName("consultant_list")
    @Expose
    private List<Consultant> consultantList = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ConsultantType> getConsultantType() {
        return consultantType;
    }

    public void setConsultantType(List<ConsultantType> consultantType) {
        this.consultantType = consultantType;
    }

    public List<Consultant> getConsultantList() {
        return consultantList;
    }

    public void setConsultantList(List<Consultant> consultantList) {
        this.consultantList = consultantList;
    }

}
