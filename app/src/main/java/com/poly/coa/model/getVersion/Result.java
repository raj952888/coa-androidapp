
package com.poly.coa.model.getVersion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("version_name")
    @Expose
    private String versionName;
    @SerializedName("version_code")
    @Expose
    private String versionCode;
    @SerializedName("app_version_date")
    @Expose
    private String appVersionDate;
    @SerializedName("app_type")
    @Expose
    private String appType;
    @SerializedName("release_details")
    @Expose
    private Object releaseDetails;
    @SerializedName("fource_upgrade")
    @Expose
    private Integer fourceUpgrade;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getAppVersionDate() {
        return appVersionDate;
    }

    public void setAppVersionDate(String appVersionDate) {
        this.appVersionDate = appVersionDate;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public Object getReleaseDetails() {
        return releaseDetails;
    }

    public void setReleaseDetails(Object releaseDetails) {
        this.releaseDetails = releaseDetails;
    }

    public Integer getFourceUpgrade() {
        return fourceUpgrade;
    }

    public void setFourceUpgrade(Integer fourceUpgrade) {
        this.fourceUpgrade = fourceUpgrade;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

}
