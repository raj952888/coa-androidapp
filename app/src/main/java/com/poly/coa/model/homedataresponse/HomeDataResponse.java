
package com.poly.coa.model.homedataresponse;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeDataResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("wallet_amount")
    @Expose
    private double walletAmount;
    @SerializedName("today_booking_list")
    @Expose
    private List<TodayBooking> todayBookingList = null;
    @SerializedName("horoscope")
    @Expose
    private Horoscope horoscope;
    @SerializedName("horoscope_details")
    @Expose
    private HoroscopeDetails horoscopeDetails;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double  getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(double walletAmount) {
        this.walletAmount = walletAmount;
    }

    public List<TodayBooking> getTodayBookingList() {
        return todayBookingList;
    }

    public void setTodayBookingList(List<TodayBooking> todayBookingList) {
        this.todayBookingList = todayBookingList;
    }

    public Horoscope getHoroscope() {
        return horoscope;
    }

    public void setHoroscope(Horoscope horoscope) {
        this.horoscope = horoscope;
    }

    public HoroscopeDetails getHoroscopeDetails() {
        return horoscopeDetails;
    }

    public void setHoroscopeDetails(HoroscopeDetails horoscopeDetails) {
        this.horoscopeDetails = horoscopeDetails;
    }

}
