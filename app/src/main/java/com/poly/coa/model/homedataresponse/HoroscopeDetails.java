
package com.poly.coa.model.homedataresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HoroscopeDetails {

    @SerializedName("horoscope_title")
    @Expose
    private String horoscopeTitle;
    @SerializedName("horoscope_image")
    @Expose
    private String horoscopeImage;

    public String getHoroscopeTitle() {
        return horoscopeTitle;
    }

    public void setHoroscopeTitle(String horoscopeTitle) {
        this.horoscopeTitle = horoscopeTitle;
    }

    public String getHoroscopeImage() {
        return horoscopeImage;
    }

    public void setHoroscopeImage(String horoscopeImage) {
        this.horoscopeImage = horoscopeImage;
    }

}
