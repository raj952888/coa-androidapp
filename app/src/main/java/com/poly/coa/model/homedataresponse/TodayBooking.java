
package com.poly.coa.model.homedataresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TodayBooking {

    @SerializedName("booking_id")
    @Expose
    private Integer bookingId;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("consultant_type")
    @Expose
    private Object consultantType;
    @SerializedName("booking_date")
    @Expose
    private String bookingDate;

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Object getConsultantType() {
        return consultantType;
    }

    public void setConsultantType(Object consultantType) {
        this.consultantType = consultantType;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

}
