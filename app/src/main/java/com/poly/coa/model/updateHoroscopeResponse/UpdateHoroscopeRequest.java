package com.poly.coa.model.updateHoroscopeResponse;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateHoroscopeRequest {

@SerializedName("user_id")
@Expose
private String userId;
@SerializedName("horoscope_id")
@Expose
private String horoscopeId;

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getHoroscopeId() {
return horoscopeId;
}

public void setHoroscopeId(String horoscopeId) {
this.horoscopeId = horoscopeId;
}

}