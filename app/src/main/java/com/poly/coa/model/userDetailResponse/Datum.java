
package com.poly.coa.model.userDetailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("password")
    @Expose
    private Object password;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("fcm_token")
    @Expose
    private Object fcmToken;
    @SerializedName("profile_img")
    @Expose
    private String profileImg;
    @SerializedName("horoscope_id")
    @Expose
    private Integer horoscopeId;
    @SerializedName("wallet_amount")
    @Expose
    private Object walletAmount;
    @SerializedName("is_verified")
    @Expose
    private String isVerified;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("horoscope_image")
    @Expose
    private String horoscopeImage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Object getPassword() {
        return password;
    }

    public void setPassword(Object password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Object getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(Object fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public Integer getHoroscopeId() {
        return horoscopeId;
    }

    public void setHoroscopeId(Integer horoscopeId) {
        this.horoscopeId = horoscopeId;
    }

    public Object getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(Object walletAmount) {
        this.walletAmount = walletAmount;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHoroscopeImage() {
        return horoscopeImage;
    }

    public void setHoroscopeImage(String horoscopeImage) {
        this.horoscopeImage = horoscopeImage;
    }

}
