
package com.poly.coa.model.verifyAmountResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("user_transaction_id")
    @Expose
    private Integer userTransactionId;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getUserTransactionId() {
        return userTransactionId;
    }

    public void setUserTransactionId(Integer userTransactionId) {
        this.userTransactionId = userTransactionId;
    }

}
