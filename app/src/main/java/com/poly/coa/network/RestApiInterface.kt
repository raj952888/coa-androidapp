package com.poly.coa.network

import com.poly.coa.model.bookingSlot.BookingResponse
import com.poly.coa.model.completedBooking.CompleteResponse
import com.poly.coa.model.getAllConsultant.GetAllAstrologerResponse
import com.poly.coa.model.getOtpResponse.GetOtpResponse
import com.poly.coa.model.getVersion.GetVersionApiResponse
import com.poly.coa.model.getslot.GetConsultantSlotResponse
import com.poly.coa.model.homedataresponse.HomeDataResponse
import com.poly.coa.model.horoscope.HoroscopeResponse
import com.poly.coa.model.upcomingResponse.UpcomingResponse
import com.poly.coa.model.updateHoroscopeResponse.UpdateHoroscopeResponse
import com.poly.coa.model.updateProfile.UpdateProfileResponse
import com.poly.coa.model.userDetailResponse.UserDetailResponse
import com.poly.coa.model.verifyOtpResponse.VerifyOtpResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface RestApiInterface {

    @POST("usersendotp?")
    fun getOtp(@Query("mobile") mobilee: String?): Call<GetOtpResponse?>?

    @POST("userotpverified?")
    fun verifyOtp(@Query("mobile") mobilee: String?,@Query("otp") otp: String?): Call<VerifyOtpResponse?>?

    /*@Multipart
    @POST("help")
    Call<HelpResponse> help(@Part("token") RequestBody token,
                            @Part("name") RequestBody name,
                            @Part("email") RequestBody email,
                            @Part("phone_number") RequestBody phone_number,
                            @Part("query_type") RequestBody query_type,
                            @Part("message") RequestBody message,
                            @Part("user_id") RequestBody user_id

*/


    @POST("updateprofile")
    @Multipart
    fun updateProfile(
        @Part("full_name") full_name: RequestBody?,
        @Part("user_id") user_id: RequestBody?,
        @Part("email") email: RequestBody?,
        @Part("gender") gender: RequestBody?,
        @Part video_file: MultipartBody.Part?
    ): Call<UpdateProfileResponse?>?


    @GET("gethoroscopelist")
    fun getHoroscope(): Call<HoroscopeResponse?>?




    @POST("updatehoroscopesign?")
    fun updateHoroscopeSign(@Query("user_id") user_id: String?,@Query("horoscope_id") horoscope_id: String?): Call<UpdateHoroscopeResponse?>?

    @POST("gethomedata?")
    fun homeData(@Query("user_id") user_id: String?): Call<HomeDataResponse?>?

    @GET("getconsultantslist")
    fun getAllConsultant(): Call<GetAllAstrologerResponse?>?


    @POST("consultant/get_consultant_slot?")
    fun getConsultantSlot(@Query("consultant_id") consultant_id: Int?): Call<GetConsultantSlotResponse?>?



    @POST("user/getupcomingbookings?")
    fun getUpcomingBooking(@Query("user_id") user_id: Int?): Call<UpcomingResponse?>?


    @POST("user/getcompletebooking?")
    fun getCompletedBooking(@Query("user_id") user_id: Int?): Call<CompleteResponse?>?

    @POST("user/login?")
    fun getUserDetails(@Query("mobile") mobilee: String?): Call<UserDetailResponse?>?

    @POST("consultant/booking_appointment?")
    fun bookingSlot(@Query("consultant_id") consultant_id: String?,@Query("user_id") user_id: String?,@Query("slot_id") slot_id: String?,@Query("date") date: String?): Call<BookingResponse?>?

    @GET("consultant/get_user_app_version")
    fun getVersion(): Call<GetVersionApiResponse?>?

}

