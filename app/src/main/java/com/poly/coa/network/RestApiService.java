package com.poly.coa.network;



import com.poly.coa.BuildConfig;
import com.poly.coa.model.addamounttowallet.AddAmountToWalletResponse;
import com.poly.coa.model.callDisconnectResponse.CallDisconnectResponse;
import com.poly.coa.model.fcmresponse.FcmTokenResponse;
import com.poly.coa.model.onetoone.VideoCall;
import com.poly.coa.model.updateProfile.UpdateProfileResponse;
import com.poly.coa.model.userDetailResponse.UserDetailResponse;
import com.poly.coa.model.verifyAmountResponse.VerifyAmountResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RestApiService {


    @Multipart
    @POST("updateprofile")
    Call<UpdateProfileResponse> editUserDetails(@Part("full_name") RequestBody token,
                                                @Part("user_id") RequestBody name,
                                                @Part("email") RequestBody email,
                                                @Part("gender") RequestBody gender,
                                                @Part MultipartBody.Part profile_img

    );


    @POST("user/login?")
    Call<UserDetailResponse> updateUser(@Query("mobile") String mobile);

    @Multipart
    @POST("consultant/video_call_by_user?")
    Call<VideoCall>OneToOne(@Part("consultant_id") RequestBody consultant_id,
                             @Part("user_id") RequestBody user_id,
                            @Part("call_type") RequestBody call_type,
                           @Part("duration") RequestBody duration);




    @POST("consultant/update_fcm_token_of_user?")
    Call<FcmTokenResponse>fcmToken(@Query("user_id") String user_id,
                                   @Query("fcm_token") String fcm_token);

    @POST("addamounttowallet?")
    Call<AddAmountToWalletResponse>addAmountToWallet(@Query("user_id") int user_id,
                                                     @Query("amount") double amount,
                                                     @Query("date") String date,
                                                     @Query("gst") double gst);

    @POST("consultant/disconnected_by_user?")
    Call<CallDisconnectResponse>callDisconnect(@Query("consultant_id") String consultant_id);

    @POST(BuildConfig.API_SERVER_IP+"consultant/end_call?")
    Call<ResponseBody>endCall(@Query("user_id") int user_id,@Query("consultant_id") int consultant_id,
                              @Query("date") String date, @Query("call_duration") String call_duration, @Query("call_medium") String call_medium,
                              @Query("start_time") String start_time, @Query("end_time") String end_time,@Query("call_id") String call_id);


    @POST("amount_verify?")
    Call<VerifyAmountResponse>verifyAmount(@Query("user_id") int user_id,
                                                @Query("user_transaction_id") int user_transaction_id,
                                                @Query("razorpay_payment_id") String razorpay_payment_id,
                                                @Query("razorpay_order_id") String razorpay_order_id);

}