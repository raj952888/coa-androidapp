package com.example.kotlinprepration.network

import android.content.Context
import androidx.databinding.library.BuildConfig
import com.poly.coa.Util.Constants
import com.poly.coa.network.RestApiInterface
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created on : Feb 25, 2019
 */
object RestClient {
    private var retrofit: Retrofit? = null

    //    private static String BASE_URL = "https://furoapi.tk/api/";
    /*private static String BASE_URL = "http://facesolution.in/taccobell/public/api/";*/
    private const val BASE_URL = Constants.BASE_URL_API_STAGE
    val apiService: RestApiInterface
        get() {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttp())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit!!.create(
                RestApiInterface::class.java
            )
        }

    private fun okHttp(): OkHttpClient {
        var context: Context

// set your desired log level
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.NONE
        return if (BuildConfig.DEBUG) {
            OkHttpClient().newBuilder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES) /* .addInterceptor(new Interceptor() {
                             @Override
                             public Response intercept(Chain chain) throws IOException {
                                 Request original = chain.request();
                                */
                /* String accessTok = FuroPrefs.getString(context, "accessToken");
                                Log.i("accesstoken",accessTok);
                                Request newRequest = original.newBuilder()
                                        .header("Authentication", accessTok)
                                        .build();*/
                /*


                 */
                /*  return chain.proceed(newRequest);*/ /*
                            }
                        })*/
                .addInterceptor(logging)
                .build()
        } else {
            OkHttpClient().newBuilder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .build()
        }
    }
}