package com.poly.coa.network;

import android.content.Context;


import com.poly.coa.BuildConfig;
import com.poly.coa.Util.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created on : Feb 25, 2019
 */
public class RetrofitInstance {
    private static Retrofit retrofit = null;
    //    private static String BASE_URL = "https://furoapi.tk/api/";
  /*private static String BASE_URL = "http://facesolution.in/taccobell/public/api/";*/

   private static String BASE_URL = Constants.BASE_URL_API_STAGE;


    public static RestApiService getApiService() {
        if (retrofit == null) {


            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttp())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();


        }
        return retrofit.create(RestApiService.class);

    }

    private static OkHttpClient okHttp() {
        Context context;

// set your desired log level

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.NONE);

        if (BuildConfig.DEBUG) {
            return new OkHttpClient().newBuilder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    /* .addInterceptor(new Interceptor() {
                         @Override
                         public Response intercept(Chain chain) throws IOException {
                             Request original = chain.request();
                            *//* String accessTok = FuroPrefs.getString(context, "accessToken");
                            Log.i("accesstoken",accessTok);
                            Request newRequest = original.newBuilder()
                                    .header("Authentication", accessTok)
                                    .build();*//*


             *//*  return chain.proceed(newRequest);*//*
                        }
                    })*/
                    .addInterceptor(logging)
                    .build();


        } else {
            return new OkHttpClient().newBuilder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .build();
        }

    }

}