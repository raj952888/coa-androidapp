package com.poly.coa.view;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.poly.coa.R;
import com.poly.coa.Util.BaseActivityNew;
import com.poly.coa.Util.CoaPrefs;
import com.poly.coa.Util.CommonMethod;
import com.poly.coa.Util.FileUtils1;
import com.poly.coa.databinding.ActivityUpdateProfileNewBinding;
import com.poly.coa.model.updateProfile.UpdateProfileResponse;
import com.poly.coa.model.userDetailResponse.UserDetailResponse;
import com.poly.coa.network.RestApiService;
import com.poly.coa.network.RetrofitInstance;
import com.example.kotlinprepration.network.RestClient;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AccountDetailActivity extends BaseActivityNew {
    TimePickerDialog.OnTimeSetListener timeSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListenerEndTime;
    public static final int REQUEST_IMAGE = 200;
    ActivityUpdateProfileNewBinding binding;
    private File imgFile;
    RadioButton radioButton;
    String imageUrl;
    ImageView imageViewimageicon;
    ProgressDialog mProgressDialog;
    int healer_id;


    int typeId;


    private String genderTxt = "Male", mobileNumber;
    int user_id;


    @Override
    public int getLayoutId() {
        return R.layout.activity_update_profile_new;
    }

    @Override
    public void initView() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutId());


        imageViewimageicon = findViewById(R.id.imageicon);
        mobileNumber = CoaPrefs.getString(getApplicationContext(), "mobileNumber");

        user_id =  CoaPrefs.getInt(getApplicationContext(),"user_id",0);


        getUserDetail();
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void initViews() {


        binding.imageUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImagePickerOptions();

            }
        });


        binding.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = findViewById(checkedId);
                genderTxt = radioButton.getText().toString();
            }
        });

        binding.tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(binding.fullname.getText().toString())) {
                    Toast.makeText(AccountDetailActivity.this, "Please enter name", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(binding.emailId.getText().toString())) {
                    Toast.makeText(AccountDetailActivity.this, "Please enter email", Toast.LENGTH_SHORT).show();
                    return;

                }
                callUpdateApi();

            }
        });
    }

    public void OnTakePermission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {


                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            Toast.makeText(AccountDetailActivity.this, "Please grant permission to continue...", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {
                    // You can update this bitmap to your server
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), uri);

                    // loading profile image from local cache
                    loadProfile(uri.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private void loadProfile(String url) {


        imageViewimageicon.setVisibility(View.GONE);
        Glide.with(activity()).load(url)
                .apply(RequestOptions.placeholderOf(R.drawable.ic_user)
                        .circleCrop()
                        .dontAnimate().error(R.drawable.ic_user)).into(binding.imageUser);

        imageUrl = url;
        Toast.makeText(this, ""+url, Toast.LENGTH_SHORT).show();



    }

    private void callUpdateApi() {
        if (imageUrl == null) {
            Toast.makeText(this, "Please select image", Toast.LENGTH_SHORT).show();
            return;
        } else {
            imageViewimageicon.setVisibility(View.GONE);
            String path = FileUtils1.getPath(getApplicationContext(), Uri.parse(imageUrl));
            Toast.makeText(this, ""+path, Toast.LENGTH_SHORT).show();

            imgFile = new File(path);
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "" + binding.fullname.getText().toString().trim());
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"), "" + binding.emailId.getText().toString().trim());
            RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), "" + genderTxt);
            RequestBody userid = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(user_id));
            RequestBody profileImg = RequestBody.create(MediaType.parse("image/*"), imgFile);
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part profilePic =
                    MultipartBody.Part.createFormData("image/*", imgFile.getName(), profileImg);


            RestApiService apiService = RetrofitInstance.getApiService();
            showLoadingNew();
            Call<UpdateProfileResponse> artistResponseCall = apiService.editUserDetails(name, userid, email, gender, profilePic);
            artistResponseCall.enqueue(new Callback<UpdateProfileResponse>() {
                @Override
                public void onResponse(Call<UpdateProfileResponse> call, retrofit2.Response<UpdateProfileResponse> response) {

                    if (response.body() != null) {
                        hideLoadingNew();

                        if (response.body().getStatus() == true) {
                            Toast.makeText(getApplicationContext(), "Profile details Upload successfully", Toast.LENGTH_SHORT).show();

                            getUserDetail();

                        } else if (response.body().getStatus() == false) {
                            Toast.makeText(getApplicationContext(), "Something went wrong !! !!", Toast.LENGTH_SHORT).show();

                        }


                    }
                }

                @Override
                public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Failure!! ", Toast.LENGTH_SHORT).show();

                    hideLoading();


                }
            });


        }


    }


   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, AccountDetailActivity.this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                imgFile = imageFiles.get(0);
                imageViewimageicon.setVisibility(View.GONE);
                Glide.with(activity()).load(Uri.fromFile(imgFile))
                        .apply(RequestOptions.placeholderOf(R.drawable.ic_user)
                                .circleCrop()
                                .dontAnimate().error(R.drawable.ic_user)).into(binding.imageUser);

            }


            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {

            }
        });
    }
*/
    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(getApplicationContext(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(getApplicationContext(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void getUserDetail() {


        RestApiService apiService = RetrofitInstance.getApiService();
        showLoadingNew();
        Call<UserDetailResponse> artistResponseCall = apiService.updateUser(mobileNumber);
        artistResponseCall.enqueue(new Callback<UserDetailResponse>() {
            @Override
            public void onResponse(Call<UserDetailResponse> call, retrofit2.Response<UserDetailResponse> response) {

                if (response.body() != null) {
                    hideLoadingNew();

                    if (response.body().getStatus() == true) {

                            binding.fullname.setText(response.body().getData().get(0).getFullName());
                            binding.emailId.setText(response.body().getData().get(0).getEmail());
                            Toast.makeText(AccountDetailActivity.this, ""+response.body().getData().get(0).getProfileImg(), Toast.LENGTH_SHORT).show();
                            Glide.with(activity()).load(response.body().getData().get(0).getProfileImg())
                                    .apply(RequestOptions.placeholderOf(R.drawable.ic_user)
                                            .circleCrop()
                                            .dontAnimate().error(R.drawable.ic_user)).into(binding.imageUser);
                            imageViewimageicon.setVisibility(View.GONE);
                            if(response.body().getData().get(0).getGender().equalsIgnoreCase("Male")){
                                binding.radioMale.setChecked(true);
                            }else{
                                binding.radioFemale.setChecked(true);
                            }
                        CoaPrefs.putString(getApplicationContext(),"name",response.body().getData().get(0).getFullName());


                    } else if (response.body().getStatus() == false) {
                        Toast.makeText(getApplicationContext(), "Something went wrong !! !!", Toast.LENGTH_SHORT).show();

                    }


                }
            }

            @Override
            public void onFailure(Call<UserDetailResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure!! ", Toast.LENGTH_SHORT).show();

                hideLoading();


            }
        });


    }


    public void showLoadingNew() {
        hideLoading();
        mProgressDialog = CommonMethod.showLoadingDialog(this);
    }

    public void hideLoadingNew() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }
}