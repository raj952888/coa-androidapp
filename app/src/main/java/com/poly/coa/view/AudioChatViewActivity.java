package com.poly.coa.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.kotlinprepration.network.RestClient;
import com.poly.coa.R;
import com.poly.coa.Util.CallServiceNew;
import com.poly.coa.Util.CoaPrefs;
import com.poly.coa.Util.NotificationUtil;
import com.poly.coa.model.callDisconnectResponse.CallDisconnectResponse;
import com.poly.coa.model.onetoone.VideoCall;
import com.poly.coa.model.userDetailResponse.UserDetailResponse;
import com.poly.coa.network.RestApiService;
import com.poly.coa.network.RetrofitInstance;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.poly.coa.Util.BaseActivityNew.isInternetConnected;

public class AudioChatViewActivity extends AppCompatActivity {
    private static final String TAG = AudioChatViewActivity.class.getSimpleName();
    public static final String NOTIFICATION_CHANNEL_ID = "10001" ;
    private final static String default_notification_channel_id = "default" ;
    NotificationManager mNotificationManager ;
    int notificationId = 180 ;
    private static final int PERMISSION_REQ_ID = 22;
    TextView textViewTimer;
    CountDownTimer callCountDownTimer;
    String startTime;
    // Permission WRITE_EXTERNAL_STORAGE is not mandatory
    // for Agora RTC SDK, just in case if you wanna save
    // logs to external sdcard.
    private static final String[] REQUESTED_PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    long millisStartTime,longMiliesEndtime;

    private RtcEngine mRtcEngine;
    private boolean mCallEnd;
    private boolean mMuted;
    private boolean mMutedVideo;

    private FrameLayout mLocalContainer;
    private RelativeLayout mRemoteContainer;
    private VideoCanvas mLocalVideo;
    private VideoCanvas mRemoteVideo;
    String channelId;

    private ImageView mCallBtn;
    private ImageView mMuteBtn,imgSpeaker;
    private ImageView mMuteVideoBtn;
    private ImageView mSwitchCameraBtn;
    boolean isOn=false;

    // Customized logger view

    /**
     * Event handler registered into RTC engine for RTC callbacks.
     * Note that UI operations needs to be in UI thread because RTC
     * engine deals with the events in a separate thread.
     */
    private final IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() {
        /**
         * Occurs when the local user joins a specified channel.
         * The channel name assignment is based on channelName specified in the joinChannel method.
         * If the uid is not specified when joinChannel is called, the server automatically assigns a uid.
         *
         * @param channel Channel name.
         * @param uid User ID.
         * @param elapsed Time elapsed (ms) from the user calling joinChannel until this callback is triggered.
         */
        @Override
        public void onJoinChannelSuccess(String channel, final int uid, int elapsed) {
            runOnUiThread(new Runnable() {
                @SuppressLint("LongLogTag")
                @Override
                public void run() {
                    Log.d("Join channel success, uid: ", "" + (uid & 0xFFFFFFFFL));
                }
            });
        }

        /**
         * Occurs when the first remote video frame is received and decoded.
         * This callback is triggered in either of the following scenarios:
         *
         *     The remote user joins the channel and sends the video stream.
         *     The remote user stops sending the video stream and re-sends it after 15 seconds. Possible reasons include:
         *         The remote user leaves channel.
         *         The remote user drops offline.
         *         The remote user calls the muteLocalVideoStream method.
         *         The remote user calls the disableVideo method.
         *
         * @param uid User ID of the remote user sending the video streams.
         * @param width Width (pixels) of the video stream.
         * @param height Height (pixels) of the video stream.
         * @param elapsed Time elapsed (ms) from the local user calling the joinChannel method until this callback is triggered.
         */
        @Override
        public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
            runOnUiThread(new Runnable() {
                @SuppressLint("LongLogTag")
                @Override
                public void run() {
                    Log.d("First remote video decoded", "uid: " + (uid & 0xFFFFFFFFL));
                    setupRemoteVideo(uid);
                }
            });
        }

        /**
         * Occurs when a remote user (Communication)/host (Live Broadcast) leaves the channel.
         *
         * There are two reasons for users to become offline:
         *
         *     Leave the channel: When the user/host leaves the channel, the user/host sends a
         *     goodbye message. When this message is received, the SDK determines that the
         *     user/host leaves the channel.
         *
         *     Drop offline: When no data packet of the user or host is received for a certain
         *     period of time (20 seconds for the communication profile, and more for the live
         *     broadcast profile), the SDK assumes that the user/host drops offline. A poor
         *     network connection may lead to false detections, so we recommend using the
         *     Agora RTM SDK for reliable offline detection.
         *
         * @param uid ID of the user or host who leaves the channel or goes offline.
         * @param reason Reason why the user goes offline:
         *
         *     USER_OFFLINE_QUIT(0): The user left the current channel.
         *     USER_OFFLINE_DROPPED(1): The SDK timed out and the user dropped offline because no data packet was received within a certain period of time. If a user quits the call and the message is not passed to the SDK (due to an unreliable channel), the SDK assumes the user dropped offline.
         *     USER_OFFLINE_BECOME_AUDIENCE(2): (Live broadcast only.) The client role switched from the host to the audience.
         */
        @Override
        public void onUserOffline(final int uid, int reason) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d("User offline", " uid: " + (uid & 0xFFFFFFFFL));
                    onRemoteUserLeft(uid);
                }
            });
        }
    };
    private String healerId;
    private String channel_name = "demo";
    private String token;

    private void setupRemoteVideo(int uid) {
        ViewGroup parent = mRemoteContainer;
        if (parent.indexOfChild(mLocalVideo.view) > -1) {
            parent = mLocalContainer;
        }

        // Only one remote video view is available for this
        // tutorial. Here we check if there exists a surface
        // view tagged as this uid.
        if (mRemoteVideo != null) {
            return;
        }

        /*
          Creates the video renderer view.
          CreateRendererView returns the SurfaceView type. The operation and layout of the view
          are managed by the app, and the Agora SDK renders the view provided by the app.
          The video display view must be created using this method instead of directly
          calling SurfaceView.
         */
        SurfaceView view = RtcEngine.CreateRendererView(getBaseContext());
        view.setZOrderMediaOverlay(parent == mLocalContainer);
        parent.addView(view);
        mRemoteVideo = new VideoCanvas(view, VideoCanvas.RENDER_MODE_HIDDEN, uid);
        // Initializes the video view of a remote user.
        mRtcEngine.setupRemoteVideo(mRemoteVideo);
    }

    private void onRemoteUserLeft(int uid) {
        if (mRemoteVideo != null && mRemoteVideo.uid == uid) {
            removeFromParent(mRemoteVideo);
            // Destroys remote view
            mRemoteVideo = null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_audio_chat_view);
        millisStartTime = System.currentTimeMillis();

        startTime = DateFormat.format("HH:mm", millisStartTime).toString();


        textViewTimer = findViewById(R.id.timer);
        initUI();

        int getPerSessionCharges =  CoaPrefs.getInt(getApplicationContext(), "getpersessioncharge",0);
        int walletAmount =  CoaPrefs.getInt(getApplicationContext(),"walletAmount",0);
        IntentFilter filter = new IntentFilter();
        filter.addAction("custom.notification.navigation");
        registerReceiver(broadcastReceiver, filter);
        long videoMinutes =  walletAmount/getPerSessionCharges;




        callCountDownTimer =  new CountDownTimer(videoMinutes*60*1000, 1000) {

            public void onTick(long millis) {
                String time =String.format("%02d:%02d:%02d",
                        TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                        TimeUnit.MILLISECONDS.toSeconds(millis) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                textViewTimer.setText("Remaining Time: "+time +" "+"min");
            }

            public void onFinish() {
                callDisconnect();

            }

        };


        callCountDownTimer.start();



       /* healerId = getIntent().getStringExtra("healer_id");
        channel_name = "" + getIntent().getStringExtra("channel_name");
        token = "" + getIntent().getStringExtra("token");*/
        channelId = CoaPrefs.getString(getApplicationContext(), "channelId");
        token = CoaPrefs.getString(getApplicationContext(), "token");
        String name = CoaPrefs.getString(getApplicationContext(), "consultantname");
        TextView textView = findViewById(R.id.name);
        textView.setText(name);

        initEngineAndJoinChannel();
        //oneToOne();

        // Ask for permissions at runtime.
        // This is just an example set of permissions. Other permissions
        // may be needed, and please refer to our online documents.
        if (checkSelfPermission(REQUESTED_PERMISSIONS[0], PERMISSION_REQ_ID) &&
                checkSelfPermission(REQUESTED_PERMISSIONS[1], PERMISSION_REQ_ID) &&
                checkSelfPermission(REQUESTED_PERMISSIONS[2], PERMISSION_REQ_ID)) {
            initEngineAndJoinChannel();
        }
    }

    private void initUI() {
        mLocalContainer = findViewById(R.id.local_video_view_container);
        mRemoteContainer = findViewById(R.id.remote_video_view_container);

        mCallBtn = findViewById(R.id.btn_call);
        mMuteBtn = findViewById(R.id.btn_mute);
        mMuteVideoBtn = findViewById(R.id.live_btn_mute_video);
        mSwitchCameraBtn = findViewById(R.id.btn_switch_camera);

        imgSpeaker = findViewById(R.id.imgSpeaker);

    }


    private boolean checkSelfPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, REQUESTED_PERMISSIONS, requestCode);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQ_ID) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED ||
                    grantResults[1] != PackageManager.PERMISSION_GRANTED ||
                    grantResults[2] != PackageManager.PERMISSION_GRANTED) {
                showLongToast("Need permissions " + Manifest.permission.RECORD_AUDIO +
                        "/" + Manifest.permission.CAMERA + "/" + Manifest.permission.WRITE_EXTERNAL_STORAGE);
                finish();
                return;
            }

            // Here we continue only if all permissions are granted.
            // The permissions can also be granted in the system settings manually.
            initEngineAndJoinChannel();
        }
    }

    private void showLongToast(final String msg) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initEngineAndJoinChannel() {
        // This is our usual steps for joining
        // a channel and starting a call.
        initializeEngine();
       // setupVideoConfig();
        //setupLocalVideo();
        joinChannel();
    }

    private void initializeEngine() {
        try {
            mRtcEngine = RtcEngine.create(getBaseContext(), getString(R.string.agora_app_id), mRtcEventHandler);
            mRtcEngine.muteLocalVideoStream(true);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }

    private void setupVideoConfig() {
        // In simple use cases, we only need to enable video capturing
        // and rendering once at the initialization step.
        // Note: audio recording and playing is enabled by default.
        mRtcEngine.enableVideo();

        // Please go to this page for detailed explanation
        // https://docs.agora.io/en/Video/API%20Reference/java/classio_1_1agora_1_1rtc_1_1_rtc_engine.html#af5f4de754e2c1f493096641c5c5c1d8f
        mRtcEngine.setVideoEncoderConfiguration(new VideoEncoderConfiguration(
                VideoEncoderConfiguration.VD_640x360,
                VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT));
    }

    private void setupLocalVideo() {
        // This is used to set a local preview.
        // The steps setting local and remote view are very similar.
        // But note that if the local user do not have a uid or do
        // not care what the uid is, he can set his uid as ZERO.
        // Our server will assign one and return the uid via the event
        // handler callback function (onJoinChannelSuccess) after
        // joining the channel successfully.
        SurfaceView view = RtcEngine.CreateRendererView(getBaseContext());
        view.setZOrderMediaOverlay(true);
        mLocalContainer.addView(view);
        // Initializes the local video view.
        // RENDER_MODE_HIDDEN: Uniformly scale the video until it fills the visible boundaries. One dimension of the video may have clipped contents.
        mLocalVideo = new VideoCanvas(view, VideoCanvas.RENDER_MODE_HIDDEN, 0);
        mRtcEngine.setupLocalVideo(mLocalVideo);
    }

    private void joinChannel() {
        // 1. Users can only see each other after they join the
        // same channel successfully using the same app id.
        // 2. One token is only valid for the channel name that
        // you use to generate this token.

        if (TextUtils.isEmpty(token) || TextUtils.equals(token, "#YOUR ACCESS TOKEN#")) {
            token = null; // default, no token
        }

        // Log.d("Raj", channel_name);
        mRtcEngine.joinChannel(token, channelId, "Extra Optional Data", 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unregisterReceiver(broadcastReceiver);
//        if (!mCallEnd) {
//            leaveChannel();
//        }
//        /*
//          Destroys the RtcEngine instance and releases all resources used by the Agora SDK.
//
//          This method is useful for apps that occasionally make voice or video calls,
//          to free up resources for other operations when not making calls.
//         */
//        RtcEngine.destroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void leaveChannel() {
        NotificationUtil.removeNotification(this,notificationId);
        mRtcEngine.leaveChannel();

        endCallApi(getIntent().getStringExtra("call_id"));

    }

    private void endCallWithApi(String call_id) {
        finish();

    }

    public void onLocalAudioMuteClicked(View view) {
        mMuted = !mMuted;
        // Stops/Resumes sending the local audio stream.
        mRtcEngine.muteLocalAudioStream(mMuted);
        int res = mMuted ? R.drawable.ic_iconly_light_voice_1 : R.drawable.ic_voice;
        mMuteBtn.setImageResource(res);
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void endCallApi(String call_id) {



        String date = DateFormat.format("yyyy-MM-dd", System.currentTimeMillis()).toString();
        longMiliesEndtime = System.currentTimeMillis();

        String endTime= DateFormat.format("HH:mm", longMiliesEndtime).toString();

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());


        long mills = ((System.currentTimeMillis()-millisStartTime) / 1000) / 60;;

        int consultant_id = CoaPrefs.getInt(getApplicationContext(), "consultantId",0);

        int userId = CoaPrefs.getInt(getApplicationContext(), "user_id", 0);
        RestApiService apiService = RetrofitInstance.getApiService();
        Call<ResponseBody> artistResponseCall = apiService.endCall(userId,consultant_id,date, String.valueOf(mills),"video", startTime,endTime,call_id);
        artistResponseCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.body() != null) {
//                    finish();
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags( Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                    finish();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure!! ", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void onSwitchSpeakerphoneClicked(View view) {
        ImageView iv = (ImageView) view;
        if (iv.isSelected()) {
            iv.setSelected(false);
            iv.clearColorFilter();
        } else {
            iv.setSelected(true);
            iv.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        // Enables/Disables the audio playback route to the speakerphone.
        //
        // This method sets whether the audio is routed to the speakerphone or earpiece. After calling this method, the SDK returns the onAudioRouteChanged callback to indicate the changes.
        mRtcEngine.setEnableSpeakerphone(view.isSelected());
    }

    public void onSpeackerClick(View view) {
        AudioManager mAudioMgr = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

        if(isOn){
            mAudioMgr.setMode(AudioManager.MODE_IN_CALL);
            mAudioMgr.setMode(AudioManager.MODE_NORMAL);
            isOn=false;
            imgSpeaker.setImageResource(R.drawable.ic_volume_off);
            mAudioMgr.setMode(AudioManager.MODE_IN_COMMUNICATION);

        }else{
            mAudioMgr.setMode(AudioManager.MODE_IN_COMMUNICATION);
            mAudioMgr.setMode(AudioManager.MODE_NORMAL);
            mAudioMgr.setMode(AudioManager.MODE_IN_CALL);
            isOn=true;
            imgSpeaker.setImageResource(R.drawable.ic_volume_on);
        }
        mAudioMgr.setSpeakerphoneOn(isOn);
    }

    public void onLocalVideoMuteClicked(View view) {
        mMutedVideo = !mMutedVideo;
        // Stops/Resumes sending the local audio stream.
        mRtcEngine.muteLocalVideoStream(mMutedVideo);
        int res = mMutedVideo ? R.drawable.ic_iconly_light_video : R.drawable.ic_video_close;
        mMuteVideoBtn.setImageResource(res);
    }

    public void onSwitchCameraClicked(View view) {
        // Switches between front and rear cameras.
        mRtcEngine.switchCamera();
    }

    public void onCallClicked(View view) {
        if (mCallEnd) {
            startCall();
            mCallEnd = false;

        } else {
            callDisconnect();
        }

        showButtons(!mCallEnd);
    }

    private void startCall() {
        setupLocalVideo();
        joinChannel();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void endCall() {
        removeFromParent(mLocalVideo);
        mLocalVideo = null;
        removeFromParent(mRemoteVideo);
        mRemoteVideo = null;
        leaveChannel();
    }

    private void showButtons(boolean show) {
        int visibility = show ? View.VISIBLE : View.GONE;
        mMuteBtn.setVisibility(visibility);

    }

    private ViewGroup removeFromParent(VideoCanvas canvas) {
        if (canvas != null) {
            ViewParent parent = canvas.view.getParent();
            if (parent != null) {
                ViewGroup group = (ViewGroup) parent;
                group.removeView(canvas.view);
                return group;
            }
        }
        return null;
    }

    private void switchView(VideoCanvas canvas) {
        ViewGroup parent = removeFromParent(canvas);
        if (parent == mLocalContainer) {
            if (canvas.view instanceof SurfaceView) {
                ((SurfaceView) canvas.view).setZOrderMediaOverlay(false);
            }
            mRemoteContainer.addView(canvas.view);
        } else if (parent == mRemoteContainer) {
            if (canvas.view instanceof SurfaceView) {
                ((SurfaceView) canvas.view).setZOrderMediaOverlay(true);
            }
            mLocalContainer.addView(canvas.view);
        }
    }

    public void onLocalContainerClick(View view) {
        switchView(mLocalVideo);
        switchView(mRemoteVideo);
    }

   /* public void callDisconnect() {
        // if (isInternetConnected(this)) {

        // showLoading();

        final RequestBody healer_id = RequestBody.create(MediaType.parse("text/plain"), "" + healerId);

        RestClient.disconnect(healer_id, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                // hideLoading();
                if (response.body() != null) {
                    endCall();
                    mCallEnd = true;
                    mCallBtn.setImageResource(R.drawable.btn_startcall);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //TODo Error handling
            }
        });

    }*/

    BroadcastReceiver broadcastReceiver =
            new BroadcastReceiver() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent.hasExtra("action") && intent.getStringExtra("action").equalsIgnoreCase("disconnected")) {
                        endCall();
                        mCallEnd = true;
                    }
                }
            };




    private void callDisconnect() {
        int userId = CoaPrefs.getInt(getApplicationContext(), "user_id", 0);
        int consultantId =  CoaPrefs.getInt(getApplicationContext(), "consultantId",0);
        RestApiService apiService = RetrofitInstance.getApiService();
        Call<CallDisconnectResponse> artistResponseCall = apiService.callDisconnect(String.valueOf(consultantId));
        artistResponseCall.enqueue(new Callback<CallDisconnectResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<CallDisconnectResponse> call, retrofit2.Response<CallDisconnectResponse> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == true) {
                        endCall();
                        mCallEnd = true;

                    } else if (response.body().getStatus() == false) {
                        Toast.makeText(getApplicationContext(), "Something went wrong !! !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CallDisconnectResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure!! ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        if(!mCallEnd)
//        createNotification();
        moveTaskToBack(true);
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);

    }


    @Override
    protected void onPause() {

        if(!mCallEnd) {
            Intent inCallServiceIntent = new Intent(this, CallServiceNew.class);
            inCallServiceIntent.putExtra("data",getIntent());
            inCallServiceIntent.putExtra("callType","COA-audio call in progress");
            startService(inCallServiceIntent);

            NotificationUtil.createNotification(this, notificationId, getIntent(),"COA-audio call in progress");
        }


        super.onPause();
    }

    @Override
    protected void onResume() {
        NotificationUtil.removeNotification(this,notificationId);
        super.onResume();
    }


}
