package com.lyxel.bringin

import android.app.AlertDialog
import android.app.FragmentManager
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.poly.coa.R
import com.poly.coa.Util.CommonMethod
import com.poly.coa.Util.SharedPreferenceUtility
import com.github.pwittchen.networkevents.library.BusWrapper
import com.github.pwittchen.networkevents.library.ConnectivityStatus
import com.github.pwittchen.networkevents.library.NetworkEvents
import com.github.pwittchen.networkevents.library.logger.Logger
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.squareup.otto.Bus


public abstract class BaseActivity : AppCompatActivity() {

    //    lateinit var mNoNet: NoNet
    var fm: FragmentManager? = null
    private var mProgressDialog: ProgressDialog? = null
    private var mAlertDialog: AlertDialog? = null

    private var busWrapper: BusWrapper? = null
    private var networkEvents: NetworkEvents? = null
    private val MESSAGE_FORMAT = "ConnectivityChanged: %s"
    public var sharedPreferenceUtility: SharedPreferenceUtility? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // comment on 28-07-2021 for showing status bar text color in black
//        window.apply {
//            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//            statusBarColor = Color.TRANSPARENT
//        }

//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        getWindow().setFlags(
//            WindowManager.LayoutParams.FLAG_FULLSCREEN,
//            WindowManager.LayoutParams.FLAG_FULLSCREEN
//        );

        sharedPreferenceUtility = SharedPreferenceUtility(this)

        fm = fragmentManager
//        mNoNet = NoNet()
//        mNoNet.initNoNet(this, fm)

        busWrapper = getOttoBusWrapper(Bus())
        networkEvents = NetworkEvents(
            applicationContext,
            busWrapper,
            Logger { message -> // log your message here
//                Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
//                String messageFormat = String.format(MESSAGE_FORMAT,message);
                if (String.format(MESSAGE_FORMAT, ConnectivityStatus.OFFLINE) == message) {
                    showSnackBar("No internet connection. Make sure Wi-Fi or cellular data is turned on, then try again")
                } else if (String.format(
                        MESSAGE_FORMAT,
                        ConnectivityStatus.WIFI_CONNECTED_HAS_NO_INTERNET
                    ) == message
                ) {
                    showSnackBar("connected to WiFi (Internet not available)")
                } else {

                }
            })
    }


    override fun onResume() {
        super.onResume()

        busWrapper!!.register(applicationContext)
        networkEvents!!.register()
    }

    override fun onPause() {

        super.onPause()
        busWrapper!!.unregister(applicationContext)
        networkEvents!!.unregister()
    }

    fun getOttoBusWrapper(bus: Bus): BusWrapper? {
        return object : BusWrapper {
            override fun register(`object`: Any) {
                bus.register(`object`)
            }

            override fun unregister(`object`: Any) {
                bus.unregister(`object`)
            }

            override fun post(event: Any) {
                bus.post(event)
            }
        }
    }

    fun showLoading() {
        hideLoading();
        mProgressDialog = CommonMethod.showLoadingDialog(this)
    }

    fun hideLoading() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing()) {
            mProgressDialog!!.cancel()
        }
    }



    fun hideLoadingAlert() {
        if (mAlertDialog != null && mAlertDialog!!.isShowing()) {
            mAlertDialog!!.cancel()
        }
    }

    public fun showSnackBar(message: String) {
        val snackbar = Snackbar.make(
            findViewById(android.R.id.content),
            message, Snackbar.LENGTH_SHORT
        )

        val sbView = snackbar.getView()
        val textView = sbView.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(this, R.color.black))
        sbView.setBackgroundColor(Color.parseColor("#D3D3D3"));
        snackbar.show()
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showKeyboard() {
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            0
        )
    }

    fun showMessage(message: String?) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_LONG)
                .show()
        }
    }



    fun showAlertDialog(message: String) {
        // build alert dialog
        val dialogBuilder = AlertDialog.Builder(this)

        // set message of alert dialog
        dialogBuilder.setMessage(message)
            // if the dialog is cancelable
            .setCancelable(false)
            // positive button text and action
            .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
            })
//                // negative button text and action
//                .setNegativeButton("Cancel", DialogInterface.OnClickListener {
//                    dialog, id -> dialog.cancel()
//                })

        // create dialog box
        val alert = dialogBuilder.create()
        // set title for alert dialog box
        alert.setTitle(R.string.app_name)
        // show alert dialog
        alert.show()

    }

//    fun showDialog(msg: String?) {
//        //Inflate the dialog with custom view
//        val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_alert_kyc, null)
//        //AlertDialogBuilder
//        val mBuilder = AlertDialog.Builder(this)
//                .setView(mDialogView)
//
//        //show dialog
//        val  mAlertDialog = mBuilder.show()
//        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
//        //login button click of custom layout
//        var textView = mDialogView.findViewById<TextView>(R.id.tabChangetext)
//        textView.setText(msg)
//        //cancel button click of custom layout
//        var txt_ok = mDialogView.findViewById<TextView>(R.id.text_ok)
//
//        txt_ok.text_ok.setOnClickListener {
//            //dismiss dialog
//            mAlertDialog.dismiss()
//        }
//        mAlertDialog.show()
//    }


    fun getVersionName(): String? {
        val context = applicationContext
        val manager = context.packageManager
        var myversionName: String? = null
        myversionName = try {
            val info = manager.getPackageInfo(context.packageName, 0)
            info.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            "Unknown-01"
        }
        return myversionName
    }


}

