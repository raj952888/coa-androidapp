package com.poly.coa.view

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.poly.coa.R
import com.poly.coa.adapter.Image
import com.poly.coa.adapter.SliderAdapterExample
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import kotlinx.android.synthetic.main.activity_choose_time_slot.*
import kotlinx.android.synthetic.main.activity_connect_any_time_from_any_where.*
import kotlinx.android.synthetic.main.activity_connect_any_time_from_any_where.next

class ChooseTimeSlotActivity : AppCompatActivity() {
    var sliderView: SliderView? = null
    var adapter: SliderAdapterExample? = null

    val mNicolasCageMovies = listOf(
        Image(R.drawable.ic_walkthrough_1),
        Image(R.drawable.ic_walkthrough_2),
        Image(R.drawable.ic_walkthrough_3)


    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_time_slot)
        sliderView = findViewById(R.id.imageSlider)


        adapter = SliderAdapterExample(applicationContext, mNicolasCageMovies)
        with(sliderView) {
            this?.setSliderAdapter(adapter)
            this?.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
            this?.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH)
            this?.setIndicatorSelectedColor(Color.WHITE)
            this?.setIndicatorUnselectedColor(Color.GRAY)
            this?.setScrollTimeInSec(6)
            this?.setAutoCycle(true)
            this?.startAutoCycle()
        }
        gotit.setOnClickListener {
            val intent = Intent(this@ChooseTimeSlotActivity, LoginActivity::class.java)
            startActivity(intent)
        }
    }

}