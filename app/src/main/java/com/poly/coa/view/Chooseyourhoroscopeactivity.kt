package com.poly.coa.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import com.poly.coa.R
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.adapter.MenuAdapter
import com.poly.coa.model.horoscope.Datum
import com.poly.coa.model.horoscope.HoroscopeResponse
import com.poly.coa.model.updateHoroscopeResponse.UpdateHoroscopeResponse
import com.poly.coa.model.updateProfile.UpdateProfileResponse
import com.example.kotlinprepration.network.RestClient
import com.google.gson.Gson
import com.lyxel.bringin.BaseActivity
import kotlinx.android.synthetic.main.activity_chooseyourhoroscopeactivity.*
import kotlinx.android.synthetic.main.bottomnavigationdesignforotpverifynew.*
import retrofit2.Call
import retrofit2.Response

class Chooseyourhoroscopeactivity : BaseActivity() {
    var horoscopeId : Int = 0
    var user_Id :Int ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chooseyourhoroscopeactivity)
        user_Id = CoaPrefs.getInt(applicationContext,"user_id",0)
        getHoroscope()

        tv_continue.setOnClickListener {
            updateHoroscope()
        }
    }

    fun getHoroscope() {

        val apiService = RestClient.apiService
        val loginModelCall =
            apiService.getHoroscope()
        showLoading()
        loginModelCall?.enqueue(object : retrofit2.Callback<HoroscopeResponse?> {
            override fun onResponse(
                call: Call<HoroscopeResponse?>,
                response: Response<HoroscopeResponse?>
            ) {

                Log.i("result", Gson().toJson(response.body()))
                try {
                    if (response.isSuccessful) {
                        Log.i("result", Gson().toJson(response.body()))
                        var saveReferenceResponse = response.body()
                        if (saveReferenceResponse!!.status == true) {
                            hideLoading()
                            var list: MutableList<Datum>? = saveReferenceResponse.data
                            var myAdapter = MenuAdapter(list, applicationContext)

                            chooseyourhoroscopsign.setLayoutManager(
                                GridLayoutManager(
                                    applicationContext,
                                    3
                                )
                            )



                            myAdapter.horoscopeList { id ->
                                horoscopeId = id




                            }


                            chooseyourhoroscopsign.setAdapter(myAdapter)


                        } else {
                            showMessage("Something went wrong !!")

                        }

                    } else {

                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<HoroscopeResponse?>, t: Throwable) {
                showSnackBar("Failure !!")

            }


        });

    }

    fun updateHoroscope() {

        if(horoscopeId== 0){
            showSnackBar("Select your Horoscope")

        }else{
            val apiService = RestClient.apiService
            val updateHoroscopeCall =
                apiService.updateHoroscopeSign(user_Id?.toString(),horoscopeId.toString())

            showLoading()
            updateHoroscopeCall?.enqueue(object : retrofit2.Callback<UpdateHoroscopeResponse?> {
                override fun onResponse(
                    call: Call<UpdateHoroscopeResponse?>,
                    response: Response<UpdateHoroscopeResponse?>
                ) {

                    Log.i("result", Gson().toJson(response.body()))
                    try {
                        if (response.isSuccessful) {
                            Log.i("result", Gson().toJson(response.body()))
                            var saveReferenceResponse = response.body()
                            if (saveReferenceResponse!!.status == true) {
                                hideLoading()
                                var intent = Intent(applicationContext, MainActivity::class.java)

                                startActivity(intent)









                            } else {
                                showMessage("Something went wrong !!")

                            }

                        } else {

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<UpdateHoroscopeResponse?>, t: Throwable) {
                    showSnackBar("Failure !!")

                }


            });

        }


    }


}