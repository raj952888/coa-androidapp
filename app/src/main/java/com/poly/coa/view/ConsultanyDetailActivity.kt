package com.poly.coa.view

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.kotlinprepration.network.RestClient
import com.poly.coa.R
import com.poly.coa.model.getAllConsultant.GetAllAstrologerResponse
import com.google.gson.Gson
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.model.homedataresponse.HomeDataResponse
import kotlinx.android.synthetic.main.consultantdetail.*
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Response

class ConsultanyDetailActivity : AppCompatActivity() {
    lateinit var consultant: GetAllAstrologerResponse


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.consultantdetail)
        val gson = Gson()
        consultant = gson.fromJson(
            intent.getStringExtra("consultantList"),
            GetAllAstrologerResponse::class.java
        )

        backbutton.setOnClickListener {
            finish()
        }





        var position = intent.getIntExtra("position", 0)
        consultantName.text = consultant.consultantList.get(position).firstName
        experience.text= ""+consultant.consultantList.get(position).experience+" "+"years of experience"
        language.text=""+consultant.consultantList.get(position).language
        chatPrice.text=""+consultant.consultantList.get(position).per_minute_charge+"/ min"
        videoCallPrice.text=""+consultant.consultantList.get(position).per_minute_charge+"/ min"
        phoneCallPrice.text=""+consultant.consultantList.get(position).per_minute_charge+"/ min"
        expertice.text=""+consultant.consultantList.get(position).expertise
        specialist.text=""+consultant.consultantList.get(position).expertise
        education.text=""+consultant.consultantList.get(position).education

        videoCall.setOnClickListener {

            val amount = CoaPrefs.getInt(applicationContext, "walletAmount", 0)
            if (amount > 100 && consultant.consultantList.get(position).status.equals("online")) {
                val subcategory = gson.toJson(consultant)
                val intent =
                    Intent(applicationContext, PreviewActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("consultantList", subcategory)
                intent.putExtra("position",position)
                startActivity(intent)
            } else {
                if(!consultant.consultantList.get(position).status.equals("online")){


                }else{
                    dialogForLowBalance()
                }

            }
        }

        phonecallid.setOnClickListener {
            val amount = CoaPrefs.getInt(applicationContext, "walletAmount", 0)
            if (amount > 100 && consultant.consultantList.get(position).status.equals("online")) {
                val subcategory = gson.toJson(consultant)
                val intent =
                    Intent(applicationContext, PreviewActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("consultantList", subcategory)
                intent.putExtra("position",position)
                startActivity(intent)
            } else {
                if(!consultant.consultantList.get(position).status.equals("online")){


                }else{
                    dialogForLowBalance()
                }

            }
        }

//        Glide.with(applicationContext)
//            .load(consultant.consultantList.get(position).customImage)
//            .addListener(object : RequestListener<Drawable?> {
//                override fun onLoadFailed(
//                    e: GlideException?,
//                    model: Any,
//                    target: Target<Drawable?>,
//                    isFirstResource: Boolean
//                ): Boolean {
//                    imageView.setImageResource(R.drawable.ic_user)
//                    return false
//                }
//
//                override fun onResourceReady(
//                    resource: Drawable?,
//                    model: Any,
//                    target: Target<Drawable?>,
//                    dataSource: DataSource,
//                    isFirstResource: Boolean
//                ): Boolean {
//                    return false
//                }
//            })
//            .diskCacheStrategy(DiskCacheStrategy.ALL)
//            .skipMemoryCache(true)
//            // scale to fit entire image within ImageView
//            .into(imageView)


        //Glide.with(this).load(data.getImage()).into(binding.profileImg);
        if (!TextUtils.isEmpty(consultant.consultantList.get(position).customImage)) {
            Glide.with(this).load(consultant.consultantList.get(position).customImage)
                .listener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any,
                        target: Target<Drawable?>,
                        isFirstResource: Boolean
                    ): Boolean {
                        Handler().postDelayed({
                            Glide.with(applicationContext).load(consultant.consultantList.get(position).image)
                                .into(imageView)
                        }, 1000)
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any,
                        target: Target<Drawable?>,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }
                }).into(imageView)
        } else if (!TextUtils.isEmpty(consultant.consultantList.get(position).image) && !consultant.consultantList.get(position).image
                .equals("null")
        ) Glide.with(this).load(consultant.consultantList.get(position).image).into(imageView)


//        Glide.with(applicationContext).load(consultant.consultantList.get(position).customImage)
//            .apply(
//                RequestOptions.placeholderOf(R.drawable.ic_user)
//                    .circleCrop()
//                    .dontAnimate().error(R.drawable.ic_user)
//            ).into(imageView)

        bookAnAppointment.setOnClickListener {
          var intent = Intent(applicationContext,BookingAppointmentActivity::class.java)
            startActivity(intent)
        }
    }

    fun dialogForLowBalance() {
        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialogforlowbalance, null)
        //AlertDialogBuilder
        val mBuilder = android.app.AlertDialog.Builder(this)
            .setView(mDialogView)

        //show dialog
        val mAlertDialog = mBuilder.show()
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        //login button click of custom layout
        val noiwanttocontinue = mAlertDialog.findViewById<TextView>(R.id.text_ok)
        val textViewclose = mAlertDialog.findViewById<TextView>(R.id.text_cancel)
        textViewclose.setOnClickListener { mAlertDialog.dismiss() }
        noiwanttocontinue.setOnClickListener {
            val intent =
                Intent(applicationContext, PaymentActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        mAlertDialog.show()
    }


    override fun onResume() {
        super.onResume()
        homeData()
    }


    fun homeData() {
        var user_Id = CoaPrefs.getInt(applicationContext, "user_id", 0)
        val apiService = RestClient.apiService
        val updateHoroscopeCall =
            apiService.homeData(user_Id.toString())
//        showLoading()
        updateHoroscopeCall?.enqueue(object : retrofit2.Callback<HomeDataResponse?> {
            override fun onResponse(
                call: Call<HomeDataResponse?>,
                response: Response<HomeDataResponse?>
            ) {

                Log.i("result", Gson().toJson(response.body()))
                try {
                    if (response.isSuccessful) {
                        Log.i("result", Gson().toJson(response.body()))
                        var saveReferenceResponse = response.body()
                        if (saveReferenceResponse!!.status == true) {
//                            hideLoading()


                            CoaPrefs.putInt(applicationContext,"walletAmount",
                                saveReferenceResponse.walletAmount.toInt()
                            )

                        } else {
//                            showMessage("Something went wrong !!")

                        }

                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<HomeDataResponse?>, t: Throwable) {
//                showMessage("Failure !!")

            }


        });

    }



}