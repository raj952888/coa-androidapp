package com.poly.coa.view

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.kotlinprepration.network.RestClient
import com.google.gson.Gson
import com.lyxel.bringin.BaseActivity
import com.poly.coa.R
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.adapter.GetAstrolozerAdapter
import com.poly.coa.adapter.HomeBookingList
import com.poly.coa.adapter.Image
import com.poly.coa.adapter.SliderAdapterExample
import com.poly.coa.model.getAllConsultant.GetAllAstrologerResponse
import com.poly.coa.model.homedataresponse.HomeDataResponse
import com.poly.coa.model.homedataresponse.TodayBooking
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import kotlinx.android.synthetic.main.activity_get_all_consultant.*
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Response

class GetAllConsultantActivity : BaseActivity() {

    var sliderView: SliderView? = null
    var adapter: SliderAdapterExample? = null
    var getAstrolozerAdapter: GetAstrolozerAdapter? = null

    val mNicolasCageMovies = listOf(
        Image(R.drawable.astrologerimage),
        Image(R.drawable.astrologerimage),
        Image(R.drawable.astrologerimage)


    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_all_consultant)

        sliderView = findViewById(R.id.imageSlider)


        adapter = SliderAdapterExample(applicationContext, mNicolasCageMovies)
        with(sliderView) {
            this?.setSliderAdapter(adapter)
            this?.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
            this?.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH)
            this?.setIndicatorSelectedColor(Color.WHITE)
            this?.setIndicatorUnselectedColor(Color.GRAY)
            this?.setScrollTimeInSec(6)
            this?.setAutoCycle(true)
            this?.startAutoCycle()
        }

        getAllConsultant()

        backButton.setOnClickListener {
            finish()
        }
    }

    fun getAllConsultant() {


            val apiService = RestClient.apiService
            val updateHoroscopeCall =
                apiService.getAllConsultant()
            showLoading()
            updateHoroscopeCall?.enqueue(object : retrofit2.Callback<GetAllAstrologerResponse?> {
                override fun onResponse(
                    call: Call<GetAllAstrologerResponse?>,
                    response: Response<GetAllAstrologerResponse?>
                ) {

                    Log.i("result", Gson().toJson(response.body()))
                    try {
                        if (response.isSuccessful) {
                            Log.i("result", Gson().toJson(response.body()))
                            var saveReferenceResponse = response.body()
                            if (saveReferenceResponse!!.status == true) {
                                hideLoading()
                                title_tv.text= saveReferenceResponse.consultantType.get(0).title
                                val getConsultant: GetAllAstrologerResponse = saveReferenceResponse
                                var  getAstrolozerAdapter = GetAstrolozerAdapter(getConsultant, applicationContext)
                                val layoutManager: RecyclerView.LayoutManager =
                                    LinearLayoutManager(applicationContext)
                                recyclerConsultant.setLayoutManager(layoutManager)
                                recyclerConsultant.setItemAnimator(DefaultItemAnimator())
                                recyclerConsultant.setAdapter(getAstrolozerAdapter)

                                description.text= response.body()!!.consultantType.get(0).description.toString()



                                getAstrolozerAdapter.SubcategoryPrdocutDescriptionlistnew {
                                        position, getAllAstrologerResponse ->

                                    val gson = Gson()
                                    val subcategory = gson.toJson(getAllAstrologerResponse)
                                    val intent = Intent(applicationContext, ConsultanyDetailActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    intent.putExtra("consultantList", subcategory)
                                    intent.putExtra("position",position)
                                    startActivity(intent)

                                }

                              getAstrolozerAdapter.horoscopeList { position, getAllAstrologerResponse ->
                                  val gson = Gson()
                                  val amount = CoaPrefs.getInt(applicationContext, "walletAmount", 0)
                                  if (amount > 100 && getAllAstrologerResponse.consultantList.get(position).status.equals("online")) {
                                      val subcategory = gson.toJson(getAllAstrologerResponse)
                                      val intent =
                                          Intent(applicationContext, PreviewActivity::class.java)
                                      intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                      intent.putExtra("consultantList", subcategory)
                                      intent.putExtra("position",position)
                                      intent.putExtra("callmedium","video")
                                      startActivity(intent)
                                  } else {
                                      if(!getAllAstrologerResponse.consultantList.get(position).status.equals("online")){


                                      }else{
                                          dialogForLowBalance()
                                      }

                                  }

                              }

                                getAstrolozerAdapter.SubcategoryPrdocutDescriptionlist { position, getAllAstrologerResponse ->
                                    val gson = Gson()
                                    val amount = CoaPrefs.getInt(applicationContext, "walletAmount", 0)
                                    if (amount > 100 && getAllAstrologerResponse.consultantList.get(position).status.equals("online")) {
                                        val subcategory = gson.toJson(getAllAstrologerResponse)
                                        val intent =
                                            Intent(applicationContext, PreviewActivity::class.java)
                                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                        intent.putExtra("consultantList", subcategory)
                                        intent.putExtra("position",position)
                                        intent.putExtra("callmedium","audio")
                                        startActivity(intent)
                                    } else {
                                        if(!getAllAstrologerResponse.consultantList.get(position).status.equals("online")){


                                        }else{
                                            dialogForLowBalance()
                                        }

                                    }


                                }







                            } else {
                                showMessage("Something went wrong !!")

                            }

                        } else {

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<GetAllAstrologerResponse?>, t: Throwable) {
                    showSnackBar("Failure !!")

                }


            });

        }
    fun dialogForLowBalance() {
        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialogforlowbalance, null)
        //AlertDialogBuilder
        val mBuilder = android.app.AlertDialog.Builder(this)
            .setView(mDialogView)

        //show dialog
        val mAlertDialog = mBuilder.show()
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        //login button click of custom layout
        val noiwanttocontinue = mAlertDialog.findViewById<TextView>(R.id.text_ok)
        val textViewclose = mAlertDialog.findViewById<TextView>(R.id.text_cancel)
        textViewclose.setOnClickListener { mAlertDialog.dismiss() }
        noiwanttocontinue.setOnClickListener {
            val intent =
                Intent(applicationContext, PaymentActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        mAlertDialog.show()
    }

    override fun onResume() {
        super.onResume()
        homeData()
    }


    fun homeData() {
       var user_Id = CoaPrefs.getInt(applicationContext, "user_id", 0)
        val apiService = RestClient.apiService
        val updateHoroscopeCall =
            apiService.homeData(user_Id.toString())
//        showLoading()
        updateHoroscopeCall?.enqueue(object : retrofit2.Callback<HomeDataResponse?> {
            override fun onResponse(
                call: Call<HomeDataResponse?>,
                response: Response<HomeDataResponse?>
            ) {

                Log.i("result", Gson().toJson(response.body()))
                try {
                    if (response.isSuccessful) {
                        Log.i("result", Gson().toJson(response.body()))
                        var saveReferenceResponse = response.body()
                        if (saveReferenceResponse!!.status == true) {
//                            hideLoading()


                            CoaPrefs.putInt(applicationContext,"walletAmount",
                                saveReferenceResponse.walletAmount.toInt()
                            )

                        } else {
//                            showMessage("Something went wrong !!")

                        }

                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<HomeDataResponse?>, t: Throwable) {
//                showMessage("Failure !!")

            }


        });

    }




}