package com.poly.coa.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.poly.coa.R;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IncomingCallActivity extends AppCompatActivity {
    IncomingCallByAaranyaReciever reciever = null;
    private MediaPlayer music;
    String healer_id;
    private String channel_name;
    private String token;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incoming_call);


        ImageView profile_img = findViewById(R.id.profile_img);
        TextView nameTV = findViewById(R.id.name);

        IntentFilter filter = new IntentFilter();
        filter.addAction("custom.notification.navigation");
        registerReceiver(broadcastReceiver, filter);
        Intent intent = getIntent();

        final String name = intent.getStringExtra("name");
        final String image = intent.getStringExtra("image");
        healer_id = intent.getStringExtra("healer_id");
        channel_name = intent.getStringExtra("channel_name");
        token = intent.getStringExtra("token");


        nameTV.setText("Incoming Call from " + name);

        Glide.with(IncomingCallActivity.this).load(image)
                .apply(RequestOptions.placeholderOf(R.drawable.ic_user)
                        .circleCrop()
                        .transform(new RoundedCorners(55))
                        .dontAnimate().error(R.drawable.ic_user)).into(profile_img);


        music = MediaPlayer.create(
                this, R.raw.water_bloop_2_close_distance);
        music.setLooping(true);
        music.start();
        View[] images = {findViewById(R.id.img1), findViewById(R.id.img2), findViewById(R.id.img3), findViewById(R.id.img4),}; //array of views that we want to animate

        //we will have 2 animator foreach view, fade in & fade out
        //prepare animators - creating array of animators & instantiating Object animators
        ArrayList<ObjectAnimator> anims = new ArrayList<>(images.length * 2);
        for (View v : images)
            anims.add(ObjectAnimator.ofFloat(v, View.ALPHA, 0f, 1f).setDuration(80)); //fade in animator
        for (View v : images)
            anims.add(ObjectAnimator.ofFloat(v, View.ALPHA, 1f, 0f).setDuration(80)); //fade out animator

        final AnimatorSet set = new AnimatorSet(); //create Animator set object
        //if we want to repeat the animations then we set listener to start again in 'onAnimationEnd' method
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                set.start(); //repeat animator set indefinitely
            }
        });

        set.setStartDelay(600); //set delay every time we start the chain of animations

        for (int i = 0; i < anims.size() - 1; i++)
            set.play(anims.get(i)).before(anims.get(i + 1)); //put all animations in set by order (from first to last)


        set.start();

        findViewById(R.id.endCall).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set.cancel();
                music.stop();
              /*  callDisconnect();*/
            }
        });

        findViewById(R.id.startCall).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set.cancel();
                music.stop();
                Intent intent = new Intent(IncomingCallActivity.this, VideoChatViewActivity.class);
                intent.putExtra("name", name);
                intent.putExtra("image", image);
                intent.putExtra("healer_id", healer_id);
                intent.putExtra("channel_name", channel_name);
                intent.putExtra("token", token);
                startActivity(intent);
                finish();

            }
        });

    }

   /* public void callDisconnect() {
        final RequestBody healer_ids = RequestBody.create(MediaType.parse("text/plain"), "" + healer_id);

        RestClient.disconnect(healer_ids, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    music.stop();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //TODo Error handling
                //hideLoading();

            }
        });

    }*/

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra("action") && intent.getStringExtra("action").equalsIgnoreCase("disconnected")) {
               /* callDisconnect();*/
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}