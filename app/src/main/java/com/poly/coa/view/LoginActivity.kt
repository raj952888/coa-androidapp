package com.poly.coa.view

import android.app.ProgressDialog
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import com.example.kotlinprepration.network.RestClient
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.Credentials
import com.google.android.gms.auth.api.credentials.HintRequest
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.lyxel.bringin.BaseActivity
import com.poly.coa.R
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.model.getOtpResponse.GetOtpResponse
import kotlinx.android.synthetic.main.activity_login_new.*
import retrofit2.Call
import retrofit2.Response

class LoginActivity : BaseActivity() {
    private val RC_SIGN_IN = 1
    var strNewMob: String? = null
    var mProgressDialog: ProgressDialog? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_new)

        try {
            requestHint()
        } catch (e: SendIntentException) {
            e.printStackTrace()
        }
        tv_continue.setOnClickListener {
            getOtp()
        }
    }



    fun getOtp() {

        val mobile: String = editTextMobile.text.toString().trim();
        if (mobile == "" || mobile == null || mobile.length < 10) {
            Snackbar.make(
                findViewById(android.R.id.content),
                "Please enter valid mobile number",
                Snackbar.LENGTH_LONG
            )
                .setActionTextColor(Color.WHITE)
                .show()
            return
        }

        if (TextUtils.isEmpty(mobile)) {
            showMessage("Please enter mobile number")
            return
        } else {

            val apiService = RestClient.apiService
            val loginModelCall =
                apiService.getOtp(mobile)
            showLoading()
            loginModelCall?.enqueue(object : retrofit2.Callback<GetOtpResponse?> {
                override fun onResponse(
                    call: Call<GetOtpResponse?>,
                    response: Response<GetOtpResponse?>
                ) {

                    Log.i("result", Gson().toJson(response.body()))
                    try {
                        if (response.isSuccessful) {
                            Log.i("result", Gson().toJson(response.body()))
                            var saveReferenceResponse = response.body()
                            if (saveReferenceResponse!!.status == true) {
                                hideLoading()
                                var intent = Intent(applicationContext, VerifyOtp::class.java)
                                intent.putExtra("mobileNumber", mobile)
                                CoaPrefs.putString(applicationContext,"mobileNumber",mobile)
                                startActivity(intent)


                            }else if (saveReferenceResponse!!.status == false) {
                                hideLoading()
                                showMessage("Enter valid OTP !!")


                            } else {
                                showMessage("Something went wrong !!")

                            }

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<GetOtpResponse?>, t: Throwable) {
                    showSnackBar("Failure !!")

                }


            });

        }


    }

    // Construct a request for phone numbers and show the picker
    @Throws(SendIntentException::class)
    private  fun requestHint() {
        val hintRequest = HintRequest.Builder()
            .setPhoneNumberIdentifierSupported(true)
            .build()
        val intent = Credentials.getClient(this).getHintPickerIntent(hintRequest)
        startIntentSenderForResult(
            intent.intentSender,
            RC_SIGN_IN, null, 0, 0, 0
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            RC_SIGN_IN ->                 // Obtain the phone number from the result
                // Obtain the phone number from the result
                if (resultCode == RESULT_OK) {
                    val credential: Credential =
                        data!!.getParcelableExtra(Credential.EXTRA_KEY)!!
                    val number = normalizePhoneNumber(credential!!.id)
                    editTextMobile.setText(number)
                }

        }
    }


    fun normalizePhoneNumber(number: String): String {
        try {
            if (number.contains("+91")) {
                strNewMob = number.replace("+91", "")
            }
        } catch (e: Exception) {
        }
        return strNewMob!!
    }
}