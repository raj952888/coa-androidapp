package com.poly.coa.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.lyxel.bringin.BaseActivity
import com.poly.coa.R
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.model.fcmresponse.FcmTokenResponse
import com.poly.coa.network.RetrofitInstance
import com.poly.coa.view.fragment.Account
import com.poly.coa.view.fragment.Appointment
import com.poly.coa.view.fragment.Home
import com.poly.coa.view.fragment.Service
import kotlinx.android.synthetic.main.activity_main2.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class MainActivity : BaseActivity() {

    private var mFragmentManager: FragmentManager? = null
    private var mStacks: HashMap<String, Stack<Fragment>>? = null
    val TAB_HOME = "tab_home"
    var manager: FragmentManager? = null
    private var mCurrentTab: String? = null
    val TAB_Appointment = "tab_appointment"
    val TAB_SERVICE = "tab_service"
    val TAB_ACCOUNT = "tab_account"

    var fragment_home: Fragment? = null
    var fragment_service: Fragment? = null
    var fragment_appointment: Fragment? = null
    var fragment_acc: Fragment? = null
    var userid: Int? = null
    var tokenfcm: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) {
                instanceIdResult: InstanceIdResult ->
            tokenfcm = instanceIdResult.token
            registerFCMToken(tokenfcm, userid!!);

        }
        userid = CoaPrefs.getInt(applicationContext, "user_id",0)
        registerFCMToken(tokenfcm.toString(), userid!!);
        mFragmentManager = supportFragmentManager
        mStacks = HashMap()
        mStacks!![TAB_HOME] = Stack()
        mStacks!![TAB_Appointment] = Stack()
        mStacks!![TAB_SERVICE] = Stack()
        mStacks!![TAB_ACCOUNT] = Stack()
        bottomnav_view.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomnav_view.selectedItemId = R.id.navigation_home
    }
    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    openFragment(Home.newInstance(null), TAB_HOME, true)

//                        item.setIcon(R.drawable.ic_home_tab)
                    return@OnNavigationItemSelectedListener true


                }
                R.id.navigation_account -> {
                    openFragment(Account(), TAB_ACCOUNT, true)
//                        item.setIcon(R.drawable.selector_stock_bottom_nav_view)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_appointment -> {
//                        openFragment(HomeFragment.newInstance(null), TAB_LIB, true)
                    openFragment(Appointment(), TAB_Appointment, true)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_service -> {
//                        openFragment(HomeFragment.newInstance(null), TAB_LIB, true)
                    openFragment(Service(), TAB_SERVICE, true)
                    return@OnNavigationItemSelectedListener true
                }

            }
            false
        }

    private fun openFragment(fragment: Fragment, tag: String?, shouldAdd: Boolean) {
        if (shouldAdd) {
            val fragmentMainContainer = mFragmentManager?.findFragmentById(R.id.fragment_container)

            if (!(fragmentMainContainer is Service) && tag.equals(TAB_SERVICE)) {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.add(R.id.fragment_container, fragment)
                transaction.addToBackStack(tag)
                transaction.commit()
            }

            if (!(fragmentMainContainer is Appointment) && tag.equals(TAB_Appointment)) {
                val transaction = supportFragmentManager.beginTransaction()

                transaction.add(R.id.fragment_container, fragment)
                transaction.addToBackStack(tag)
                transaction.commit()
            }

            if (!(fragmentMainContainer is Home) && tag.equals(TAB_HOME)) {
                val transaction = supportFragmentManager.beginTransaction()

                transaction.add(R.id.fragment_container, fragment)
                transaction.addToBackStack(tag)
                transaction.commit()
            }
            if (!(fragmentMainContainer is Account) && tag.equals(TAB_ACCOUNT)) {
                val transaction = supportFragmentManager.beginTransaction()

                transaction.add(R.id.fragment_container, fragment)
                transaction.addToBackStack(tag)
                transaction.commit()
            }


        }


    }

    override fun onBackPressed() {

//        if (exit_count == 1) {
//            showMessage("Press again to exit from the app.")
//
//        }
//
//        else if (exit_count == 2) {
//            finish()
//        }

        showLogoutDialog("Are you sure, want to exit?")


//        if(count==1){
//            showLogoutDialog("Are you sure, want to exit?")
//        }
//        count++;
//        super.onBackPressed()
    }

    fun showLogoutDialog(msg: String?) {
        //Inflate the dialog with custom view
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_logout, null)
        //AlertDialogBuilder
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)

        //show dialog
        val mAlertDialog = mBuilder.show()
        mAlertDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        //login button click of custom layout
        var textView = mDialogView.findViewById<TextView>(R.id.tabChangetext)
        textView.setText(msg)
        //cancel button click of custom layout
        var txt_ok = mDialogView.findViewById<TextView>(R.id.text_ok)
        var txt_cancel = mDialogView.findViewById<TextView>(R.id.text_cancel)

        txt_ok.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()

//            finishAffinity()
        }
        txt_cancel.setOnClickListener(View.OnClickListener {
            mAlertDialog.dismiss()
        })

        mAlertDialog.show()
    }

    fun registerFCMToken(fcm_token: String?, userid: Int) {
        val apiService = RetrofitInstance.getApiService()
        val artistResponseCall = apiService.fcmToken(userid.toString(), fcm_token)
        artistResponseCall.enqueue(object : Callback<FcmTokenResponse?> {
            override fun onResponse(
                call: Call<FcmTokenResponse?>,
                response: Response<FcmTokenResponse?>
            ) {
                if (response.body() != null) {


                }
            }

            override fun onFailure(call: Call<FcmTokenResponse?>, t: Throwable) {}
        })
    }

}