package com.poly.coa.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.poly.coa.R;
import com.poly.coa.model.getAllConsultant.Consultant;
import com.poly.coa.model.getAllConsultant.GetAllAstrologerResponse;
import com.google.gson.Gson;

public class MainActivity2 extends AppCompatActivity {

    GetAllAstrologerResponse consultant;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.consultantdetail);
        Gson gson = new Gson();
        consultant = gson.fromJson(getIntent().getStringExtra("consultantList"), GetAllAstrologerResponse.class);

    }
}