package com.poly.coa.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.lyxel.bringin.BaseActivity;
import com.poly.coa.R;
import com.poly.coa.Util.CoaPrefs;
import com.poly.coa.model.addamounttowallet.AddAmountToWalletResponse;
import com.poly.coa.model.verifyAmountResponse.VerifyAmountResponse;
import com.poly.coa.network.RestApiService;
import com.poly.coa.network.RetrofitInstance;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;

public class PaymentActivity extends BaseActivity implements PaymentResultListener {
    private static final String TAG = PaymentActivity.class.getSimpleName();
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    EditText editTextetAmount;
    ImageView imageViewbackbutton;
    private String orderId, order_id;
    double amount = 99.00;
    double gstAmount, sgst, finalAmount,finalGst,finalSgst;
    RadioButton radioButton99, radioButton199, radioButton299, radioButton399, radioButton499, radioButton599;
    int amountWallet;
    TextView textViewwalletAmount;
    int userTransaction;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        radioButton99 = findViewById(R.id.ninetyNine);
        textViewwalletAmount = findViewById(R.id.walletAmount);
        radioButton199 = findViewById(R.id.oneninetynine);
        radioButton299 = findViewById(R.id.twoNinetyNine);
        imageViewbackbutton= findViewById(R.id.backbutton);

        radioButton399 = findViewById(R.id.threeninetynine);
        radioButton499 = findViewById(R.id.fourNinetynine);
        radioButton599 = findViewById(R.id.fivenintynine);
        editTextetAmount = findViewById(R.id.etAmount);
        amountWallet = CoaPrefs.getInt(getApplicationContext(), "walletAmount", 0);
        textViewwalletAmount.setText("" + amountWallet);
        radioButton99.setChecked(true);
        imageViewbackbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        /*
         To ensure faster loading of the Checkout form,
          call this method as early as possible in your checkout flow.
         */
        Checkout.preload(getApplicationContext());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        String currentDateandTime = sdf.format(new Date());

        // Payment button created by you in XML layout
        TextView button = (TextView) findViewById(R.id.startpayment);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAmountToWallet();
                //startPayment();
            }
        });


    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.ninetyNine:
                if (checked) {
                    amount = 99.00;
                    radioButton199.setChecked(false);
                    radioButton299.setChecked(false);
                    radioButton399.setChecked(false);
                    radioButton499.setChecked(false);
                    radioButton599.setChecked(false);
                }


                break;
            case R.id.oneninetynine:
                if (checked) {
                    amount = 199.00;
                    radioButton99.setChecked(false);
                    radioButton299.setChecked(false);
                    radioButton399.setChecked(false);
                    radioButton499.setChecked(false);
                    radioButton599.setChecked(false);

                }

                break;

            case R.id.twoNinetyNine:
                if (checked)
                    amount = 299.00;
                radioButton99.setChecked(false);
                radioButton199.setChecked(false);
                radioButton399.setChecked(false);
                radioButton499.setChecked(false);
                radioButton599.setChecked(false);
                break;
            case R.id.threeninetynine:
                if (checked)
                    amount = 399.00;
                radioButton99.setChecked(false);
                radioButton199.setChecked(false);
                radioButton299.setChecked(false);
                radioButton499.setChecked(false);
                radioButton599.setChecked(false);
                break;
            case R.id.fourNinetynine:
                if (checked)
                    amount = 499.00;
                radioButton99.setChecked(false);
                radioButton199.setChecked(false);
                radioButton299.setChecked(false);
                radioButton399.setChecked(false);
                radioButton599.setChecked(false);
                break;
            case R.id.fivenintynine:
                if (checked)
                    amount = 599.00;
                radioButton99.setChecked(false);
                radioButton199.setChecked(false);
                radioButton299.setChecked(false);
                radioButton399.setChecked(false);
                radioButton499.setChecked(false);
                break;
        }
    }

    public void startPayment(String orderId, double amount, String keyRazorPay) {
        String mobileNumber = CoaPrefs.getString(getApplicationContext(), "mobileNumber");
        Log.e("orderId", orderId);
        order_id = orderId;
        this.orderId = orderId;
        /**
         * Instantiate Checkout
         */
        Checkout checkout = new Checkout();

        /**
         * Set your logo here
         *
         */

        /*  checkout.setKeyID("rzp_live_Bt2GGfAEf0EP9I");*/


        /**
         * Reference to current activity
         */
        final Activity activity = this;

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            JSONObject options = new JSONObject();
            options.put("name", "COA");
            options.put("description", "Payment");
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("theme.color", R.color.colorPrimary);
            options.put("currency", "INR");

            options.put("amount", amount);//pass amount in currency subunits
            options.put("order_id", orderId);//from response of step 3.
            Log.e("orderId", orderId);
            JSONObject preFill = new JSONObject();
            /* preFill.put("email", "tarun@example.com");*/
            preFill.put("contact", mobileNumber);
            options.put("prefill", preFill);
            checkout.setKeyID(keyRazorPay);
            checkout.open(activity, options);


        } catch (Exception e) {
            Log.e(TAG, "Error in starting Razorpay Checkout", e);
        }
    }


    /**
     * The name of the function has to be
     * onPaymentSuccess
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */

    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            Log.e("razorpayPaymentID", razorpayPaymentID);

            verifyAmount(razorpayPaymentID, userTransaction);

        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Toast.makeText(this, "Payment failed: ", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    private void addAmountToWallet() {
        int userId = CoaPrefs.getInt(getApplicationContext(), "user_id", 0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        String currentDateandTime = sdf.format(new Date());

        gstAmount = (amount * (9.00 / 100.00));
        sgst = (amount * (9.00 / 100.00));

        finalAmount = gstAmount + sgst+amount;
        finalGst= sgst+gstAmount;


        RestApiService apiService = RetrofitInstance.getApiService();
        showLoading();
        Call<AddAmountToWalletResponse> artistResponseCall = apiService.addAmountToWallet(userId, amount, currentDateandTime,finalGst );
        artistResponseCall.enqueue(new Callback<AddAmountToWalletResponse>() {
            @Override
            public void onResponse(Call<AddAmountToWalletResponse> call, retrofit2.Response<AddAmountToWalletResponse> response) {
                if (response.body() != null) {
                    hideLoading();
                    if (response.body().getStatus() == true) {
                        userTransaction = response.body().getResult().getUserTransaction();

                        taxesAndCharges(response.body().getResult().getDataRoz().getOrderId(), finalAmount, response.body().getResult().getDataRoz().getKey(), sgst);
                    } else if (response.body().getStatus() == false) {
                        Toast.makeText(getApplicationContext(), "Something went wrong !! !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddAmountToWalletResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure!! ", Toast.LENGTH_SHORT).show();

                hideLoading();


            }
        });


    }

    public void taxesAndCharges(String orderIdValue, double amount_value, String razorPayKey, double sgst) {
        final AlertDialog.Builder dialogBuilderDelivery = new AlertDialog.Builder(PaymentActivity.this, R.style.CustomAlertDialog);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.taxesandcharges, null);

        dialogBuilderDelivery.setView(dialogView);

        final AlertDialog dialog = dialogBuilderDelivery.create();


        TextView amounttv = dialogView.findViewById(R.id.tvamount);
        TextView gst = dialogView.findViewById(R.id.tvgst);
        TextView sgsttv = dialogView.findViewById(R.id.sgst);

        TextView subTotal = dialogView.findViewById(R.id.tvtotal);
        TextView continuee = dialogView.findViewById(R.id.tv_continue);
        LinearLayout linearLayoutClose = dialogView.findViewById(R.id.close);


        amounttv.setText("₹" + " " + df2.format(amount));
        gst.setText("₹" + " " + df2.format(gstAmount));
        sgsttv.setText("₹" + " " + df2.format(gstAmount));

        subTotal.setText("₹" + " " + df2.format(amount + gstAmount + sgst));

        continuee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startPayment(orderIdValue, amount_value, razorPayKey);
            }
        });

        linearLayoutClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();

    }

    private void verifyAmount(String razorPayId, int transactionId) {
        int userId = CoaPrefs.getInt(getApplicationContext(), "user_id", 0);

        RestApiService apiService = RetrofitInstance.getApiService();
        showLoading();
        Call<VerifyAmountResponse> artistResponseCall = apiService.verifyAmount(userId, transactionId, razorPayId, order_id);
        artistResponseCall.enqueue(new Callback<VerifyAmountResponse>() {
            @Override
            public void onResponse(Call<VerifyAmountResponse> call, retrofit2.Response<VerifyAmountResponse> response) {

                if (response.body() != null) {
                    hideLoading();

                    if (response.body().getStatus() == true) {
                        Toast.makeText(getApplicationContext(), "Payment Successful!!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(PaymentActivity.this, MainActivity.class);
                        startActivity(intent);


                    } else if (response.body().getStatus() == false) {
                        Toast.makeText(getApplicationContext(), "Something went wrong !! !!", Toast.LENGTH_SHORT).show();

                    }


                }
            }

            @Override
            public void onFailure(Call<VerifyAmountResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure!! ", Toast.LENGTH_SHORT).show();

                hideLoading();


            }
        });


    }


}