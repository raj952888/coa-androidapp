package com.poly.coa.view;

import static android.view.View.GONE;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.poly.coa.R;
import com.poly.coa.Util.AutoFitTextureView;
import com.poly.coa.Util.CoaPrefs;
import com.poly.coa.model.callDisconnectResponse.CallDisconnectResponse;
import com.poly.coa.model.getAllConsultant.GetAllAstrologerResponse;
import com.poly.coa.model.onetoone.VideoCall;
import com.poly.coa.network.RestApiService;
import com.poly.coa.network.RetrofitInstance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class PreviewActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener, ActivityCompat.OnRequestPermissionsResultCallback {

    private AutoFitTextureView mTextureView;
    String callMedium;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int MAX_PREVIEW_WIDTH = 1920;
    private static final int MAX_PREVIEW_HEIGHT = 1080;
    int getPerSessionCharge;
    int videoMinutes;

    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private Semaphore mCameraOpenCloseLock = new Semaphore(1);
    private String mCameraId;
    LinearLayout linearLayoutOne;
    RelativeLayout relativeLayout;
    GetAllAstrologerResponse consultant;
    Ringtone ringtoneSound;
    private CameraDevice mCameraDevice;
    private CameraCaptureSession mCaptureSession;
    private CaptureRequest.Builder mPreviewRequestBuilder;
    private CaptureRequest mPreviewRequest;
    private int mSensorOrientation;
    private Size mPreviewSize;
    String consultant_id;
    CountDownTimer timer;
    BroadcastReceiver broadcastReceiver;
    private TextView textViewName;

    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onResume() {
        super.onResume();

     if (callMedium.equalsIgnoreCase("video")){
         if (mTextureView.isAvailable()) {
             openCamera(mTextureView.getWidth(), mTextureView.getHeight());
         } else {
             mTextureView.setSurfaceTextureListener(this);
         }
     }else{
         textViewName.setTextColor(getColor(R.color.black));
         mTextureView.setVisibility(GONE);
     }

    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTextureView = new AutoFitTextureView(this);

        mTextureView.setSurfaceTextureListener(this);
        setContentView(R.layout.activity_preview2);


        linearLayoutOne = findViewById(R.id.linearLayout);
        linearLayoutOne.addView(mTextureView);
        int getPerSessionCharges = CoaPrefs.getInt(getApplicationContext(), "getpersessioncharge", 0);
        int walletAmount = CoaPrefs.getInt(getApplicationContext(), "walletAmount", 0);
        try {
            videoMinutes = walletAmount / getPerSessionCharges;
        } catch (ArithmeticException e) {

        }


         textViewName = findViewById(R.id.consultant_name);
        ImageView imageView = findViewById(R.id.profileImg);
        ImageView imageView1 = findViewById(R.id.btn_call);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callDisconnect();
            }
        });


        Intent intent = getIntent();
        callMedium = intent.getStringExtra("callmedium");
        Gson gson = new Gson();
        consultant = gson.fromJson(getIntent().getStringExtra("consultantList"), GetAllAstrologerResponse.class);
        int position = intent.getIntExtra("position", 0);
        CoaPrefs.putString(getApplicationContext(), "consultantname", consultant.getConsultantList().get(position).getFirstName());
        textViewName.setText(consultant.getConsultantList().get(position).getFirstName());
        consultant_id = String.valueOf(consultant.getConsultantList().get(position).getConsultantId());
        getPerSessionCharge = consultant.getConsultantList().get(position).getPerSessionCharge();
        CoaPrefs.putInt(getApplicationContext(), "consultantId", consultant.getConsultantList().get(position).getConsultantId());
        CoaPrefs.putInt(getApplicationContext(), "getpersessioncharge", consultant.getConsultantList().get(position).getPer_minute_charge());

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("action") && intent.getStringExtra("action").equalsIgnoreCase("disconnected")) {
                    callDisconnect();
                } else if (intent.hasExtra("action") && intent.getStringExtra("action").equalsIgnoreCase("received")) {
                    timer.cancel();
                    if (callMedium.equalsIgnoreCase("video")) {
                        Intent intent1 = new Intent(PreviewActivity.this, VideoChatViewActivity.class);
                        intent1.putExtra("call_id", intent.getStringExtra("call_id"));
                        startActivity(intent1);
                        finish();
                    } else {
                        Intent intent1 = new Intent(PreviewActivity.this, AudioChatViewActivity.class);
                        intent1.putExtra("call_id", intent.getStringExtra("call_id"));
                        startActivity(intent1);
                        finish();
                    }

                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction("custom.notification.navigation");
        registerReceiver(broadcastReceiver, filter);
        Glide.with(this)
                .load((consultant.getConsultantList().get(position).getCustomImage()))
                .addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(true)


                .fitCenter() // scale to fit entire image within ImageView
                .into(imageView);
        Uri ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        ringtoneSound = RingtoneManager.getRingtone(getApplicationContext(), ringtoneUri);

        if (ringtoneSound != null) {
            ringtoneSound.play();
        }
        oneToOne();
        timerInOtp();

    }

    static class CompareSizesByArea implements Comparator<Size> {

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public int compare(Size lhs, Size rhs) {
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }
    }

    public void timerInOtp() {
        timer = new CountDownTimer(60000, 1000) {
            @RequiresApi(api = Build.VERSION_CODES.N)
            public void onTick(long millisUntilFinished) {
                long timer = millisUntilFinished;
                long seconds = TimeUnit.MILLISECONDS.toSeconds(timer);
            }

            public void onFinish() {
                callDisconnect();
            }

        }.start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private static Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                          int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {
        List<Size> bigEnough = new ArrayList<>();
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            return choices[0];
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setUpCameraOutputs(int width, int height) {
        Activity activity = this;
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics
                        = manager.getCameraCharacteristics(cameraId);

                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
                    continue;
                }

                mCameraId = "1";

                StreamConfigurationMap map = characteristics.get(
                        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map == null) {
                    continue;
                }

                Size largest = Collections.max(
                        Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                        new CompareSizesByArea());

                int displayRotation = activity.getWindowManager().getDefaultDisplay().getRotation();
                mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                boolean swappedDimensions = false;
                switch (displayRotation) {
                    case Surface.ROTATION_0:
                    case Surface.ROTATION_180:
                        if (mSensorOrientation == 90 || mSensorOrientation == 270) {
                            swappedDimensions = true;
                        }
                        break;
                    case Surface.ROTATION_90:
                    case Surface.ROTATION_270:
                        if (mSensorOrientation == 0 || mSensorOrientation == 180) {
                            swappedDimensions = true;
                        }
                        break;
                }

                Point displaySize = new Point();
                activity.getWindowManager().getDefaultDisplay().getSize(displaySize);
                int rotatedPreviewWidth = width;
                int rotatedPreviewHeight = height;
                int maxPreviewWidth = displaySize.x;
                int maxPreviewHeight = displaySize.y;

                if (swappedDimensions) {
                    rotatedPreviewWidth = height;
                    rotatedPreviewHeight = width;
                    maxPreviewWidth = displaySize.y;
                    maxPreviewHeight = displaySize.x;
                }

                if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                    maxPreviewWidth = MAX_PREVIEW_WIDTH;
                }

                if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                    maxPreviewHeight = MAX_PREVIEW_HEIGHT;
                }

                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                        rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                        maxPreviewHeight, largest);

                int orientation = getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    mTextureView.setAspectRatio(
                            mPreviewSize.getWidth(), mPreviewSize.getHeight());
                } else {
                    mTextureView.setAspectRatio(
                            mPreviewSize.getHeight(), mPreviewSize.getWidth());
                }

                return;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void createCameraPreviewSession() {
        try {
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;
            Surface surface = new Surface(texture);

            mPreviewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(surface);
            mCameraDevice.createCaptureSession(Arrays.asList(surface),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            if (null == mCameraDevice) {
                                return;
                            }

                            mCaptureSession = cameraCaptureSession;
                            try {
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                        CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

                                mPreviewRequest = mPreviewRequestBuilder.build();
                                mCaptureSession.setRepeatingRequest(mPreviewRequest,
                                        null, null);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            mCameraDevice = cameraDevice;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
            Activity activity = PreviewActivity.this;
            if (null != activity) {
                activity.finish();
            }
        }

    };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void openCamera(int width, int height) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestCameraPermission();
            }
            return;
        }

        setUpCameraOutputs(width, height);
        CameraManager manager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);
        try {
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            manager.openCamera(mCameraId, mStateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        openCamera(width, height);
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        closeCamera();
        return true;
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestCameraPermission() {
        requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                return;
            }
        }
    }

    private void callDisconnect() {

        int userId = CoaPrefs.getInt(getApplicationContext(), "user_id", 0);
        RestApiService apiService = RetrofitInstance.getApiService();
        Call<CallDisconnectResponse> artistResponseCall = apiService.callDisconnect(consultant_id);
        artistResponseCall.enqueue(new Callback<CallDisconnectResponse>() {
            @Override
            public void onResponse(Call<CallDisconnectResponse> call, retrofit2.Response<CallDisconnectResponse> response) {

                if (response.body() != null) {
                    if (response.body().getStatus() == true) {
                        ringtoneSound.stop();
                        finish();
                    } else if (response.body().getStatus() == false) {
                        Toast.makeText(getApplicationContext(), "Something went wrong !! !!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CallDisconnectResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure!! ", Toast.LENGTH_SHORT).show();


            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ringtoneSound.stop();
        unregisterReceiver(broadcastReceiver);
    }

    private void oneToOne() {
        int getPerSessionCharges = CoaPrefs.getInt(getApplicationContext(), "getpersessioncharge", 0);
        int walletAmount = CoaPrefs.getInt(getApplicationContext(), "walletAmount", 0);
        try {
            videoMinutes = walletAmount / getPerSessionCharges;
        } catch (ArithmeticException e) {

        }
        int userid = CoaPrefs.getInt(getApplicationContext(), "user_id", 0);
        final RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(userid));
        RequestBody ConsultantId = RequestBody.create(MediaType.parse("text/plain"), consultant_id);
        RequestBody callType = RequestBody.create(MediaType.parse("text/plain"), callMedium);
        RequestBody duration = RequestBody.create(MediaType.parse("text/plain"), String.valueOf((videoMinutes * 60 * 1000)));


        RestApiService apiService = RetrofitInstance.getApiService();

        Call<VideoCall> artistResponseCall = apiService.OneToOne(ConsultantId, userId, callType, duration);
        artistResponseCall.enqueue(new Callback<VideoCall>() {
            @Override
            public void onResponse(Call<VideoCall> call, retrofit2.Response<VideoCall> response) {

                if (response.body() != null) {

                    if (response.body().getStatus() == true) {
                        CoaPrefs.putString(getApplicationContext(), "channelId", response.body().getData().getChannelId());
                        CoaPrefs.putString(getApplicationContext(), "token", response.body().getData().getToken());


                    } else if (response.body().getStatus() == false) {
                        Toast.makeText(getApplicationContext(), "Something went wrong !! !!", Toast.LENGTH_SHORT).show();

                    }


                }
            }

            @Override
            public void onFailure(Call<VideoCall> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failure!! ", Toast.LENGTH_SHORT).show();


            }
        });


    }
}