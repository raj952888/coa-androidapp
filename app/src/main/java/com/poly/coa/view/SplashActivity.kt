package com.poly.coa.view

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kotlinprepration.network.RestClient
import com.google.gson.Gson
import com.lyxel.bringin.BaseActivity
import com.poly.coa.R
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.model.getVersion.GetVersionApiResponse
import com.poly.coa.model.userDetailResponse.UserDetailResponse
import com.poly.coa.network.RetrofitInstance
import kotlinx.android.synthetic.main.activity_chooseyourhoroscopeactivity.*
import kotlinx.android.synthetic.main.activity_user_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SplashActivity : BaseActivity() {
    var myversionName: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_new)
        splashCall()
        getVersion()
    }

    private val SPLASH_DISPLAY_LENGTH = 3000
    fun splashCall() {
        Handler().postDelayed({

            getVersionApi()


        }, SPLASH_DISPLAY_LENGTH.toLong())
    }

    fun getVersionApi() {

        val apiService = RestClient.apiService
        val loginModelCall =
            apiService.getVersion()
        showLoading()
        loginModelCall?.enqueue(object : retrofit2.Callback<GetVersionApiResponse?> {
            override fun onResponse(
                call: Call<GetVersionApiResponse?>,
                response: Response<GetVersionApiResponse?>
            ) {

                Log.i("result", Gson().toJson(response.body()))
                try {
                    if (response.isSuccessful) {
                        Log.i("result", Gson().toJson(response.body()))
                        var saveReferenceResponse = response.body()
                        if (saveReferenceResponse!!.status == true) {

                            hideLoading()
                            var version = saveReferenceResponse.result.get(0).versionName
                            if (version==myversionName){
                                if ((CoaPrefs.getInt(applicationContext, "isVerified", 0) == 1) &&  (CoaPrefs.getBoolean(applicationContext,"userName")== true)) {
                                    val intent =
                                        Intent(this@SplashActivity, MainActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                } else if((CoaPrefs.getInt(applicationContext, "isVerified", 0) == 1) &&   (CoaPrefs.getBoolean(applicationContext,"userName")== false)) {
                                    val intent =
                                        Intent(this@SplashActivity, UserDetailActivityNew::class.java)
                                    startActivity(intent)
                                    finish()
                                }  else{
                                    val intent =
                                        Intent(this@SplashActivity, LoginActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                }
                            }else{
                                taxesAndCharges()
                            }





                        } else {
                            showMessage("Something went wrong !!")

                        }

                    } else {

                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<GetVersionApiResponse?>, t: Throwable) {
                showSnackBar("Failure !!")

            }


        });

    }

    fun getVersion(): String? {
        val context = applicationContext
        val manager = context.packageManager

        try {
            val info = manager.getPackageInfo(context.packageName, 0)
            myversionName = info.versionName


        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            myversionName = "Unknown-01"
        }
        return myversionName
    }

    fun taxesAndCharges() {
        val dialogBuilderDelivery =
            AlertDialog.Builder(this@SplashActivity, R.style.CustomAlertDialog)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.update, null)
        dialogBuilderDelivery.setView(dialogView)
        val dialog = dialogBuilderDelivery.create()

        val continuee = dialogView.findViewById<TextView>(R.id.tv_continue)
        continuee.setOnClickListener {
            openAppOnPlayStore()

        }



        dialog.show()
    }
    private fun openAppOnPlayStore() {
        val appPackageName = packageName
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$appPackageName")
                )
            )
        } catch (anfe: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }
    }



}