package com.poly.coa.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.poly.coa.R;
import com.poly.coa.Util.CoaPrefs;

public class SplashActivityNew extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_new);



        splashCall();
    }
    public void splashCall() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(CoaPrefs.getInt(getApplicationContext(),"isVerified",0)== 1){

                    Intent intent = new Intent(SplashActivityNew.this, MainActivity.class);
                    startActivity(intent);
                    finish();

                }else{
                    Intent intent = new Intent(SplashActivityNew.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }


            }
        }, SPLASH_DISPLAY_LENGTH);


    }
}