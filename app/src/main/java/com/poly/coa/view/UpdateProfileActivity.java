package com.poly.coa.view;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.poly.coa.R;
import com.poly.coa.Util.BaseActivityNew;
import com.poly.coa.Util.CoaPrefs;
import com.poly.coa.Util.CommonMethod;
import com.poly.coa.Util.FileUtils11;
import com.poly.coa.databinding.ActivityUpdateProfileNewBinding;
import com.poly.coa.model.updateProfile.UpdateProfileResponse;
import com.poly.coa.model.userDetailResponse.UserDetailResponse;
import com.poly.coa.network.RestApiService;
import com.poly.coa.network.RetrofitInstance;
import com.example.kotlinprepration.network.RestClient;
import com.google.gson.Gson;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UpdateProfileActivity extends BaseActivityNew {
    private static final int REQUEST_CODE_DOC = 12232;
    TimePickerDialog.OnTimeSetListener timeSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListenerEndTime;

    ActivityUpdateProfileNewBinding binding;
    private File imgFile;
    ImageView imageViewimageicon;
    ProgressDialog mProgressDialog;
    int healer_id;


    int typeId;


    private String genderTxt = "Male", mobileNumber;
    int user_id;

    @Override
    public int getLayoutId() {
        return R.layout.activity_update_profile_new;
    }

    @Override
    public void initView() {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutId());

        imageViewimageicon = findViewById(R.id.imageicon);
        mobileNumber = CoaPrefs.getString(getApplicationContext(), "mobileNumber");
        Intent intent = getIntent();
        if (intent != null) {
            user_id = intent.getIntExtra("userId", 0);
        }


        initViews();
    }


    private void initViews() {


        binding.imageUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasPermission(Manifest.permission.CAMERA) && hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    pickImage();

                } else {
                    requestPermissionsSafely(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, ASK_MULTIPLE_PERMISSION_REQUEST_CODE);
                }
            }
        });


        binding.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = findViewById(checkedId);
                genderTxt = radioButton.getText().toString();
            }
        });

        binding.tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(binding.fullname.getText().toString())) {
                    Toast.makeText(UpdateProfileActivity.this, "Please enter name", Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(binding.emailId.getText().toString())) {
                    Toast.makeText(UpdateProfileActivity.this, "Please enter email", Toast.LENGTH_SHORT).show();
                    return;

                }
                callUpdateApi();

            }
        });
    }


    private void browseImages() {
        String types[] = {
                "image/"
        };

        Intent intent1 = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent1.addCategory(Intent.CATEGORY_OPENABLE);
        intent1.setType("/");

        intent1.putExtra(Intent.EXTRA_MIME_TYPES, types);
        intent1.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        startActivityForResult(intent1,REQUEST_CODE_DOC);

//        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//        intent.setType("/");
//        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//        startActivityForResult(Intent.createChooser(intent, getString(R.string.choose_file)), REQUEST_CODE_DOC);

    }

    private String getMimeType(Uri uri) {

        ContentResolver cR = getContentResolver();
        return cR.getType(uri);

    }

    private void callUpdateApi() {
        if (imgFile == null) {
            Toast.makeText(this, "Please select image", Toast.LENGTH_SHORT).show();
            return;
        } else {
            imageViewimageicon.setVisibility(View.GONE);

            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "" + binding.fullname.getText().toString().trim());
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"), "" + binding.emailId.getText().toString().trim());
            RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), "" + genderTxt);
            RequestBody userid = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(33));
            RequestBody profileImg = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile);
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part profilePic =
                    MultipartBody.Part.createFormData("profile_img", imgFile.getName(), profileImg);


            RestApiService apiService = RetrofitInstance.getApiService();
            showLoadingNew();
            Call<UpdateProfileResponse> artistResponseCall = apiService.editUserDetails(name, userid, email, gender, profilePic);
            artistResponseCall.enqueue(new Callback<UpdateProfileResponse>() {
                @Override
                public void onResponse(Call<UpdateProfileResponse> call, retrofit2.Response<UpdateProfileResponse> response) {

                    if (response.body() != null) {
                        hideLoadingNew();

                        if (response.body().getStatus() == true) {
                            Toast.makeText(getApplicationContext(), "Profile details Upload successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UpdateProfileActivity.this, Chooseyourhoroscopeactivity.class);
                            startActivity(intent);
                            CoaPrefs.putBoolean(getApplicationContext(),"isProfileUpdate",true);
                            finish();
                            Toast.makeText(UpdateProfileActivity.this, "Profile details upload succesfully", Toast.LENGTH_SHORT).show();


                        } else if (response.body().getStatus() == false) {
                            Toast.makeText(getApplicationContext(), "Something went wrong !! !!", Toast.LENGTH_SHORT).show();
hideLoadingNew();
                        }


                    }
                }

                @Override
                public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Failure!! ", Toast.LENGTH_SHORT).show();

                    hideLoadingNew();


                }
            });


        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==REQUEST_CODE_DOC){
            if (data.getData() != null) {
                Uri uri = data.getData();
                String path11 = FileUtils11.Companion.makeFileCopyInCacheDir(uri, this);
                imgFile = new File(path11);
                imageViewimageicon.setVisibility(View.GONE);
                Glide.with(activity()).load(Uri.fromFile(imgFile))
                        .apply(RequestOptions.placeholderOf(R.drawable.ic_user)
                                .circleCrop()
                                .dontAnimate().error(R.drawable.ic_user)).into(binding.imageUser);
            }

            }else{
            EasyImage.handleActivityResult(requestCode, resultCode, data, UpdateProfileActivity.this, new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                    e.printStackTrace();
                }

                @Override
                public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                    imgFile = imageFiles.get(0);
                    imageViewimageicon.setVisibility(View.GONE);
                    Glide.with(activity()).load(Uri.fromFile(imgFile))
                            .apply(RequestOptions.placeholderOf(R.drawable.ic_user)
                                    .circleCrop()
                                    .dontAnimate().error(R.drawable.ic_user)).into(binding.imageUser);

                }


                @Override
                public void onCanceled(EasyImage.ImageSource source, int type) {

                }
            });
        }

    }

    private void getUserDetail() {


            RestApiService apiService = RetrofitInstance.getApiService();
            showLoadingNew();
            Call<UserDetailResponse> artistResponseCall = apiService.updateUser(mobileNumber);
            artistResponseCall.enqueue(new Callback<UserDetailResponse>() {
                @Override
                public void onResponse(Call<UserDetailResponse> call, retrofit2.Response<UserDetailResponse> response) {

                    if (response.body() != null) {
                        hideLoadingNew();

                        if (response.body().getStatus() == true) {

                         /*   binding.fullname.setText(response.body().getData().get(0).getFullName());
                            binding.emailId.setText(response.body().getData().get(0).getEmail());
                            Glide.with(activity()).load(response.body().getData().get(0).getProfileImg())
                                    .apply(RequestOptions.placeholderOf(R.drawable.ic_user)
                                            .circleCrop()
                                            .dontAnimate().error(R.drawable.ic_user)).into(binding.imageUser);
                            imageViewimageicon.setVisibility(View.GONE);*/

                            Intent intent = new Intent(UpdateProfileActivity.this, Chooseyourhoroscopeactivity.class);
                            startActivity(intent);
                            Toast.makeText(UpdateProfileActivity.this, "Profile details upload succesfully", Toast.LENGTH_SHORT).show();


                        } else if (response.body().getStatus() == false) {
                            Toast.makeText(getApplicationContext(), "Something went wrong !! !!", Toast.LENGTH_SHORT).show();

                        }


                    }
                }

                @Override
                public void onFailure(Call<UserDetailResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Failure!! ", Toast.LENGTH_SHORT).show();

                    hideLoading();


                }
            });

        }



    public void showLoadingNew() {
        hideLoading();
        mProgressDialog = CommonMethod.showLoadingDialog(this);
    }

    public void hideLoadingNew() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }
}