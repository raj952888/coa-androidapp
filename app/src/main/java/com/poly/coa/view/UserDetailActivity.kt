package com.poly.coa.view

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import br.com.onimur.handlepathoz.HandlePathOz
import br.com.onimur.handlepathoz.HandlePathOzListener
import br.com.onimur.handlepathoz.model.PathOz
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.example.kotlinprepration.network.RestClient
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.lyxel.bringin.BaseActivity
import com.poly.coa.R
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.Util.ProgressRequestBodyImage
import com.poly.coa.model.updateProfile.UpdateProfileResponse
import com.poly.coa.model.userDetailResponse.UserDetailResponse
import com.poly.coa.network.RetrofitInstance
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_user_detail.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.math.RoundingMode
import java.net.URLEncoder
import java.text.DecimalFormat


class UserDetailActivity : BaseActivity(), ProgressRequestBodyImage.UploadCallbacks,
    ImagePickerActivity.PickerOptionListener, HandlePathOzListener.SingleUri {

    var gender: String? = null
    var user_type: String? = null
    var imagePath: String? = null
    private var mobileNumber: kotlin.String? = null
    private var genderTxt = "Male"
    var user_id = 0
    var language: String? = null
    var mCropImageUri: Uri? = null
    var list_language: MutableList<String>? = ArrayList<String>()
    var progressDialog: ProgressDialog? = null
    var isUserValid: Boolean? = false
    private lateinit var handlePathOz: HandlePathOz

    var mPickerOptionListener: ImagePickerActivity.PickerOptionListener? = null
    private val TAG: String = UserDetailActivity::class.java.getSimpleName()
    val REQUEST_IMAGE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)
        handlePathOz = HandlePathOz(this, this)
        mobileNumber = CoaPrefs.getString(applicationContext, "mobileNumber")

        user_id = CoaPrefs.getInt(applicationContext, "user_id", 0)
        mPickerOptionListener = this
        initialise()
        initViews()
        getUserDetail()
        imageUser.setOnClickListener(View.OnClickListener {
            Dexter.withContext(getApplicationContext())
                .withPermissions(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                        // check for permanent denial of any permission
                        if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog()
                        }
                        if (multiplePermissionsReport.areAllPermissionsGranted()) {
                            showImagePickerOptions()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        list: List<PermissionRequest>,
                        permissionToken: PermissionToken
                    ) {
                    }
                })
                .withErrorListener { dexterError ->
                    showSettingsDialog()
                    Toast.makeText(
                        this@UserDetailActivity,
                        "Error occurred! $dexterError",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                .check()

        })
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(
            this@UserDetailActivity,
            mPickerOptionListener
        )
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(
            this@UserDetailActivity,
            R.style.AlertDialogCustomSettings
        )
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton(
            "GOTO SETTINGS"
        ) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
            "Cancel"
        ) { dialog, which -> dialog.cancel() }
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", this@UserDetailActivity.getPackageName(), null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Check which request we're responding to
        if (requestCode == 102) {
        }

        // handle result of pick image chooser
//        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
//            val imageUri = CropImage.getPickImageResultUri(this, data)
//
//            // For API >= 23 we need to check specifically that we have permissions to read external storage.
//            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
//                // request permissions and handle the result in onRequestPermissionsResult()
//                mCropImageUri = imageUri
//                requestPermissions(
//                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
//                    0
//                )
//            } else {
//                // no permissions required or already grunted, can start crop image activity
//                startCropImageActivity(imageUri)
//            }
//        }
//
//        // handle result of CropImageActivity
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            val result = CropImage.getActivityResult(data)
//            if (resultCode == RESULT_OK) {
//                val str_image_uri = result.uri.toString()
//                setImageFromStorage(str_image_uri)
//                uploadPic(str_image_uri)
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Toast.makeText(
//                    this,
//                    "Cropping failed: $result",
//                    Toast.LENGTH_LONG
//                ).show()
//            }
//        }


        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    // You can update this bitmap to your server
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)

                    // loading profile image from local cache
//                    loadProfile(uri.toString())
                    val str_image_uri = uri.toString()
                    handlePathOz.getRealPath(uri!!)
                    setImageFromStorage(str_image_uri)

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

    }

    fun setImageFromStorage(imgpath: String?) {
        if (imgpath != null) {
            Glide.with(applicationContext).load(imgpath)
                .apply(
                    RequestOptions.placeholderOf(R.drawable.ic_user)
                        .circleCrop()
                        .dontAnimate().error(R.drawable.ic_user)
                ).into(imageUser)
            imageicon.setVisibility(View.GONE)
        }
    }

    private fun launchCameraIntent() {
        val intent: Intent = Intent(
            this@UserDetailActivity,
            ImagePickerActivity::class.java
        )
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent: Intent = Intent(
            this@UserDetailActivity,
            ImagePickerActivity::class.java
        )
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    override fun onProgressUpdate(percentage: Int, mtotal: Long) {
        progressDialog!!.progress = percentage
        val df2 = DecimalFormat("#.##")

        val fsize = mtotal.toDouble() / (1024 * 1024)
        if (fsize > 1) {
            df2.roundingMode = RoundingMode.UP
        }

        progressDialog!!.setMessage("Uploading File (Size: " + df2.format(fsize) + " MB)")

    }

    private fun initialise() {
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage("Uploading File")
        progressDialog!!.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
        progressDialog!!.max = 100
        progressDialog!!.setCancelable(false)
    }

    override fun onError() {
        progressDialog!!.dismiss()
    }

    override fun onFinish() {

    }

    override fun uploadStart() {
        progressDialog!!.show()
        progressDialog!!.progress = 0
    }

    override fun onTakeCameraSelected() {
        launchCameraIntent()
    }

    override fun onChooseGallerySelected() {
        launchGalleryIntent()
    }

    override fun onRequestHandlePathOz(pathOz: PathOz, tr: Throwable?) {

        imagePath = pathOz.path
    }

    fun saveBitmapToFile(file: File): File? {

        return try {
            // BitmapFactory options to downsize the image
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            options.inSampleSize = 6
            // factor of downsizing the image
            var inputStream = FileInputStream(file)
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, options)
            inputStream.close()

            // The new size we want to scale to
            val REQUIRED_SIZE = 75

            // Find the correct scale value. It should be the power of 2.
            var scale = 1
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE &&
                options.outHeight / scale / 2 >= REQUIRED_SIZE
            ) {
                scale *= 2
            }
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            inputStream = FileInputStream(file)
            val selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2)
            inputStream.close()

            // here i override the original image file
            file.createNewFile()
            val outputStream = FileOutputStream(file)

            selectedBitmap!!.compress(Bitmap.CompressFormat.JPEG, 90, outputStream)
            file

        } catch (e: Exception) {
            null
        }
    }

    fun uploadPic(imagestoragepath: String) {

        val imageFileuri = File(imagestoragepath)

        val fileBody = ProgressRequestBodyImage(saveBitmapToFile(imageFileuri), this@UserDetailActivity)

        val videofile = MultipartBody.Part.createFormData(
            "profile_img",
            URLEncoder.encode(imageFileuri!!.name, "utf-8"),
            fileBody
        )
        val name = RequestBody.create(
            MediaType.parse("text/plain"),
            "" + fullname.getText().toString().trim()
        )
        val email = RequestBody.create(
            MediaType.parse("text/plain"),
            "" + emailId.getText().toString().trim()
        )
        val gender = RequestBody.create(MediaType.parse("text/plain"), "" + genderTxt)
        val userid = RequestBody.create(MediaType.parse("text/plain"), user_id.toString())

        val apiService = RestClient.apiService
        val loginModelCall =
            apiService.updateProfile(name, userid, email, gender, videofile)

        loginModelCall?.enqueue(object : retrofit2.Callback<UpdateProfileResponse?> {
            override fun onResponse(
                call: Call<UpdateProfileResponse?>,
                response: Response<UpdateProfileResponse?>
            ) {

                Log.i("result", Gson().toJson(response.body()))
                try {
                    if (response.isSuccessful) {
                        progressDialog!!.dismiss()
                        Log.i("result", Gson().toJson(response.body()))
                        var saveReferenceResponse = response.body()
                        if (saveReferenceResponse!!.status == true) {
                            getUserDetail()


                            showMessage("Profile Image upload Successfully")


                        } else {
                            progressDialog!!.dismiss()
                            showMessage("Something went wrong !!")

                        }

                    } else {
                        progressDialog!!.dismiss()
                        showMessage("Something went wrong !!")
                    }

                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    progressDialog!!.dismiss()
                }
            }

            override fun onFailure(call: Call<UpdateProfileResponse?>, t: Throwable) {
                showMessage("Failure")
                progressDialog!!.dismiss()
            }

        });
    }

    private fun getUserDetail() {
        val apiService = RetrofitInstance.getApiService()

        val artistResponseCall = apiService.updateUser(mobileNumber)
        artistResponseCall.enqueue(object : Callback<UserDetailResponse?> {
            override fun onResponse(
                call: Call<UserDetailResponse?>,
                response: Response<UserDetailResponse?>
            ) {
                if (response.body() != null) {

                    if (response.body()!!.status == true) {
                        fullname.setText(response.body()!!.data[0].fullName)
                        emailId.setText(response.body()!!.data[0].email)

                        Glide.with(applicationContext).load(response.body()!!.data[0].profileImg)
                            .apply(
                                RequestOptions.placeholderOf(R.drawable.ic_user)
                                    .circleCrop()
                                    .dontAnimate().error(R.drawable.ic_user)
                            ).into(imageUser)
                        imageicon.setVisibility(View.GONE)
                        if (response.body()!!.data[0].gender.equals("Male", ignoreCase = true)) {
                            radioMale.setChecked(true)
                        } else {
                            radioFemale.setChecked(true)
                        }
                        CoaPrefs.putString(
                            applicationContext,
                            "name",
                            response.body()!!.data[0].fullName
                        )
                    } else if (response.body()!!.status == false) {
                        Toast.makeText(
                            applicationContext,
                            "Something went wrong !! !!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            override fun onFailure(call: Call<UserDetailResponse?>, t: Throwable) {
                Toast.makeText(applicationContext, "Failure!! ", Toast.LENGTH_SHORT).show()
                hideLoading()
            }
        })
    }

    private fun initViews() {
        imageUser.setOnClickListener(View.OnClickListener { showImagePickerOptions() })
        radioGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            val radioButton = findViewById<RadioButton>(checkedId)
            genderTxt = radioButton.text.toString()
        })

        tv_next.setOnClickListener(View.OnClickListener {
            if (TextUtils.isEmpty(fullname.getText().toString())) {
                Toast.makeText(this@UserDetailActivity, "Please enter name", Toast.LENGTH_SHORT)
                    .show()
                return@OnClickListener
            } else if (TextUtils.isEmpty(emailId.getText().toString())) {
                Toast.makeText(this@UserDetailActivity, "Please enter email", Toast.LENGTH_SHORT)
                    .show()
                return@OnClickListener
            }
            if(TextUtils.isEmpty(imagePath)){
                showMessage("Please select image")
            }else{

                  uploadPic(imagePath!!)

            }

        })
    }
}