/*
package com.lyxel.bringin.view.activity

import br.com.onimur.handlepathoz.HandlePathOz
import br.com.onimur.handlepathoz.HandlePathOzListener
import com.lyxel.bringin.BaseActivity
import com.poly.coa.Util.ProgressRequestBodyImage
import com.poly.coa.view.ImagePickerActivity


class UserDetailsActivity : BaseActivity(), ProgressRequestBodyImage.UploadCallbacks,
    ImagePickerActivity.PickerOptionListener, HandlePathOzListener.SingleUri {

    var gender: String? = null
    var user_type: String? = null
    var language: String? = null
    var mCropImageUri: Uri? = null
    var list_language: MutableList<String>? = ArrayList<String>()
    var progressDialog: ProgressDialog? = null
    var isUserValid: Boolean? = false
    private lateinit var handlePathOz: HandlePathOz

    var mPickerOptionListener: ImagePickerActivity.PickerOptionListener? = null
    private val TAG: String = UserDetailsActivity::class.java.getSimpleName()
    val REQUEST_IMAGE = 100


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_details)

        handlePathOz = HandlePathOz(this, this)
        setView()
        mPickerOptionListener = this
    }

    fun setMessageWithClickableLink(textView: TextView) {
        //The text and the URL
        val content = "Skip now"
//        val url = "http://my-site.com/information"
        //Clickable Span will help us to make clickable a text

        val startIndex = content.indexOf("Skip now")
        val endIndex = startIndex + "Skip now".length
        //SpannableString will be created with the full content and
        // the clickable content all together
        val spannableString = SpannableString(content)
        //only the word 'link' is clickable
        spannableString.setSpan(object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true // <-- this will remove automatic underline in set span
                ds.color = resources.getColor(R.color.text_color)
            }

            override fun onClick(widget: View) {
                // do what you want with clickable value

                try {
                    val intent = Intent(this@UserDetailsActivity, InterestActivity::class.java)
                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        //The following is to set the new text in the TextView
        //no styles for an already clicked link
        textView.setText(spannableString)
        textView.setMovementMethod(LinkMovementMethod.getInstance())
        textView.setHighlightColor(Color.TRANSPARENT)
    }

    private fun updateUserdetails(
        gender: String?,
        name: String?,
        age: String?,
        birthdate: String?
    ) {
        showLoading()
        val loginService = ApiClient.buildService(ApiInterface::class.java)

        var map = HashMap<String, String>()
        if (gender != null) {
            map.put("gender", gender!!)
        }
        if (age != null) {
            map.put("age", age!!)
        }
        if (birthdate != null) {
            map.put("birthdate", birthdate!!)
        }
        if (!edCity.text.toString().isEmpty()) {
            map.put("city", edCity.text.toString())
        }

        map.put("user_type", user_type!!)
        if (!edBio.text.toString().isEmpty()) {
            map.put("bio", edBio.text.toString())
        }

//        map.put("city", edCity.text.toString())
//        map.put("location_lat", "")
//        map.put("location_long", "")
//        map.put("languages", language!!)
        map.put("handle", edUserName.text.toString())
        map.put("name", name!!)


        val requestCall =
            loginService.updateUser(
                map,
                sharedPreferenceUtility!!.getString(AppConstants.USER),
                sharedPreferenceUtility!!.getString(AppConstants.TOKEN)
            )

        requestCall.enqueue(object : Callback<UpdateUserResponse> {

            override fun onResponse(
                call: Call<UpdateUserResponse>,
                response: Response<UpdateUserResponse>
            ) {

                hideLoading()
                try {
                    if (response.isSuccessful) {
                        val loginEmailResponse = response.body()
                        if (response.code() == 200) {
                            if (loginEmailResponse!!.status == 200) {
                                loginEmailResponse?.let {
                                    if (loginEmailResponse.data.profilePic != null) {
                                        sharedPreferenceUtility!!.setString(
                                            AppConstants.USERPROFILEIMAGE,
                                            loginEmailResponse.data.profilePic
                                        )
                                    }
                                    sharedPreferenceUtility!!.setString(
                                        AppConstants.USERDETAILS,
                                        "1"
                                    )
                                    val intent = Intent(
                                        this@UserDetailsActivity,
                                        InterestActivity::class.java
                                    )
                                    intent.flags =
                                        Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                                    startActivity(intent)
                                    finishAffinity()
                                }
                            } else if (response.code() == 203) {
                                showDialog(
                                    getString(R.string.something_went_wrong)
                                )
                            } else if (loginEmailResponse!!.status == 201) {
                                showSnackBar(
                                    getString(R.string.something_went_wrong)
                                )
                            } else {
                                showSnackBar(
                                    getString(R.string.something_went_wrong)
                                )
                            }

                        } else if (response.code() == 201) {
                            showDialog(
                                getString(R.string.something_went_wrong)
                            )
                        } else if (loginEmailResponse!!.status == 203) {
                            showDialog(
                                getString(R.string.something_went_wrong)
                            )
                        } else {
                            showSnackBar(
                                getString(R.string.something_went_wrong)
                            )
                        }

                    } else {
                        showSnackBar(
                            getString(R.string.something_went_wrong)
                        )

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<UpdateUserResponse>, t: Throwable) {
                hideLoading()
                showSnackBar(
                    getString(R.string.something_went_wrong)
                )
            }
        })
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(
            this@UserDetailsActivity,
            mPickerOptionListener
        )
    }

    private fun launchCameraIntent() {
        val intent: Intent = Intent(
            this@UserDetailsActivity,
            ImagePickerActivity::class.java
        )
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent: Intent = Intent(
            this@UserDetailsActivity,
            ImagePickerActivity::class.java
        )
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    fun getAge(dobString: String): Int {
        var date: Date? = null
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        try {
            date = sdf.parse(dobString)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        if (date == null) return 0
        val dob = Calendar.getInstance()
        val today = Calendar.getInstance()
        dob.time = date
        val year = dob[Calendar.YEAR]
        val month = dob[Calendar.MONTH]
        val day = dob[Calendar.DAY_OF_MONTH]
        dob[year, month + 1] = day
        var age = today[Calendar.YEAR] - dob[Calendar.YEAR]
        if (today[Calendar.DAY_OF_YEAR] < dob[Calendar.DAY_OF_YEAR]) {
            age--
        }
        return age
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(
            this@UserDetailsActivity,
            R.style.AlertDialogCustomSettings
        )
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton(
            "GOTO SETTINGS"
        ) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
            "Cancel"
        ) { dialog, which -> dialog.cancel() }
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", this@UserDetailsActivity.getPackageName(), null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Check which request we're responding to
        if (requestCode == 102) {
        }

        // handle result of pick image chooser
//        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
//            val imageUri = CropImage.getPickImageResultUri(this, data)
//
//            // For API >= 23 we need to check specifically that we have permissions to read external storage.
//            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
//                // request permissions and handle the result in onRequestPermissionsResult()
//                mCropImageUri = imageUri
//                requestPermissions(
//                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
//                    0
//                )
//            } else {
//                // no permissions required or already grunted, can start crop image activity
//                startCropImageActivity(imageUri)
//            }
//        }
//
//        // handle result of CropImageActivity
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            val result = CropImage.getActivityResult(data)
//            if (resultCode == RESULT_OK) {
//                val str_image_uri = result.uri.toString()
//                setImageFromStorage(str_image_uri)
//                uploadPic(str_image_uri)
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                Toast.makeText(
//                    this,
//                    "Cropping failed: $result",
//                    Toast.LENGTH_LONG
//                ).show()
//            }
//        }


        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    // You can update this bitmap to your server
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)

                    // loading profile image from local cache
//                    loadProfile(uri.toString())
                    val str_image_uri = uri.toString()
                    handlePathOz.getRealPath(uri!!)
                    setImageFromStorage(str_image_uri)

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

    }


    fun setImageFromStorage(imgpath: String?) {
        if (imgpath != null) {
            Glide.with(this).asBitmap().load(Uri.parse(imgpath))
                .placeholder(R.drawable.default_user_dark).centerInside()
                .into(object : BitmapImageViewTarget(img_profile_pic) {
                    override fun setResource(resource: Bitmap?) {
                        val circularBitmapDrawable = RoundedBitmapDrawableFactory.create(
                            this@UserDetailsActivity.getResources(),
                            resource
                        )
                        circularBitmapDrawable.cornerRadius = 20f
                        img_profile_pic.setImageDrawable(circularBitmapDrawable)
                    }
                })
        }
    }

    fun setView() {

        if (isNetworkConnected()) {

            getUserDetails()
        } else {
            showSnackBar(
                getString(R.string.no_connection)
            )
        }

        segmented2.setTintColor(resources.getColor(R.color.colorAccent))

        setMessageWithClickableLink(tv_skip)

        ll_send.setOnClickListener(View.OnClickListener {
            if (!edName.text.toString().isEmpty() && !edUserName.text.toString().isEmpty()
                && user_type != null && isUserValid!! && !dob.text.toString()
                    .equals("DOB (yyyy/MM/dd)")
            ) {

                updateUserdetails(
                    gender,
                    edName.text.toString(),
                    getAge(dob.text.toString()).toString(),
                    dob.text.toString()
                )
            } else {
                showDialog("Please enter all valid details")
            }

        })

        edUserName.addTextChangedListener(textWatcher)

        dob.setOnClickListener(View.OnClickListener {
            val c = Calendar.getInstance()
            val mYear = c[Calendar.YEAR]
            val mMonth = c[Calendar.MONTH]
            val mDay = c[Calendar.DAY_OF_MONTH]

            val datePickerDialog = DatePickerDialog(
                this,
                { view, year, monthOfYear, dayOfMonth ->
                    dob.setText(
                        String.format(
                            "%04d/%02d/%02d", year,
                            monthOfYear + 1, dayOfMonth
                        )
                    )

                }, mYear, mMonth, mDay
            )
            datePickerDialog.show()

        })

        img_profile_pic.setOnClickListener(View.OnClickListener {
            Dexter.withContext(getApplicationContext())
                .withPermissions(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(multiplePermissionsReport: MultiplePermissionsReport) {
                        // check for permanent denial of any permission
                        if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog()
                        }
                        if (multiplePermissionsReport.areAllPermissionsGranted()) {
                            showImagePickerOptions()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        list: List<PermissionRequest>,
                        permissionToken: PermissionToken
                    ) {
                    }
                })
                .withErrorListener { dexterError ->
                    showSettingsDialog()
                    Toast.makeText(
                        this@UserDetailsActivity,
                        "Error occurred! $dexterError",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                .check()

        })

        val list_gender: Array<String>? = resources.getStringArray(R.array.gender)
        var adapter = ArrayAdapter(
            this,
            R.layout.spinner_text,
            list_gender!!
        )
        spinner_gender.adapter = adapter
        spinner_gender.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                if (position > 0) {
                    gender = list_gender.get(position)
                } else {
                    gender = null
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }

        val list_audience: Array<String>? = resources.getStringArray(R.array.audience)
        var adapter_audience = ArrayAdapter(
            this,
            R.layout.spinner_text,
            list_audience!!
        )
        spinner_audience.adapter = adapter_audience

        spinner_audience.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                if (position > 0) {
                    user_type = list_audience!!.get(position)
                } else {
                    user_type = null
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }



        initialise()
    }

    fun uploadPic(imagestoragepath: String) {
//        var file1: File? = null
//        val bitmap = decodeUriToBitmap(this@UserDetailsActivity, Uri.parse(imagestoragepath))
//        if (bitmap != null) {
//            file1 = saveToExternal(bitmap)
//        }

        val imageFileuri = File(imagestoragepath)

        val fileBody = ProgressRequestBodyImage(imageFileuri, this@UserDetailsActivity)

        val videofile = MultipartBody.Part.createFormData(
            "image",
            URLEncoder.encode(imageFileuri!!.name, "utf-8"),
            fileBody
        )


        val loginService = ApiClientVideo.buildService(ApiInterface::class.java)


        val requestCall =
            loginService.uploadPic(
                sharedPreferenceUtility!!.getString(AppConstants.TOKEN),
                videofile
            )

        requestCall.enqueue(object : Callback<UploadPicResponse> {

            override fun onResponse(
                call: Call<UploadPicResponse>,
                response: Response<UploadPicResponse>
            ) {


                try {
                    if (response.isSuccessful) {
                        progressDialog!!.dismiss()
                        val loginEmailResponse = response.body()
                        if (response.code() == 200) {
                            if (loginEmailResponse!!.status == 200) {
                                Toast.makeText(
                                    this@UserDetailsActivity,
                                    "profile photo uploaded successfully",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                showSnackBar(
                                    getString(R.string.something_went_wrong)
                                )
                            }

                        } else if (response.code() == 403) {
                            progressDialog!!.dismiss()
                            showSnackBar(
                                getString(R.string.something_went_wrong)
                            )

                        } else {
                            progressDialog!!.dismiss()
                            showSnackBar(
                                getString(R.string.something_went_wrong)
                            )

                        }
                    }
                } catch (e: Exception) {
                    progressDialog!!.dismiss()
                    e.printStackTrace()
                    showSnackBar(
                        getString(R.string.something_went_wrong)
                    )
                }

            }

            override fun onFailure(call: Call<UploadPicResponse>, t: Throwable) {
                progressDialog!!.dismiss()
                showSnackBar(
                    getString(R.string.something_went_wrong)
                )
            }
        })
    }

    private fun initialise() {
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage("Uploading File")
        progressDialog!!.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
        progressDialog!!.max = 100
        progressDialog!!.setCancelable(false)
    }

    override fun onProgressUpdate(percentage: Int, mtotal: Long) {
        progressDialog!!.progress = percentage
        val df2 = DecimalFormat("#.##")

        val fsize = mtotal.toDouble() / (1024 * 1024)
        if (fsize > 1) {
            df2.roundingMode = RoundingMode.UP
        }

        progressDialog!!.setMessage("Uploading File (Size: " + df2.format(fsize) + " MB)")

    }

    override fun onError() {
        progressDialog!!.dismiss()
    }

    override fun onFinish() {

    }

    override fun uploadStart() {
        progressDialog!!.show()
        progressDialog!!.progress = 0
    }

    private fun userHandler() {

        val loginService = ApiClient.buildService(ApiInterface::class.java)
        var map = HashMap<String, String>()
        map.put("handle", edUserName.text.toString())
        val requestCall =
            loginService.userHandler(
                sharedPreferenceUtility!!.getString(
                    AppConstants.TOKEN
                ), map

            )

        requestCall.enqueue(object : Callback<UserhandleResponse> {

            override fun onResponse(
                call: Call<UserhandleResponse>,
                response: Response<UserhandleResponse>
            ) {

                try {
                    if (response.isSuccessful) {
                        val loginEmailResponse = response.body()
                        if (response.code() == 200) {
                            if (loginEmailResponse!!.status == 200) {
                                if (loginEmailResponse.data.available && CommonMethod.isUsernameValid(
                                        edUserName.text.toString()
                                    )
                                ) {
//                                    isuser.visibility = View.VISIBLE
                                    isuser.setText("user available")
                                    isuser.setTextColor(resources.getColor(R.color.colorAccent))
                                    isUserValid = true
                                } else {
                                    if (edUserName.isEnabled) {
                                        isUserValid = false
                                    } else {
                                        isUserValid = true
                                    }

                                    if (!CommonMethod.isUsernameValid(edUserName.text.toString())) {
//                                    isuser.visibility = View.VISIBLE
                                        isuser.setText("user not available")
                                        isuser.setTextColor(resources.getColor(R.color.red))
                                    } else {
                                        isuser.text = "Invalid Username"
                                        isuser.setTextColor(resources.getColor(R.color.red))
                                    }

                                }


                            } else {
                                showSnackBar(
                                    getString(R.string.something_went_wrong)
                                )
                            }
                        } else {
                            showSnackBar(
                                getString(R.string.something_went_wrong)
                            )
                        }

                    } else {
                        showSnackBar(
                            getString(R.string.something_went_wrong)
                        )

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    showSnackBar(
                        getString(R.string.something_went_wrong)
                    )
                }

            }

            override fun onFailure(call: Call<UserhandleResponse>, t: Throwable) {
                showSnackBar(
                    getString(R.string.something_went_wrong)
                )
            }
        })
    }

    private fun getLanguage() {

        val loginService = ApiClient.buildService(ApiInterface::class.java)

        val requestCall =
            loginService.getLanguages(
                sharedPreferenceUtility!!.getString(AppConstants.TOKEN)
            )

        requestCall.enqueue(object : Callback<LanguageResponse> {

            override fun onResponse(
                call: Call<LanguageResponse>,
                response: Response<LanguageResponse>
            ) {

                hideLoading()
                try {
                    if (response.isSuccessful) {
                        val loginEmailResponse = response.body()
                        if (response.code() == 200) {
                            if (loginEmailResponse!!.status == 200) {
                                loginEmailResponse?.let {
                                    list_language!!.add("Select Language")
                                    for (language in loginEmailResponse.data) {
                                        list_language!!.add(language.language)
                                    }
                                    setAdapterLanguage()
                                }
                            } else if (response.code() == 203) {
                                showDialog(
                                    getString(R.string.something_went_wrong)
                                )
                            } else if (loginEmailResponse!!.status == 201) {
                                showSnackBar(
                                    getString(R.string.something_went_wrong)
                                )
                            } else {
                                showSnackBar(
                                    getString(R.string.something_went_wrong)
                                )
                            }

                        } else if (response.code() == 201) {
                            showDialog(
                                getString(R.string.something_went_wrong)
                            )
                        } else if (loginEmailResponse!!.status == 203) {
                            showDialog(
                                getString(R.string.something_went_wrong)
                            )
                        } else {
                            showSnackBar(
                                getString(R.string.something_went_wrong)
                            )
                        }

                    } else {
                        showSnackBar(
                            getString(R.string.something_went_wrong)
                        )

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<LanguageResponse>, t: Throwable) {
                hideLoading()
                showSnackBar(
                    getString(R.string.something_went_wrong)
                )
            }
        })
    }

    private fun saveToExternal(bm: Bitmap): File? {
        val root = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/Bringin")
        myDir.mkdirs()
        val generator = Random()
        var n = 10000
        n = generator.nextInt(n)
        val fname = "Image-$n.jpg"
        val file = File(myDir, fname)
        Log.i(ContentValues.TAG, "" + file)
        if (file.exists()) file.delete()
        try {
            val out = FileOutputStream(file)
            bm.compress(Bitmap.CompressFormat.JPEG, 90, out)
            out.flush()
            out.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return file
    }

    private fun decodeUriToBitmap(mContext: Context, sendUri: Uri): Bitmap? {
        var getBitmap: Bitmap? = null
        try {
            val image_stream: InputStream?
            try {
                image_stream = mContext.contentResolver.openInputStream(sendUri)
                getBitmap = BitmapFactory.decodeStream(image_stream)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return getBitmap
    }

    private fun getUserDetails() {
        showLoading()
        val loginService = ApiClient.buildService(ApiInterface::class.java)

        val requestCall =
            loginService.getUserDetails(sharedPreferenceUtility!!.getString(AppConstants.TOKEN))

        requestCall.enqueue(object : Callback<UserdetailsResponse> {

            override fun onResponse(
                call: Call<UserdetailsResponse>,
                response: Response<UserdetailsResponse>
            ) {

                hideLoading()
                try {
                    if (response.isSuccessful) {
                        val loginEmailResponse = response.body()
                        if (response.code() == 200) {
                            if (loginEmailResponse!!.status == 200) {
                                loginEmailResponse?.let {
                                    ll_send.isClickable = true

                                    getLanguage()
                                    gender = loginEmailResponse.data.data.gender
                                    edName.setText(loginEmailResponse.data.data.name)
                                    if (loginEmailResponse.data.data.profilePic != null) {
                                        sharedPreferenceUtility!!.setString(
                                            AppConstants.USERPROFILEIMAGE,
                                            loginEmailResponse.data.data.profilePic
                                        )
                                    }
                                    if (loginEmailResponse.data.data.birthdate != null) {
                                        dob.setText(loginEmailResponse.data.data.birthdate)
                                    }
                                    if (loginEmailResponse.data.data.bio != null) {
                                        edBio.setText(loginEmailResponse.data.data.bio)
                                    }
                                    if (loginEmailResponse.data.data.city != null) {
                                        edCity.setText(loginEmailResponse.data.data.city)
                                    }
                                    if (loginEmailResponse.data.data.gender != null) {
                                        try {
                                            if (loginEmailResponse.data.data.gender.equals(
                                                    "Male",
                                                    ignoreCase = true
                                                )
                                            ) {
                                                spinner_gender.setSelection(1)

                                            } else if (loginEmailResponse.data.data.gender.equals(
                                                    "Female",
                                                    ignoreCase = true
                                                )
                                            ) {
                                                spinner_gender.setSelection(2)

                                            } else if (loginEmailResponse.data.data.gender.equals(
                                                    "Intersex",
                                                    ignoreCase = true
                                                )
                                            ) {
                                                spinner_gender.setSelection(3)

                                            } else if (loginEmailResponse.data.data.gender.equals(
                                                    "Trans",
                                                    ignoreCase = true
                                                )
                                            ) {
                                                spinner_gender.setSelection(4)

                                            } else if (loginEmailResponse.data.data.gender.equals(
                                                    "Fluid",
                                                    ignoreCase = true
                                                )
                                            ) {
                                                spinner_gender.setSelection(5)

                                            } else if (loginEmailResponse.data.data.gender.equals(
                                                    "Prefer",
                                                    ignoreCase = true
                                                )
                                            ) {
                                                spinner_gender.setSelection(6)

                                            } else if (loginEmailResponse.data.data.gender.equals(
                                                    "Other",
                                                    ignoreCase = true
                                                )
                                            ) {
                                                spinner_gender.setSelection(7)
                                            } else {
                                                spinner_gender.setSelection(0)
                                            }
                                        } catch (e: Exception) {
                                            e.printStackTrace()
                                        }
                                    }

                                    edUserName.setText(loginEmailResponse.data.data.handle)
                                    sharedPreferenceUtility!!.setString(
                                        AppConstants.HANDLE,
                                        loginEmailResponse.data.data.handle
                                    )
                                    sharedPreferenceUtility!!.setString(
                                        AppConstants.USERNAME,
                                        edName.text.toString()
                                    )
                                    sharedPreferenceUtility!!.setString(
                                        AppConstants.DOB,
                                        dob.text.toString()
                                    )

                                    if (loginEmailResponse.data.data.handle != null
                                        && !loginEmailResponse.data.data.handle.equals("")
                                    ) {
                                        edUserName.isEnabled = false
                                        isuser.visibility = View.GONE
                                        isUserValid = true
                                    } else {
                                        edUserName.isEnabled = true
                                    }
                                    setImageFromStorage(loginEmailResponse.data.data.profilePic)
                                    if (loginEmailResponse.data.data.userType != null) {
                                        try {
                                            if (loginEmailResponse.data.data.userType.equals("Creator")) {
                                                spinner_audience.setSelection(1)
                                            } else if (loginEmailResponse.data.data.userType.equals(
                                                    "Audience"
                                                )
                                            ) {
                                                spinner_audience.setSelection(2)
                                            } else {
                                                spinner_audience.setSelection(3)
                                            }
                                        } catch (e: Exception) {
                                            e.printStackTrace()
                                        }
                                    }

                                    ll_send.isClickable = true


                                }
                            } else if (response.code() == 203) {
                                showDialog(
                                    getString(R.string.something_went_wrong)
                                )
                            } else if (loginEmailResponse!!.status == 201) {
                                showSnackBar(
                                    getString(R.string.something_went_wrong)
                                )
                            } else {
                                showSnackBar(
                                    getString(R.string.something_went_wrong)
                                )
                            }

                        } else if (response.code() == 201) {
                            showSnackBar(
                                getString(R.string.something_went_wrong)
                            )
                        } else if (loginEmailResponse!!.status == 203) {
                            showDialog(
                                getString(R.string.something_went_wrong)
                            )
                        } else {
                            showSnackBar(
                                getString(R.string.something_went_wrong)
                            )
                        }

                    } else {
                        showSnackBar(
                            getString(R.string.something_went_wrong)
                        )

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call<UserdetailsResponse>, t: Throwable) {
                hideLoading()
                showSnackBar(
                    getString(R.string.something_went_wrong)
                )
            }
        })
    }

    fun setAdapterLanguage() {
        var adapter_language = ArrayAdapter(
            this,
            R.layout.spinner_text,
            list_language!!
        )
        spinner_language.adapter = adapter_language
        spinner_language.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                if (position > 0) {
                    language = list_language!!.get(position)
                } else {
                    language = null
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }

    }

    override fun onTakeCameraSelected() {
        launchCameraIntent()
    }

    override fun onChooseGallerySelected() {
        launchGalleryIntent()
    }

    override fun onRequestHandlePathOz(pathOz: PathOz, tr: Throwable?) {
        Log.d("hi", pathOz.path)
        uploadPic(pathOz.path)
    }
}*/
