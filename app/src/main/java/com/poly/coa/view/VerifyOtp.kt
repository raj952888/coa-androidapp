package com.poly.coa.view

import android.content.*
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.TextView.BufferType
import androidx.annotation.RequiresApi
import com.poly.coa.R
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.model.getOtpResponse.GetOtpResponse
import com.poly.coa.model.verifyOtpResponse.VerifyOtpResponse
import com.example.kotlinprepration.network.RestClient
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.lyxel.bringin.BaseActivity
import kotlinx.android.synthetic.main.activity_login_new.*
import kotlinx.android.synthetic.main.bottomnavigationdesignforotpverify.*
import kotlinx.android.synthetic.main.bottomnavigationdesignforotpverify.resendotp
import kotlinx.android.synthetic.main.bottomnavigationdesignforotpverifynew.*
import kotlinx.android.synthetic.main.bottomnavigationdesignforotpverifynew.close
import kotlinx.android.synthetic.main.bottomnavigationdesignforotpverifynew.mobile_no
import kotlinx.android.synthetic.main.bottomnavigationdesignforotpverifynew.pinViewedittext
import kotlinx.android.synthetic.main.bottomnavigationdesignforotpverifynew.timer
import kotlinx.android.synthetic.main.bottomnavigationdesignforotpverifynew.verifyOtp
import retrofit2.Call
import retrofit2.Response
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern


class VerifyOtp : BaseActivity() {

    private val SMS_CONSENT_REQUEST = 2
    var mobileNO = ""
    var oneTimeCode = ""

    private val smsVerificationReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (SmsRetriever.SMS_RETRIEVED_ACTION == intent.action) {
                val extras = intent.extras
                val smsRetrieverStatus = extras!![SmsRetriever.EXTRA_STATUS] as Status?
                when (smsRetrieverStatus!!.statusCode) {
                    CommonStatusCodes.SUCCESS -> {
                        // Get consent intent
                        val consentIntent =
                            extras.getParcelable<Intent>(SmsRetriever.EXTRA_CONSENT_INTENT)
                        try {
                            // Start activity to show consent dialog to user, activity must be started in
                            // 5 minutes, otherwise you'll receive another TIMEOUT intent
                            startActivityForResult(
                                consentIntent,
                                SMS_CONSENT_REQUEST
                            )
                        } catch (e: ActivityNotFoundException) {
                            // Handle the exception ...
                        }
                    }
                    CommonStatusCodes.TIMEOUT -> {
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bottomnavigationdesignforotpverifynew)
        val intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        registerReceiver(smsVerificationReceiver, intentFilter)



        gobackNew.setOnClickListener {

            finish()

        }


        verifyOtp.setOnClickListener {

            verifyOtp()

        }
        resendotp.setOnClickListener {
            getOtp()

        }

        close.setOnClickListener {
            finish()
        }
        val extras = intent
        if (extras != null) {
            mobileNO = extras.getStringExtra("mobileNumber")!!
            CoaPrefs.putString(applicationContext,"mobileNumber",mobileNO)
            val spannable = SpannableString( "A 4 - Digit OTP has been sent to  $mobileNO ,Please enter")
            spannable.setSpan(
                ForegroundColorSpan(Color.BLUE),
                34, // start
                44, // end
                Spannable.SPAN_EXCLUSIVE_INCLUSIVE
            )

            mobile_no.text =spannable


        }






        timerInOtp()
        val smsRetrieverClient = SmsRetriever.getClient(this)
        smsRetrieverClient.startSmsUserConsent(null)

    }

    fun verifyOtp() {
        if (TextUtils.isEmpty(pinViewedittext.text)) {
            showMessage("Please enter OTP !!")
            return
        } else {
            var otp = pinViewedittext.text

            val apiService = RestClient.apiService
            val loginModelCall =
                apiService.verifyOtp(mobileNO, otp?.toString())
            showLoading()
            loginModelCall?.enqueue(object : retrofit2.Callback<VerifyOtpResponse?> {
                override fun onResponse(
                    call: Call<VerifyOtpResponse?>,
                    response: Response<VerifyOtpResponse?>
                ) {

                    Log.i("result", Gson().toJson(response.body()))
                    try {
                        if (response.isSuccessful) {
                            Log.i("result", Gson().toJson(response.body()))
                            var saveReferenceResponse = response.body()
                            if (saveReferenceResponse!!.status == true) {

                                hideLoading()

                                var userId = saveReferenceResponse.data.id

                                var userName = saveReferenceResponse.data.fullName
                                if(userName== null){
                                    var intent =
                                        Intent(applicationContext, UserDetailActivityNew::class.java)
                                    intent.putExtra("userId", userId)
                                    CoaPrefs.putBoolean(applicationContext,"userName", false)


                                    CoaPrefs.putInt(applicationContext,"isVerified",saveReferenceResponse.data.isVerified)
                                    CoaPrefs.putInt(applicationContext,"user_id",userId)
                                    startActivity(intent)
                                    finish()
                                }else{
                                    var intent =
                                        Intent(applicationContext, MainActivity::class.java)
                                    intent.putExtra("userId", userId)
                                    CoaPrefs.putBoolean(applicationContext,"userName", true)
                                    CoaPrefs.putString(applicationContext,"name", saveReferenceResponse.data.fullName.toString())
                                    CoaPrefs.putInt(applicationContext,"user_id",userId)
                                    CoaPrefs.putInt(applicationContext,"isVerified",saveReferenceResponse.data.isVerified)
                                    startActivity(intent)

                                    finish()
                                }





                            } else if(saveReferenceResponse.status== false) {

                                showMessage("Enter Valid OTP !!")

                            }

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<VerifyOtpResponse?>, t: Throwable) {
                    showSnackBar("Failure !!")

                }


            });

        }


    }

    fun timerInOtp() {
        object : CountDownTimer(50000, 1000) {
            @RequiresApi(api = Build.VERSION_CODES.N)
            override fun onTick(millisUntilFinished: Long) {
                val seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                val s = seconds.toString()
                val spannable = SpannableString( "Resend OTP in $s sec ")
                spannable.setSpan(
                    ForegroundColorSpan(Color.RED),
                    14, // start
                    20, // end
                    Spannable.SPAN_EXCLUSIVE_INCLUSIVE
                )
                timer.setText(spannable)
            }

            override fun onFinish() {
                Snackbar.make(
                    findViewById(android.R.id.content),
                    "OTP expire please click on resend !! ",
                    Snackbar.LENGTH_LONG
                )
                    .setActionTextColor(Color.WHITE)
                    .show()
                resendotp.setVisibility(View.VISIBLE)
                timer.setVisibility(View.GONE)
                pinViewedittext.setText("")
                hideLoading()
            }
        }.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            SMS_CONSENT_REQUEST -> if (resultCode == RESULT_OK) {
                // Get SMS message content
                val message = data!!.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE)
                // Extract one-time code from the message and complete verification
                // `sms` contains the entire text of the SMS message, so you will need
                // to parse the string.
                oneTimeCode = parseCode(message.toString())!! // define this function
                pinViewedittext.setText(oneTimeCode)


                verifyOtp()

                // send one time code to the server
            } else {
                // Consent canceled, handle the error ...
            }
        }
    }

    private fun parseCode(message: String): String? {
        val p = Pattern.compile("\\b\\d{4}\\b")
        val m = p.matcher(message)
        var code: String? = ""
        while (m.find()) {
            code = m.group(0)
        }
        return code
    }


    fun getOtp() {

        val mobile: String = mobileNO;

        if (TextUtils.isEmpty(mobile)) {
            return
        } else {

            val apiService = RestClient.apiService
            val loginModelCall =
                apiService.getOtp(mobile)
            showLoading()
            loginModelCall?.enqueue(object : retrofit2.Callback<GetOtpResponse?> {
                override fun onResponse(
                    call: Call<GetOtpResponse?>,
                    response: Response<GetOtpResponse?>
                ) {

                    Log.i("result", Gson().toJson(response.body()))
                    try {
                        if (response.isSuccessful) {
                            Log.i("result", Gson().toJson(response.body()))
                            var saveReferenceResponse = response.body()
                            if (saveReferenceResponse!!.status == true) {
                                hideLoading()


                            } else {
                                showMessage("Something went wrong !!")

                            }

                        } else {

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<GetOtpResponse?>, t: Throwable) {
                    showSnackBar("Failure !!")

                }


            });

        }


    }
}