package com.poly.coa.view.dialog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.poly.coa.R;
import com.poly.coa.view.Chooseyourhoroscopeactivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;


public class CustomizationDialog {


    BottomSheetDialog bottomSheetDialog, bottomSheetDialogEdit;

    Activity activity;


    public CustomizationDialog(Activity activity) {
        this.activity = activity;

        AddMoreItemOrCustomizationDialog();


    }


    private void AddMoreItemOrCustomizationDialog() {

        bottomSheetDialog = new BottomSheetDialog(activity);
        final View bottomDialougeCustomization = activity.getLayoutInflater().inflate(R.layout.bottomnavigationdesignforotpverifynew, null);
        bottomSheetDialog.setContentView(bottomDialougeCustomization);
        bottomSheetDialog.setCancelable(true);
        TextView tvProductPrice = bottomDialougeCustomization.findViewById(R.id.dialogContinue);
        tvProductPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, Chooseyourhoroscopeactivity.class);
                activity.startActivity(intent);
            }
        });
        showAddMoreItemOrCustomizationDialog();

    }

    private void showAddMoreItemOrCustomizationDialog() {
        bottomSheetDialog.show();
    }


}
