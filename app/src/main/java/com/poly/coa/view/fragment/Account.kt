package com.poly.coa.view.fragment


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.poly.coa.R
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.model.userDetailResponse.UserDetailResponse
import com.poly.coa.network.RetrofitInstance
import com.poly.coa.view.*
import com.lyxel.bringin.BaseFragment
import kotlinx.android.synthetic.main.account.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Account : BaseFragment() {

    var mactivity: MainActivity? = null
    var mobileNumber: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.account, container, false)

        mactivity = activity as MainActivity?

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mobileNumber = CoaPrefs.getString(mactivity, "mobileNumber")


        termandcondition.setOnClickListener {
           val intent = Intent(mactivity, WebViewActivity::class.java)
           intent.putExtra("url", "https://coa.polyedgetechnologies.com/terms")
           intent.putExtra("title", "Terms & Conditions")
           startActivity(intent)
       }

         privacy.setOnClickListener {
             val intent = Intent(mactivity, WebViewActivity::class.java)
             intent.putExtra("url", "https://coa.polyedgetechnologies.com/privacy")
             intent.putExtra("title", "Privacy Policy")
             startActivity(intent)
         }

        edit_tv.setOnClickListener {
            val intent = Intent(mactivity, UserDetailActivity::class.java)
            startActivity(intent)
        }
        logoutll.setOnClickListener {
            Toast.makeText(mactivity, "Logged out", Toast.LENGTH_SHORT).show()
            CoaPrefs.clear(mactivity)

            val intent = Intent(activity, SplashActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            startActivity(intent)
            mactivity!!.finish()
        }
    }




    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }

    companion object {

        @JvmStatic
        fun newInstance(bundle: Bundle?) =
            Account().apply {
                arguments = bundle
            }
    }



    private fun getUserDetail() {
        val apiService = RetrofitInstance.getApiService()

        val artistResponseCall = apiService.updateUser(mobileNumber)
        artistResponseCall.enqueue(object : Callback<UserDetailResponse?> {
            override fun onResponse(
                call: Call<UserDetailResponse?>,
                response: Response<UserDetailResponse?>
            ) {
                if (response.body() != null) {
                    hideLoading()
                    if (response.body()!!.status == true) {
                       titleTv.setText(response.body()!!.data[0].fullName)

                        Glide.with(mactivity!!).load(response.body()!!.data[0].profileImg)
                            .apply(
                                RequestOptions.placeholderOf(R.drawable.ic_user)
                                    .circleCrop()
                                    .dontAnimate().error(R.drawable.ic_user)
                            ).into(profileImg)


                        /*Intent intent = new Intent(AccountDetailActivity.this, Chooseyourhoroscopeactivity.class);
                        startActivity(intent);
                        Toast.makeText(AccountDetailActivity.this, "Profile details upload succesfully", Toast.LENGTH_SHORT).show();
*/
                    } else if (response.body()!!.status == false) {
                        Toast.makeText(
                            mactivity,
                            "Something went wrong !! !!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            override fun onFailure(call: Call<UserDetailResponse?>, t: Throwable) {
                Toast.makeText(mactivity, "Failure!! ", Toast.LENGTH_SHORT).show()
                hideLoading()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        getUserDetail()
    }
}