package com.poly.coa.view.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.poly.coa.R
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import kotlinx.android.synthetic.main.fragment_appointment.*

class Appointment : Fragment() {

    private var completed: Completed? = null
    var tabLayout: TabLayout? = null
    private var upcoming: Upcoming? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_appointment, container, false)
        tabLayout = view.findViewById(R.id.tabChallenge)
        bindWidgetsWithAnEvent()
        setupTabLayout()


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }

    private fun setupTabLayout() {
        upcoming = Upcoming()
        completed = Completed()

        tabLayout?.addTab(tabLayout?.newTab()?.setText("Up coming")!!, true)
        tabLayout?.addTab(tabLayout?.newTab()?.setText("Completed")!!)

    }


    private fun bindWidgetsWithAnEvent() {
        tabLayout?.setOnTabSelectedListener(object : OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                setCurrentTabFragment(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    fun setCurrentTabFragment(tabPosition: Int) {
        when (tabPosition) {
            0 -> replaceFragmentWithStack(
                R.id.fl_inside_home, Upcoming.newInstance(null),
                Upcoming::class.java.getSimpleName()
            )
            1 -> {

                replaceFragmentWithStack(
                    R.id.fl_inside_home, Completed.newInstance(null),
                    Completed::class.java.getSimpleName()
                )

            }

        }
    }

    fun replaceFragmentWithStack(containerId: Int, fragment: Fragment, name: String?) {
        try {
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(containerId, fragment, name)
            if (!fragment.isAdded) {
                fragmentTransaction.addToBackStack(name)
                fragmentTransaction.commitAllowingStateLoss()
            }
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(bundle: Bundle?) =
            Appointment().apply {
                arguments = bundle
            }
    }
}