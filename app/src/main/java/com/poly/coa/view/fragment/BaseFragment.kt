package com.lyxel.bringin


import android.app.AlertDialog
import android.app.ProgressDialog
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.poly.coa.R
import com.poly.coa.Util.CommonMethod

public abstract class BaseFragment : Fragment() {

    private var mProgressDialog: ProgressDialog? = null
    private var mAlertDialog: AlertDialog? = null

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    fun showLoading() {
        hideLoading();
        mProgressDialog = CommonMethod.showLoadingDialog(requireContext())
    }

    fun hideLoading() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing()) {
            mProgressDialog!!.cancel()
        }
    }


    fun hideLoadingAlert() {
        if (mAlertDialog != null && mAlertDialog!!.isShowing()) {
            mAlertDialog!!.cancel()
        }
    }


    fun showMessage(message: String?) {
        if (message != null) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(
                activity,
                getString(R.string.something_went_wrong),
                Toast.LENGTH_SHORT
            )
                .show()
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }


    override fun onDestroy() {
        super.onDestroy()
    }
}