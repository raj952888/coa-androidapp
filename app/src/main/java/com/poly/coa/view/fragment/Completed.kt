package com.poly.coa.view.fragment


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.poly.coa.R
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.adapter.CompletedNew
import com.poly.coa.adapter.UpComing
import com.poly.coa.model.completedBooking.CompleteResponse
import com.poly.coa.model.completedBooking.Datum
import com.poly.coa.view.MainActivity
import com.example.kotlinprepration.network.RestClient
import com.google.gson.Gson
import com.lyxel.bringin.BaseFragment
import kotlinx.android.synthetic.main.completed.*
import retrofit2.Call
import retrofit2.Response

class Completed : BaseFragment() {

    var linearLayoutTermAndCondition: LinearLayout? = null

    var user_Id: Int? = null

    var mactivity: MainActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mactivity = activity as MainActivity?
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.completed, container, false)
        user_Id = CoaPrefs.getInt(mactivity,"user_id",0)



        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        getCompletedBooking()


    }

    companion object {

        @JvmStatic
        fun newInstance(bundle: Bundle?) =
            Completed().apply {
                arguments = bundle
            }
    }

    fun getCompletedBooking() {


        val apiService = RestClient.apiService
        val updateHoroscopeCall =
            apiService.getCompletedBooking(32)
        showLoading()
        updateHoroscopeCall?.enqueue(object : retrofit2.Callback<CompleteResponse?> {
            override fun onResponse(
                call: Call<CompleteResponse?>,
                response: Response<CompleteResponse?>
            ) {

                Log.i("result", Gson().toJson(response.body()))
                try {
                    if (response.isSuccessful) {
                        Log.i("result", Gson().toJson(response.body()))
                        var saveReferenceResponse = response.body()
                        if (saveReferenceResponse!!.status == true) {
                            hideLoading()
                            var list: MutableList<Datum>? = saveReferenceResponse.data
                            if(list != null && list.size>0){
                                var myAdapter = CompletedNew(list, mactivity)

                                val layoutManager: RecyclerView.LayoutManager =
                                    LinearLayoutManager(mactivity)
                                recyclerComplete.setLayoutManager(layoutManager)
                                recyclerComplete.setItemAnimator(DefaultItemAnimator())
                                recyclerComplete.setAdapter(myAdapter)
                            }else{

                            }


                        } else {
                            showMessage("Something went wrong !!")

                        }

                    } else {

                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<CompleteResponse?>, t: Throwable) {
                showMessage("Failure !!")

            }


        });

    }
}