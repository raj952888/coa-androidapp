package com.poly.coa.view.fragment


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.poly.coa.R
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.adapter.HomeBookingList
import com.poly.coa.model.homedataresponse.TodayBooking
import com.poly.coa.view.*
import com.example.kotlinprepration.network.RestClient
import com.google.gson.Gson
import com.lyxel.bringin.BaseFragment
import com.poly.coa.model.homedataresponse.HomeDataResponse
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.upcoming.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class Home : BaseFragment() {
    var mactivity: MainActivity? = null
    var user_Id: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mactivity = activity as MainActivity?

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        user_Id = CoaPrefs.getInt(mactivity, "user_id", 0)

        name.text= getGreetingMessage() + " ! " +CoaPrefs.getString(mactivity,"name")
        homeData()

        instantCall.setOnClickListener {
            val intent = Intent(mactivity, GetAllConsultantActivity::class.java)
            startActivity(intent)
        }
        llbookingappointment.setOnClickListener {
            /*val intent = Intent(mactivity, BookingAppointmentActivity::class.java)
            startActivity(intent)*/
            Toast.makeText(mactivity,"coming soon",Toast.LENGTH_LONG).show()
        }

        wallet.setOnClickListener {
            val intent = Intent(mactivity, PaymentActivity::class.java)
            startActivity(intent)
        }
        wallettwo.setOnClickListener {
            val intent = Intent(mactivity, PaymentActivity::class.java)
            startActivity(intent)
        }


    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }

    companion object {

        @JvmStatic
        fun newInstance(bundle: Bundle?) =
            Home().apply {
                arguments = bundle
            }
    }
    fun getGreetingMessage(): String {
        val c = Calendar.getInstance()
        val timeOfDay = c.get(Calendar.HOUR_OF_DAY)

        return when (timeOfDay) {
            in 0..11 -> "Good Morning"
            in 12..15 -> "Good Afternoon"
            in 16..20 -> "Good Evening"
            in 21..23 -> "Good Night"
            else -> "Hello"
        }
    }

    fun homeData() {


        val apiService = RestClient.apiService
        val updateHoroscopeCall =
            apiService.homeData(user_Id.toString())
        showLoading()
        updateHoroscopeCall?.enqueue(object : retrofit2.Callback<HomeDataResponse?> {
            override fun onResponse(
                call: Call<HomeDataResponse?>,
                response: Response<HomeDataResponse?>
            ) {

                Log.i("result", Gson().toJson(response.body()))
                try {
                    if (response.isSuccessful) {
                        Log.i("result", Gson().toJson(response.body()))
                        var saveReferenceResponse = response.body()
                        if (saveReferenceResponse!!.status == true) {
                            hideLoading()

                            if(saveReferenceResponse.todayBookingList.size>0 && saveReferenceResponse.todayBookingList != null){
                                bookingcount.text =
                                    "You have " +  saveReferenceResponse.todayBookingList.size + " " + "Booking"
                                bookingcount.setTextColor(resources.getColor(R.color.green))

                            }



                            todayHoroscope.text = saveReferenceResponse.horoscope.description
                            horoscopename.text = saveReferenceResponse.horoscopeDetails.horoscopeTitle
                            Glide.with(mactivity!!)
                                .load(saveReferenceResponse.horoscopeDetails.horoscopeImage)
                                .centerCrop()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .skipMemoryCache(true)
                                .fitCenter()
                                .into(image_horoscope)
                            if(saveReferenceResponse.walletAmount== null){
                                walletPrice.text ="0.00"
                            }else{
                                walletPrice.text =saveReferenceResponse.walletAmount.toString()
                            }
                            CoaPrefs.putInt(mactivity,"walletAmount",
                                saveReferenceResponse.walletAmount.toInt()
                            )

                            var list: MutableList<TodayBooking>? = saveReferenceResponse.todayBookingList
                            var myAdapter = HomeBookingList(list, mactivity)

                            val layoutManager: RecyclerView.LayoutManager =
                                LinearLayoutManager(mactivity)
                            booking.setLayoutManager(layoutManager)
                            booking.setItemAnimator(DefaultItemAnimator())
                            booking.setAdapter(myAdapter)

                        } else {
                            showMessage("Something went wrong !!")

                        }

                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<HomeDataResponse?>, t: Throwable) {
                showMessage("Failure !!")

            }


        });

    }


}
