package com.poly.coa.view.fragment


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.poly.coa.R
import com.poly.coa.Util.CoaPrefs
import com.poly.coa.adapter.GetAllSlot
import com.poly.coa.adapter.UpComing
import com.poly.coa.model.getslot.GetConsultantSlotResponse
import com.poly.coa.model.getslot.Result
import com.poly.coa.model.upcomingResponse.Datum
import com.poly.coa.model.upcomingResponse.UpcomingResponse
import com.poly.coa.view.MainActivity
import com.example.kotlinprepration.network.RestClient
import com.google.gson.Gson
import com.lyxel.bringin.BaseFragment
import kotlinx.android.synthetic.main.activity_get_all_consultant.*
import kotlinx.android.synthetic.main.book_appointment.*
import kotlinx.android.synthetic.main.upcoming.*
import retrofit2.Call
import retrofit2.Response

class Upcoming : BaseFragment() {

    var user_Id :Int ?= null

    var mactivity: MainActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.upcoming, container, false)

        mactivity = activity as MainActivity?


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        user_Id = CoaPrefs.getInt(mactivity,"user_id",0)
        getUpcomingBooking()

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }

    companion object {

        @JvmStatic
        fun newInstance(bundle: Bundle?) =
            Upcoming().apply {
                arguments = bundle
            }
    }

    fun getUpcomingBooking() {


        val apiService = RestClient.apiService
        val updateHoroscopeCall =
            apiService.getUpcomingBooking(user_Id)
        showLoading()
        updateHoroscopeCall?.enqueue(object : retrofit2.Callback<UpcomingResponse?> {
            override fun onResponse(
                call: Call<UpcomingResponse?>,
                response: Response<UpcomingResponse?>
            ) {

                Log.i("result", Gson().toJson(response.body()))
                try {
                    if (response.isSuccessful) {
                        Log.i("result", Gson().toJson(response.body()))
                        var saveReferenceResponse = response.body()
                        if (saveReferenceResponse!!.status == true) {
                            hideLoading()
                            var list: MutableList<Datum>? = saveReferenceResponse.data
                            if(list != null && list.size>0){
                                var myAdapter = UpComing(list, mactivity)

                                val layoutManager: RecyclerView.LayoutManager =
                                    LinearLayoutManager(mactivity)
                                recyclerupcoming.setLayoutManager(layoutManager)
                                recyclerupcoming.setItemAnimator(DefaultItemAnimator())
                                recyclerupcoming.setAdapter(myAdapter)
                            }else{
                                noBooking.visibility= View.VISIBLE
                                date.visibility= View.VISIBLE
                                showMessage("No booking")
                            }


                        } else {
                            showMessage("Something went wrong !!")

                        }

                    } else {

                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<UpcomingResponse?>, t: Throwable) {
                showMessage("Failure !!")

            }


        });

    }
}