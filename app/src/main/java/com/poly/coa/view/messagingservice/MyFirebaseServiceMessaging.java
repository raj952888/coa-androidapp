package com.poly.coa.view.messagingservice;

/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.poly.coa.R;
import com.poly.coa.Util.CoaPrefs;
import com.poly.coa.Util.Constants;
import com.poly.coa.view.IncomingCallActivity;


/**
 * NOTE: There can only be one service in each app that receives FCM messages. If multiple
 * are declared in the Manifest then the first one will be chosen.
 * <p>
 * In order to make this Java sample functional, you must remove the following from the Kotlin messaging
 * service in the AndroidManifest.xml:
 * <p>
 * <intent-filter>
 * <action android:name="com.google.firebase.MESSAGING_EVENT" />
 * </intent-filter>
 */
public class MyFirebaseServiceMessaging extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private static NotificationCompat.Builder drivingNotifBldr;
    private MediaPlayer music;
    private NotificationCompat.Builder mNotifBuilder;
    private boolean isCallActive = false;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (isCallActive) {
            return;
        }

        if (remoteMessage.getData().size() > 0) {
            if (remoteMessage.getData().containsKey("action")) {
                Intent filter = new Intent("custom.notification.navigation");
                if (remoteMessage.getData().containsKey("call_id")) {
                    filter.putExtra("call_id", remoteMessage.getData().get("call_id"));

                }
                filter.putExtra("action", remoteMessage.getData().get("action"));
                sendBroadcast(filter);
                return;
            }
            isCallActive = true;
            String image = remoteMessage.getData().get("healer_image");
            String name = remoteMessage.getData().get("healer_name");
            String healer_id = remoteMessage.getData().get("healer_id");
            String channel_name = remoteMessage.getData().get("channel_name");
            String token = remoteMessage.getData().get("token");
            Intent intent = new Intent(this, IncomingCallActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("name", name);
            intent.putExtra("image", image);
            intent.putExtra("healer_id", "" + healer_id);
            intent.putExtra("channel_name", "" + channel_name);
            intent.putExtra("token", "" + token);
            startActivity(intent);
            isCallActive = false;
        }
        isCallActive = false;

    }

    private void handleNotification(RemoteMessage remoteMessage) {

    }


    @Override
    public void onNewToken(String token) {
        int userId = CoaPrefs.getInt(getApplicationContext(), "user_id", 0);
       /* Constants.registerFCMToken(token, userId);*/
        CoaPrefs.putString(this,"fcmToken",token);
        Log.d(TAG, "Refreshed token: " + token);


    }

    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }


    private String getChannelId() {
        return "CALL_ID";
    }


}